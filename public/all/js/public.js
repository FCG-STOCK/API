//从cookie取值改变昵称
//使用decodeURI对中文进行解码
var username = $.cookie("username");
var nick_name = $.cookie("nick_name");

    if(username){
      	if(username==nick_name){
        	//手机号加*
          	var xin = username.substr(0,3);
          	var xin2 = xin+"********";
          	$("#mybs").html(xin2);	
        }else{
        	$("#mybs").html(nick_name);	
        }
      
        
        $("#mybs").attr("href","/index/center/myaccount.html");
        $("#mybj").html("退出");
        $("#mybj").attr("onclick","loginout()");
        $("#mybj").attr("href","#");
        if(username==nick_name){
            $(".siot").html("未设置昵称");
        }else{
            $(".siot").html(nick_name);
        }
        
    }

//获取头像信息并显示
$.ajax({
        url: "/API/info/header",
        dataType: "json",
        success: function (data) {
            // alert('ajax调用成功');
            var dataObj = JSON.parse(data);
            if(dataObj.status==1){
               $("#xgtx").attr('src',dataObj.list);
              $("#lala").attr('src',dataObj.list);
            }                 
        }
    })





//退出登陆清楚cookie
function loginout(){
	$.ajax({
        url: "/API/index/loginout",
        dataType: "json",
        success: function (data) {
            // alert('ajax调用成功');
            var dataObj = JSON.parse(data);
            if(dataObj.status==1){
               window.location.href = '/index/index/login.html';
            }else{
               alert("失败");
            }                            
        }
    })             
}

//检查输入的昵称的长度，最多7个字符
function check(){
    var abc = $("#name").val();
    var len = abc.length;
    if(len>7||len==0){
        $("#test").css("display","block");
        $(".suy").removeAttr("onclick");
    }else{
        $(".suy").attr("onclick","setname()");
    }
}

//设置昵称
function setname(){
        $(function(){
            $.ajax({
                url: "/API/index/nickname",
                data: { nickname: $("#name").val(),
                        username: $.cookie("username")},
                dataType: "json",
                method:"post", 
                async:true,
                success: function (data) {
                    // alert('ajax调用成功');
                    var dataObj = JSON.parse(data);
                    if(dataObj.status==1){
                        //用户名存cookie
                        // $.cookie("nick_name", "", {expires: -1});
                        // $.cookie("nick_name",$("#name").val());
                        var nick_name = $.cookie("nick_name");
                        $(".siot").html(nick_name);
                        layer.closeAll();
                        //修改成功后再弹出一层提示修改成功的框
                        layer.open({
                            type: 1,
                            title:"修改成功",
                            area: ['400px', '150px'],
                            shadeClose: true, //点击遮罩关闭
                            content: '\<\div class="box6"><div class="box6left">\n' +
                                '    <div class="box6lftcen">\n' +
                                '      <img src="/public/all/image/cel/quer.png">\n' +
                                '    </div>\n' +
                                '    </div>\n' +
                                '    <div class="box6rig">\n' +
                                '        <div class="box6rigcen">\n' +
                                '            <p>昵称已经修改</p >\n' +
                                '        </div>\n' +
                                '\n' +
                                '    </div> \<\/div>'
                        });
                        setTimeout(function () { layer.closeAll(); }, 1000);//延迟1秒后关闭 
                          
                    }else{
                        alert("修改失败");
                    }                            
                }
                })                                        
        })      
    }

//修改头像
function setheader(file){
        
    	
  		var fd = new FormData();
   fd.append('headurl[]',file);

        $.ajax({  url: "/API/info/uppicture",
            data: fd,
            dataType: "json",
            method:"post", 
            cache:false,
            traditional: true,//设置tradtional为true阻止深度序列化
            contentType: false,
            processData: false,//processData 默认为false，当设置为true的时候,jquery ajax 提交的时候不会序列化 data，而是直接使用data
            success: function (data) { 
                var dataObj = JSON.parse(data);
                if(dataObj.status==1){
                    var src = dataObj.list;
                    $("#lala").attr("src",src);
                    $("#xgtx").attr("src",src);
                    layer.closeAll();
                        //修改成功后再弹出一层提示修改成功的框
                        layer.open({
                            type: 1,
                            title:"修改成功",
                            area: ['400px', '150px'],
                            shadeClose: true, //点击遮罩关闭
                            content: '\<\div class="box6"><div class="box6left">\n' +
                                '    <div class="box6lftcen">\n' +
                                '      <img src="/public/all/image/cel/quer.png">\n' +
                                '    </div>\n' +
                                '    </div>\n' +
                                '    <div class="box6rig">\n' +
                                '        <div class="box6rigcen">\n' +
                                '            <p>头像已经修改</p >\n' +
                                '        </div>\n' +
                                '\n' +
                                '    </div> \<\/div>'
                        });
                        setTimeout(function () { layer.closeAll(); }, 1000);//延迟1秒后关闭 
                }
              	if(dataObj.status==1100){
                    	layer.open({
                          type: 1,
                          title:"提示",
                          area: ['600px', '150px'],
                          shadeClose: true, //点击遮罩关闭
                          content: '\<\div class="box6" style="margin-left: 1%;margin-top: 5%;"><div class="box6left">\n' +
                          '    <div class="box6lftcen">\n' +
                          '      <img src="/public/all/image/cel/error.png">\n' +
                          '    </div>\n' +
                          '    </div>\n' +
                          '    <div class="box6rig">\n' +
                          '        <div class="box6rigcen" style="text-align:center;">\n' +
                          '            <p>'+dataObj.msg+'</p >\n' +
                          '        </div>\n' +
                          '\n' +
                          '    </div> \<\/div>'
                                    });
                    	return;
                                        
                    }
              
              
            } 
        }) 
        
    }

