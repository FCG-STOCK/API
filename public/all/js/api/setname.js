function setname(){
    		$(function(){
    			$.ajax({
                    url: "/API/index/nickname",
                    data: { nickname: $("#name").val(),
                			username: $.cookie("username")},
                    dataType: "json",
                    method:"post", 
                    async:true,
                    success: function (data) {
                        // alert('ajax调用成功');
                        var dataObj = JSON.parse(data);
                        if(dataObj.status==1){
                            //用户名存cookie
                            // $.cookie("nick_name", "", {expires: -1});
                            $.cookie("nick_name",$("#name").val());
                            //数据提交后关闭弹出层  
                            var index = parent.layer.getFrameIndex(window.name);//获得index  
                            parent.layer.close(index);//延迟                           
                            var username = $.cookie("username");
                            var nick_name = $.cookie("nick_name");
                            if(username==nick_name){
                                $(".siot").html("未设置昵称");
                            }else{
                                $(".siot").html(nick_name);
                            }                             
                            //layer信息提示  
                            // layer.msg('修改成功');                            
                        }else{
                            alert("修改失败");
                        }                            
                    }
                    })                                        
    		})    	
    	}