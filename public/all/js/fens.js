			$(function () {
                // var data = $("form").serialize();
                    $.ajax({
                        url:"/api/info/grailindex",
                        dataType:"text",
                        success: function (data) {
                            // alert('ajax调用成功');
                           			
                          			var oAit=JSON.parse(data); 
                          
                          Highcharts.dateFormats = {
                              W: function (timestamp) {
                                  var date = new Date(timestamp),
                                      day = date.getUTCDay() === 0 ? 7 : date.getUTCDay(),
                                      dayNumber;
                                  date.setDate(date.getUTCDate() + 4 - day);
                                  dayNumber = Math.floor((date.getTime() - new Date(date.getUTCFullYear(), 0, 1, -6)) / 86400000);
                                  return 1 + Math.floor(dayNumber / 7);
                              }
                          };
                                
                          chart = Highcharts.chart('container', {
                                chart: {
                                    zoomType: 'x'
                                },
                                title: {
                                    text: ''
                                },
                              credits: {  
                                              enabled: false    
                                          },  
                                xAxis: {
                                    type: 'datetime',
                                    dateTimeLabelFormats: {
                                        millisecond: '%H:%M:%S.%L',
                                        second: '%H:%M:%S',
                                        minute: '%H:%M',
                                        hour: '%H:%M',
                                        day: '%m-%d',
                                        week: '%m-%d',
                                        month: '%Y-%m',
                                        year: '%Y'
                                    }
                                },
                                tooltip: {
                                    xDateFormat: '%Y-%m-%d',
                                    shared: true
                                },
                                yAxis: {
                                    title: {
                                        text: ''
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                    area: {
                                        fillColor: {
                                            linearGradient: {
                                                x1: 0,
                                                y1: 0,
                                                x2: 0,
                                                y2: 1
                                            },
                                            stops: [
                                                [0, Highcharts.getOptions().colors[0]],
                                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                            ]
                                        },
                                        marker: {
                                            radius: 2
                                        },
                                        lineWidth: 1,
                                        states: {
                                            hover: {
                                                lineWidth: 1
                                            }
                                        },
                                        threshold: null
                                    }
                                },
                                series: [{
                                    type: 'area',
                                    name: '股价',
                                    data: oAit
                                }]
                            
                            
                            });
                              
                        }
                    })                          
  
  
                }) 