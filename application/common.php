<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Db;
use think\Request;

/**
 * 判断是否为合法的身份证号码
 * @param $mobile
 * @return int
 */
function isCreditNo($vStr){
    $vCity = array(
        '11','12','13','14','15','21','22',
        '23','31','32','33','34','35','36',
        '37','41','42','43','44','45','46',
        '50','51','52','53','54','61','62',
        '63','64','65','71','81','82','91'
    );
    if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) return false;
    if (!in_array(substr($vStr, 0, 2), $vCity)) return false;
    $vStr = preg_replace('/[xX]$/i', 'a', $vStr);
    $vLength = strlen($vStr);
    if ($vLength == 18) {
        $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
    } else {
        $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
    }
    if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) return false;
    if ($vLength == 18) {
        $vSum = 0;
        for ($i = 17 ; $i >= 0 ; $i--) {
            $vSubStr = substr($vStr, 17 - $i, 1);
            $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr , 11));
        }
        if($vSum % 11 != 1) return false;
    }
    return true;
}

// CURL
function https_request($url,$header=NULL,$data=null){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,$url);
    if(!empty($header)){
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    }
    if (!empty($data)){
        curl_setopt($curl,CURLOPT_POST,1);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}

//公用方法
function sendMessage_common($mobile,$content){
    //参数赋值
    $name = 'skcl';//账号
    $password = 'x4hbr81q';//密码
    $seed = date("YmdHis");//当前时间
    $content = $content;//短信内容
    $ext = '';//扩展号码
    $reference = '';//参考信息
    $enCode = 'UTF-8';//编码（UTF-8、GBK）
    $method = 'GET';//请求方式（POST、GET）
    $url = '';
    if ($enCode=='UTF-8'){
        $url = 'http://160.19.212.218:8080/eums/utf8/send_strong.do';//UTF-8编码接口地址
    }else if ($enCode=='GBK'){
        $url = 'http://160.19.212.218:8080/eums/send_strong.do';//GBK编码接口地址
    }
    $content  = encoding($content,$enCode);//注意编码，字段编码要和接口所用编码一致，有可能出现汉字之类的记得转换编码

    //请求参数
    //utf8和gbk编码请自行转换
    $params = array(
        'name' => encoding($name,$enCode),//帐号，由网关分配
        'seed' => $seed,//当前时间，格式：YYYYMMDD HHMMSS，例如：20130806102030。客户时间早于或晚于网关时间超过30分钟，则网关拒绝提交。
        //从php5.1.0开始，PHP.ini里加了date.timezone这个选项，并且默认情况下是关闭的也就是显示的时间（无论用什么php命令）都是格林威治标准时间和我们的时间（北京时间）差了正好8个小时。
        //找到php.ini中的“;date.timezone =”这行，将“;”去掉，改成“date.timezone = PRC”（PRC：People's Republic of China 中华人民共和国），重启Apache，问题解决。
        'key' => md5(md5($password).$seed),//md5( md5(password)  +  seed) )
        //其中“+”表示字符串连接。即：先对密码进行md5加密，将结果与seed值合并，再进行一次md5加密。
        //两次md5加密后字符串都需转为小写。
        //例如：若当前时间为2013-08-06 10:20:30，密码为123456，
        //则：key=md5(md5(“123456”) + “20130806102030” )
        //则：key=md5(e10adc3949ba59abbe56e057f20f883e20130806102030)
        //则：key= cd6e1aa6b89e8e413867b33811e70153
        'dest' => $mobile,//手机号码（多个号码用“半角逗号”分开），GET方式每次最多100个号码，POST方式号码个数不限制，但建议不超过3万个
        'content' => $content,//短信内容。最多500个字符。
        'ext' => $ext,//扩展号码（视通道是否支持扩展，可以为空或不填）
        'reference' => $reference//参考信息（最多50个字符，在推送状态报告、推送上行时，推送给合作方，本参数不参与任何下行控制，仅为合作方提供方便，可以为空或不填）如果不知道如何使用，请忽略该参数，不能含有半角的逗号和分号。
    );
    //echo $method.'请求如下：<br/>';
    if ($method=='POST'){
        $resp =  send_post_curl($url, $params);//POST请求，数据返回格式为error:xxx,success:xxx
    }else if ($method=='GET'){
        $resp =  send_get($url, $params);//GET请求，数据返回格式为error:xxx,success:xxx
    }
    $response = explode(':', $resp);

    $code = $response[1];//响应代码

    if ($response[0]=='success'){
      	return json([],1,'发送成功');
     }else{
      	return json([],0,'发送失败');
     }
}


function send_get($url,$params){
    $getdata = http_build_query($params);
    $content = file_get_contents($url.'?'.$getdata);
    return $content;
}
function send_post_curl($url,$params){
    $postdata = http_build_query($params);
    $length = strlen($postdata);
    $cl = curl_init($url);//①：初始化
    curl_setopt($cl, CURLOPT_POST, true);//②：设置属性
    curl_setopt($cl,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
    curl_setopt($cl,CURLOPT_HTTPHEADER,array("Content-Type: application/x-www-form-urlencoded","Content-length: ".$length));
    curl_setopt($cl,CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($cl,CURLOPT_RETURNTRANSFER,true);
    $content = curl_exec($cl);//③：执行并获取结果
    curl_close($cl);//④：释放句柄
    return $content;
}
function encoding($str,$urlCode){
    if( !empty($str) ){
        $fileType = mb_detect_encoding($str , array('UTF-8','GBK','LATIN1','BIG5')) ;
    }
    return mb_convert_encoding($str, $urlCode, $fileType);
}

function sendMessage($rand,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    $content = '【'.$title.'】您的验证码是'.$rand.'。如非本人操作，请忽略本短信';//短信内容
    return sendMessage_common($mobile,$content);
}

//信用金亏损50%（后台可设置）提醒
function losecredit_message_tip($content,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    $content = '【'.$title.'】'.$content;
    return sendMessage_common($mobile,$content);
}
//止盈提醒
function zhiying_message_tip($content,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    $content = '【'.$title.'】'.$content;
    return sendMessage_common($mobile,$content);
}
//止损提醒
function zhisun_message_tip($content,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    $content = '【'.$title.'】'.$content;
    return sendMessage_common($mobile,$content);
}

//用户策略余额不足，不够扣除管理费了
function diyan_tip($content,$mobile){
    $title=Db::name('hide')->where(['id' => 40])->value('value');
    $content = '【'.$title.'】'.$content;
    return sendMessage_common($mobile,$content);
}

//获取某只股的详细信息
function stockDetail($stock_code){
    $shipanstock=new Stock();
    $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$shipanstock->Hq($stock_code)));
    $data=[
        'name'=>$arr[1],
        'code'=>$arr[2],
        'nowPri'=>$arr[3],
        'todayopen'=>$arr[5],
        'yestodayshou'=>$arr[4],
        'chengjiaoliang'=>$arr[36],  //成交量（手）
        'zongshizhi'=>$arr[45],      //总市值
        'increase'=>$arr[31],        //涨跌
        'increPer'=>$arr[32],        //涨跌%
        'upstopprice'=>$arr[47],     //涨停价
        'downstopprice'=>$arr[48],   //跌停价
        'zuigao'=>$arr[41],          //最高
        'zuidi'=>$arr[42],           //最低
        'shiyinglv'=>$arr[39],       //市盈率
        'shijinglv'=>$arr[46],       //市净率
        'buyOnePri'=>$arr[9],        //买一
        'buyOne'=>$arr[10],          //买一量（手）
        'buyTwoPri'=>$arr[11],
        'buyTwo'=>$arr[12],
        'buyThreePri'=>$arr[13],
        'buyThree'=>$arr[14],
        'buyFourPri'=>$arr[15],
        'buyFour'=>$arr[16],
        'buyFivePri'=>$arr[17],
        'buyFive'=>$arr[18],
        'sellOnePri'=>$arr[19],
        'sellOne'=>$arr[20],
        'sellTwoPri'=>$arr[21],
        'sellTwo'=>$arr[22],
        'sellThreePri'=>$arr[23],
        'sellThree'=>$arr[24],
        'sellFourPri'=>$arr[25],
        'sellFour'=>$arr[26],
        'sellFivePri'=>$arr[27],
        'sellFive'=>$arr[28],
    ];

    return $data;
}

//搜索界面查询数据
function getOneStock($gid) {
    $shipanstock=new Stock();
    $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$shipanstock->Hq($gid)));

    $list['result'][0]['data']['gid']=input('gid');
    $list['result'][0]['data']['increPer']=$arr[32];
    $list['result'][0]['data']['increase']=$arr[31];
    $list['result'][0]['data']['name']=$arr[1];
    $list['result'][0]['data']['todayStartPri']=$arr[5];
    $list['result'][0]['data']['yestodEndPri']=$arr[4];
    $list['result'][0]['data']['nowPri']=$arr[3];
    $list['result'][0]['data']['todayMax']=$arr[33];
    $list['result'][0]['data']['todayMin']=$arr[34];
    $list['result'][0]['data']['traNumber']=$arr[36];
    $list['result'][0]['data']['traAmount']=$arr[37];
    $list['result'][0]['data']['buyOne']=$arr[10];
    $list['result'][0]['data']['buyOnePri']=$arr[9];
    $list['result'][0]['data']['buyTwo']=$arr[12];
    $list['result'][0]['data']['buyTwoPri']=$arr[11];
    $list['result'][0]['data']['buyThree']=$arr[14];
    $list['result'][0]['data']['buyThreePri']=$arr[13];
    $list['result'][0]['data']['buyFour']=$arr[16];
    $list['result'][0]['data']['buyFourPri']=$arr[15];
    $list['result'][0]['data']['buyFive']=$arr[18];
    $list['result'][0]['data']['buyFivePri']=$arr[17];
    $list['result'][0]['data']['sellOne']=$arr[20];
    $list['result'][0]['data']['sellOnePri']=$arr[19];
    $list['result'][0]['data']['sellTwo']=$arr[22];
    $list['result'][0]['data']['sellTwoPri']=$arr[21];
    $list['result'][0]['data']['sellThree']=$arr[24];
    $list['result'][0]['data']['sellThreePri']=$arr[23];
    $list['result'][0]['data']['sellFour']=$arr[26];
    $list['result'][0]['data']['sellFourPri']=$arr[25];
    $list['result'][0]['data']['sellFive']=$arr[28];
    $list['result'][0]['data']['sellFivePri']=$arr[27];
    $list['result'][0]['data']['date']=substr($arr[30],0,4).'-'.substr($arr[30],4,2).'-'.substr($arr[30],6,2);
    $list['result'][0]['data']['time']=substr($arr[30],8,2).':'.substr($arr[30],10,2).':'.substr($arr[30],12,2);

    return $list;
}


//搜索界面查询数据
function getOneStock2($gid) {
    $shipanstock=new Stock();
  	$result=$shipanstock->Hq_list($gid);
    $para=explode(';',$result);
    $list['nowPri']=explode(',',$para[0])[3];
  	$list['gid']=$gid;
  	return $list;
  
}

//判断股票是否停盘（创建策略）
function thischeckstockstop($gid) {
    $shipanstock=new Stock();

    $upstop=Db::name('hide')->where(['id'=>22])->value('value');//禁止购买的涨停百分比 0.08
  	$downstop=Db::name('hide')->where(['id'=>47])->value('value');//禁止购买的跌停百分比 -0.08
	$dattime=Db::name('hide')->where(['id' => 48])->value('value');
    $time=1;
    for($i=2;$i<12;$i++){
        if(isHoliday($i)!=0){
            $time+=1;
        }else{
            break;
        }
        continue;
    }
    $times=1;
    for($i=1;$i<12;$i++){
        if(isHoliday($i)!=0){
            $times+=1;
        }else{
            break;
        }
        continue;
    }
    $arr=explode('~',iconv("gb2312", "utf-8//IGNORE",$shipanstock->Hq($gid)));
    if($arr[31]!='0.00'&&$arr[3]){
        if(strpos($arr[1],'ST')===0||strpos($arr[1],'ST')){
            return 201;
        }else{
            $str=$shipanstock->Stop($gid);
            $list=explode(',',substr($str,1,-1));
            foreach($list as $k=>$v){
                if(strpos($v,date('Y-m-d',time()-($time+1)*3600*24).' 15:00:00')){
                    $qclose=substr($list[$k+4],7,5);
                }
                if(strpos($v,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                    $yclose=substr($list[$k+4],7,5);
                }
            }
            if(!isset($yclose)){return 201;}
            if(!isset($qclose)){return 201;}
            $now=$arr[3];
            $today=($now-$yclose)/$yclose;
            $yesterday=($yclose-$qclose)/$qclose;
            if($today>$upstop){
              return 201;
            }
            if($today<$downstop){
              return 201;
            }
            if($dattime!=1){
              if($yesterday>$upstop){
                return 201;
              }
              if($yesterday<$downstop){
                return 201;
              }
            }
          	if($now<0.1){
            	return 201;
            }
            return 200;
        }
    }else{
        return 201;
    }
}

//获取是否是节假日
//工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2；
function isHoliday($i=0)
{

    $today = date('Ymd',time()-$i*24*3600);

    if (cache($today) !== false) {
        return cache($today);
    } else {
        $api2 = json_decode(https_request('http://api.goseek.cn/Tools/holiday?date='.$today),true)['data'];
        if (is_numeric($api2)) {
            if(!$api2&&(date('w',time()-$i*24*3600)<6&&date('w',time()-$i*24*3600)>0)){
                cache($today, $api2, 86400);
            }else{
                cache($today, 1, 86400);
            }
            return cache($today);
        } else {
            $api1 = https_request('http://tool.bitefu.net/jiari/?d='.$today);
            if (is_numeric($api1)) {
                if(!$api1&&(date('w',time()-$i*24*3600)<6&&date('w',time()-$i*24*3600)>0)){
                    cache($today, $api1, 86400);
                }else{
                    cache($today, 1, 86400);
                }
                return cache($today);
            } else {
                return -1;
            }
        }
    }
}

function json($list, $status, $msg,$para='') {
    $arr['status'] = $status;
    $arr['msg'] = $msg;
    $arr['list'] = $list;
    if($para){
        $arr['para'] = $para;
    }
    return json_encode($arr,JSON_UNESCAPED_UNICODE);
}

function token(){
    $info = Request::instance()->param(true);
    if(array_key_exists('token',$info)&&$info['token']){
        $adminer=base64_decode($info['token']);
        $account=explode('-',$adminer)[0];
        $hm=explode('-',$adminer)[1];
        if(Db::name('admin')->where(['account'=>$account,'lastlogin_time'=>$hm,'is_del'=>1])->find()){
            return 1;
        }else{
            return 100;
        }
    }
}

function logg($type,$content){
    Db::name('log')->insert(['type'=>$type, 'content'=>$content, 'create_time'=>time()]);
}


//推送(内部调用)
function push($account='',$content='',$push_type=1,$true='') {
	Db::name('push')->insert(['push_type'=>$push_type,'account'=>$account,'content'=>$content,'status'=>1,'is_del'=>1,'create_time'=>time()]);
}

function ulog($msg,$type,$file){
    $path = RUNTIME_PATH.'ulog/'.$file.'.log';
    $str = '['.date('Y-m-d H:i:s').'] '.$type.' '.$msg.PHP_EOL;
    file_put_contents($path,$str,FILE_APPEND);
}

?>
