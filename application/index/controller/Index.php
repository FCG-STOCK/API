<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use think\Loader;
use think\Cache;
use think\Session;
use Stock;

header('Content-Type:application/json; charset=utf-8');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,Authorization");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');

class Index extends controller {

    public function login() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $data['account'] = $info['account'];
        $data['pwd'] = $info['pwd'];

        if (!$data['account']) {
            return json('', 0, '用户名不能为空');
        }
        $rule = [
            'account' => '^1[3456789]\d{9}$',
            'pwd'     => 'require',
        ];
        $msg = [
            'account' => '用户名必须是手机号',
            'pwd'     => '密码不能为空',
        ];
        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)) return json('', 0, $validate->getError());
        $data['pwd'] = md5($data['pwd']);
        $data['is_del']=1;
      
      	$if_register = Db::name('admin')->where('account',$data['account'])->find();
      	if(!$if_register){
        	return json('', 0, '用户名不存在');
        }

        //查找登陆账号
        $admin = Db::name('admin')->where($data)->find();
        if (!$admin) return json('', 0, '用户名或密码错误');

        //最后登录时间
        $time=time();
        $res=Db::name('admin')->where(['id'=>$admin['id']])->update(['lastlogin_time'=>$time,'regid'=>$info['account']]);
        if($res==1){
            //最终查找登陆账号
            $admin = Db::name('admin')->where($data)->find();
            $admin['token']=base64_encode($admin['account'].'-'.$time);
            //推广人数
            $admin['extend_num'] = Db::name('admin')->where(['inviter'=>$admin['account'],'is_del'=>1])->count();
            $hideList = Db::name('hide')->column('value','id');
            //是否隐藏策略 1隐藏 2不隐藏
            $hide = isset($hideList[0]) ? $hideList[0] : 2;
            //策略可进行翻倍的倍数
            $times=isset($hideList[2]) ? $hideList[2] : 0;
            //T+1买卖的手续费比例
            $poundage=isset($hideList[4]) ? $hideList[4] : 0;
            //T+D买卖的手续费比例
            $bigpoundage=isset($hideList[8]) ? $hideList[8] : 0;
            //提现手续费
            $tixianpoundage=isset($hideList[10]) ? $hideList[10] : 0;
            //递延费比例
            $defer=isset($hideList[5]) ? $hideList[5] : 0;
            //信用金的倍数
            $credit_bs = isset($hideList[15]) ? $hideList[15] : 0;
            //最低信用金
            $credit_min = isset($hideList[16]) ? $hideList[16] : 0;
            //最高信用金
            $credit_max = isset($hideList[17]) ? $hideList[17] : 0;
            //允许亏损的信用金百分比
            $credit_kuishun = isset($hideList[18]) ? $hideList[18] : 0;

            $arr['option'] = [
                'hide'=>$hide,
                'times'=>$times,
                'poundage'=>$poundage,
                'bigpoundage'=>$bigpoundage,
                'defer'=>$defer,
                'tixianpoundage'=>$tixianpoundage,
                'credit_bs'=>$credit_bs,
                'credit_min'=>$credit_min,
                'credit_max'=>$credit_max,
                'credit_kuishun'=>$credit_kuishun,
            ];

            //会员创建时间
            $admin['create_time']=strval(date('YmdHis',$admin['create_time']));

            //会员充值后才会有银行卡 1为充值过，可以提现 2为没充值过，不可以提现
            $bankcart = Db::name('bankcart')->where(['account'=>$info['account'],'bank_require' => 1,'is_del'=>1])->find();
            if($bankcart['bank_num']){
                $admin['have_banknum']=strval($bankcart['bank_num']);
            }else{
                $admin['have_banknum']='';
            }

            return json($admin, 1, '登录成功',$arr);
        }else{
            return json('', 0, '修改最后登陆时间失败');
        }
    }

    //注册
    public function register() {
        $info = Request::instance()->param();
        if(!isset($info['Account'])||empty($info['Account'])) return json([],0,'参数有误#1');
        if(!isset($info['RegisterPassword'])||empty($info['RegisterPassword'])) return json([],0,'参数有误#2');
        //if(!isset($info['Inviter'])||empty($info['Inviter'])) return json([],0,'参数有误#3');
        //账号查重
        $user=Db::name('admin')->where(['account'=>$info['Account']])->find();
        if($user){return json('', 0, '账号已存在');}

        $apiurl=Db::name('hide')->where(['id'=>25])->value('value');
        if(empty($apiurl))json([],0,'系统错误，请联系管理员#1');

        $data['account'] = $info['Account'];
        $data['nick_name'] = $info['Account'];
        $data['pwd'] = $info['RegisterPassword'];
        $data['is_del'] = 1;
        $data['lastlogin_time'] = time();
        $data['create_time'] = time();
        $data['analog_money'] = 100000;
        $data['tactics_balance'] = 0;
        $data['frozen_analog_money'] = 0;
        $data['headurl'] = $apiurl.'/public/static/img/default.png';

        if (!$data['account']) {
            return json('', 0, '用户名不能为空');
        }
      	if (!$data['nick_name']) {
            return json('', 0, '昵称不能为空');
        }
      	if (!$info['Inviter']) {
            //return json('', 0, '邀请人不能为空');
        }
        $rule = [
            'account' => '^1[3456789]\d{9}$',
            'pwd'     => 'require',
            'inviter' => 'number',
        ];
        $msg = [
            'account' => '用户名必须是手机号',
            'pwd'     => '密码不能为空',
            'inviter' => '邀请人必须是数字',
        ];
        $validate = new Validate($rule, $msg);
        if (!$validate->check($data)) {
            return json('', 0, $validate->getError());
        }

        $data['pwd'] = md5($data['pwd']);

        //是否有邀请人,可填手机号或分销商工作人员编号,根据邀请人获得不同id
        if(isset($info['Inviter'])&&!empty($info['Inviter'])){
            //判断工作人员表中是否有此分销商工作人员编号
            $marketuser=Db::name('marketer_user')->where(['id'=>$info['Inviter'],'is_del'=>1])->find();
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['Inviter'],'is_del'=>1])->find();
            //判断邀请人
            if($marketuser){
                //最近一次受此工作人员推广的用户  例8010012
                $list=Db::name('admin')->where(['marketer_user'=>$info['Inviter'],'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']=substr($id,0,6).(substr($id,6)+1);
                }else{
                    $data['id']=$info['Inviter'].'1';
                }
                //添加分销商id和工作人员id
                $data['marketer_id']=substr($info['Inviter'],0,3);
                $data['marketer_user']=$info['Inviter'];
                //添加工作人员id到邀请人
                $data['inviter']=$info['Inviter'];
            }elseif($inviter){
                //最近一次没有分销商的普通用户  例0000002
                $list=Db::name('admin')->where(['marketer_id'=>0,'is_del'=>1])->select();
                array_multisort(array_column($list,'id'),SORT_DESC,$list);
                if($list){
                    $id=$list[0]['id'];
                    $data['id']='000000'.($id+1);
                }else{
                    $data['id']='0000001';
                }
                //添加手机号到邀请人
                $data['inviter']=$info['Inviter'];
            }else{
                return json('', 0, '邀请人不存在');
            }
        }else{
            //最近一次没有分销商的普通用户  例0000002
            $list=Db::name('admin')->field('id')->where(['marketer_id'=>0,'is_del'=>1])->select();
            array_multisort(array_column($list,'id'),SORT_DESC,$list);
            if($list){
                $id=$list[0]['id'];
                $data['id']='000000'.($id+1);
            }else{
                $data['id']='0000001';
            }
        }
        //添加会员
        $res = Db::name('admin')->insert($data);
        if (!$res) return json('', 0, '注册失败');

        //红包有效期
        $redday=Db::name('hide')->where(['id'=>11])->value('value');

        //发送红包
        if(array_key_exists('Inviter',$info)&&$info['Inviter']){
            //判断会员表中是否有此手机号
            $inviter=Db::name('admin')->where(['account'=>$info['Inviter'],'is_del'=>1])->find();
            if($inviter){
                //给这个邀请人发放1个红包
                $para=[
                    'account'=>$info['Inviter'],
                    'packet_name'=>'邀请奖励红包',
                    'packet_money'=>10,
                    'is_use'=>2,
                    'create_time'=>time(),
                    'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
                ];
                if(!Db::name('packet')->insert($para))ulog('红包发放失败：'.print_r($data,1),'ERROR','packet');
            }
        }
        $para = [];
        //新注册用户获得5个红包
        for($i=0;$i<5;$i++){
            $para[]=[
                'account'=>$info['Account'],
                'packet_name'=>'新人红包',
                'packet_money'=>10,
                'is_use'=>2,
                'create_time'=>time(),
                'daoqi_time'=>strtotime(date('Ymd',time()+$redday*24*3600)),
            ];
        }
        if(!Db::name('packet')->insertAll($para)) ulog('新人红包发放失败:'.print_r($para,1),'ERROR','packet');
        //添加个人设置
        $settings=Db::name('settings')->where(['account'=>$info['Account']])->find();
        if(!$settings){
            if(!Db::name('settings')->insert(['account'=>$info['Account']])) ulog('用户设置数据出错','ERROR','setting');
        }

        return json('', 1, '注册成功');
    }
    //我的页面,用户个人信息（用于资金改变后重新查询）
    public function mydetail() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#3');
        $admin = Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();

      	$admin['token']=base64_encode($admin['account'].'-'.$admin['lastlogin_time']);

        //推广人数
        $admin['extend_num'] = Db::name('admin')->where(['inviter'=>$admin['account'],'is_del'=>1])->count();
      
        //会员充值后才会有银行卡 1为充值过，可以提现 2为没充值过，不可以提现
        $bankcart = Db::name('bankcart')->where(['account'=>$info['account'],'bank_require' => 1,'is_del'=>1])->find();
        $admin['have_banknum']='';
        if($bankcart['bank_num'])$admin['have_banknum']=strval($bankcart['bank_num']);
        return json($admin, 1, '查询成功');
    }
    //IOS版本号
    public function getversionIOS() {
        $version=Db::name('hide')->where(['id' => 12])->value('value');
        $data['version']=$version;
        return json($data, 1, '获取成功');
    }
    //安卓版本号
    public function getversionAZ() {
        $version=Db::name('hide')->where(['id' => 13])->value('value');
        return json($version, 1, '获取成功');
    }
    //我的token
    public function mytoken() {
        $info = Request::instance()->param();
        $lastlogin_time = Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->value('lastlogin_time');
        $token=base64_encode($info['account'].'-'.$lastlogin_time);
        return json($token, 1, '查询成功');
    }
	//策略可进行翻倍的倍数，策略最低损失的信用金
  	public function multiple(){
        $multiple=Db::name('hide')->where(['id' => 2])->value('value');
      	$loss_credit=Db::name('hide')->where(['id' => 18])->value('value');
      	$zhiying=Db::name('hide')->where(['id' => 50])->value('value');
      	$is_zhiying=Db::name('hide')->where(['id' => 49])->value('value');
      	$is_zhiying==1 ? $is_zhiying=100 : $is_zhiying=$zhiying;
      	$list=[
        	'multiple'=>$multiple,
          	'loss_credit'=>$loss_credit,
          	'zhiying'=>$zhiying,
          	'is_zhiying'=>$is_zhiying,
        ];
      	return json($list, 1, '查询成功');
    }
    //修改密码 忘记密码
    public function modifyPwd() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if (!isset($info['account'])||empty($info['account'])) return json('', 0, '手机号不能为空');
        if (!isset($info['pwd'])||empty($info['pwd'])) return json('', 0, '密码不能为空');
        if (!isset($info['repwd']) || empty($info['repwd'])) return json('', 0, '确认密码不能为空');
        if ($info['pwd'] != $info['repwd']) return json('', 0, '两次输入密码不一致');
      	$if_register = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
      	if(!$if_register){
        	return json('', 0, '用户名不存在'); 
        }
        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['pwd' => md5($info['pwd'])]);
        if(!$res) return json([], 0, '修改失败');
        return json('', 1, '修改成功');
    }

    //退出应用，清空regid
    public function exitApp(){
        $info = Request::instance()->param();
        Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update(['regid'=>'']);
        return json('', 1, '退出成功');
    }

    //上传头像
    public function uploadhead(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);

        //创建文件
        $filename = time().'.jpg';
        //文件夹路径
        $url="./public/uploads/".date('Ymd');
        //创建文件夹
        if(!is_dir($url)){
            mkdir($url,0777,true);
        }
        //写入文件
        $file = fopen($url."/".$filename,"w");//打开文件准备写入
        fwrite($file,base64_decode($info['headurl']));//写入
        fclose($file);//关闭

        $apiurl=Db::name('hide')->where(['id' => 25])->value('value');

        $data['headurl'] = $apiurl."/public/uploads/".date('Ymd')."/".$filename;
        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        if ($res == 1) {
            return json($data, 1, '上传成功');
        } else {
            return json('', 0, '上传失败');
        }
    }
    //上传头像PC
    public function uploadheadPC(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);

        $apiurl=Db::name('hide')->where(['id' => 25])->value('value');

        //头像
        $file = request()->file('headurl');
        if($file){
            $name1 = $file->getInfo()['name'];
            //中文文件名用此行代码转码
            //$name2=iconv('utf-8','gbk',$name1);
            $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
            if($move){
                $data['headurl']= $apiurl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
            }else{
                echo $file->getError();
            }
        }

        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        if ($res == 1) {
            return json($data, 1, '上传成功');
        } else {
            return json('', 0, '上传失败');
        }
    }
    //我的推广的链接
    public function extend_url(){
        $extend_url=Db::name('hide')->where(['id'=>28])->value('value').'/index/down/registerh5?register=';
        return json($extend_url, 1, '查询成功');
    }
    //PC合作伙伴
    public function partner(){
        $list = Db::name('web_spe')->field('spe_img')->where('is_del',1)->select();
        return json($list, 1, '查询成功');
    }

    //会员中心实名认证
    public function authentication(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#4');
        //API接口地址
        $apiurl=Db::name('hide')->where(['id'=>25])->value('value');
        //身份证正面
        //创建文件
        $filename = $info['account'].'front.jpg';
        //文件夹路径
        $url="./public/uploads/".date('Ymd');
        //创建文件夹
        if(!is_dir($url)){
            mkdir($url,0777,true);
        }
        //写入文件
        $file = fopen($url."/".$filename,"w");//打开文件准备写入
        fwrite($file,base64_decode($info['frontcard']));//写入
        fclose($file);//关闭
        $data['frontcard'] = $apiurl."/public/uploads/".date('Ymd')."/".$filename;

        //身份证背面
        //创建文件
        $filename = $info['account'].'back.jpg';
        //文件夹路径
        $url="./public/uploads/".date('Ymd');
        //创建文件夹
        if(!is_dir($url)){
            mkdir($url,0777,true);
        }
        //写入文件
        $file = fopen($url."/".$filename,"w");//打开文件准备写入
        fwrite($file,base64_decode($info['backcard']));//写入
        fclose($file);//关闭
        $data['backcard'] = $apiurl."/public/uploads/".date('Ymd')."/".$filename;

        $data['true_name']=$info['true_name'];
        $data['card_id']=$info['card_id'];
        $data['is_true']=3;

        Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update($data);
        return json('', 1, '提交成功');
    }
  
  
  	//会员中心实名认证
    public function authentication_ios(){
      	if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}
        $info = Request::instance()->param();   
        $data['true_name'] = $info['true_name'];//名字
        $data['card_id'] = $info['card_id'];//身份证号
        $data['frontcard'] = $info['frontcard'];//身份证正面照
        $data['backcard'] = $info['backcard'];//身份证反面照
        $data['is_true'] = '3';//实名认证待审核
        
        Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update($data);
        return json('', 1, '提交成功');
    }

    //会员中心实名认证PC
    public function authenticationPC(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);
        if(!isset($info['true_name'])||empty($info['true_name'])) return json([],0,'参数有误#5');
        if(!isset($info['card_id'])||empty($info['card_id'])) return json([],0,'参数有误#6');
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#7');
        //API接口地址
        $apiurl=Db::name('hide')->where(['id'=>25])->value('value');

        //身份证正面
        $file = request()->file('frontcard');
        file_put_contents('xuxux.txt',json_encode($file,320));
        if($file){
            $name1 = $file->getInfo()['name'];
            //中文文件名用此行代码转码
            //$name2=iconv('utf-8','gbk',$name1);
            $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
            if($move){
                $data['frontcard']= $apiurl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
            }else{
                echo $file->getError();
            }
        }

        //身份证背面
        $file = request()->file('backcard');
        if($file){
            $name1 = $file->getInfo()['name'];
            //中文文件名用此行代码转码
            //$name2=iconv('utf-8','gbk',$name1);
            $move = $file->validate(['ext'=>'jpg,jpeg,png'])->rule('date')->move(ROOT_PATH . 'public' . DS . 'uploads' . DS .date('Ymd',time()),$name1);
            if($move){
                $data['backcard']= $apiurl.str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .date('Ymd',time()) . DS . $name1);
            }else{
                echo $file->getError();
            }
        }

        $data['true_name']=$info['true_name'];
        $data['card_id']=$info['card_id'];
        $data['is_true']=3;

        $res = Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update($data);
        if(!$res) return json([],0,'提交失败');
        return json('', 1, '提交成功');
    }

    //修改昵称
    public function nickname() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $data['nick_name'] = $info['nickname'];
        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }

    //我的推广
    public function myextend() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $list=Db::name('admin')->where(['inviter'=>input('account'),'is_del'=>1])->select();
        foreach ($list as $k => &$v) {
            $v['create_time'] = date('Y-m-d',$v['create_time']);
            $v['lastlogin_time'] = date('Y-m-d',$v['lastlogin_time']);
        }
        unset($v);
        $extend['count']=count($list);
        $extend['list']=$list;
        return json($extend, 1, '查询成功');
    }
    //是否有新消息
    public function isHavePush() {
        $info = Request::instance()->param();
        $list=Db::name('push')->where(['account' => $info['account'],'status'=>1,'is_del'=>1])->select();
        $arr['all']=count($list);
        $arr = ['xitong'=>0,'hongbao'=>0,'celue'=>0,'dingyue'=>0];
        foreach ($list as $k => $v) {
            if($v['push_type']==1){
                $arr['xitong']+=1;
            }elseif($v['push_type']==2){
                $arr['hongbao']+=1;
            }elseif($v['push_type']==3){
                $arr['celue']+=1;
            }elseif($v['push_type']==4){
                $arr['dingyue']+=1;
            }
        }
        return json($arr, 1, '查询成功');
    }
    //消息中心系统消息
    public function systemPush() {
        $info = Request::instance()->param();
        $list=Db::name('push')->where(['account' => $info['account'],'status'=>1,'is_del'=>1])->order('create_time desc')->limit(30)->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);}
        return json($list, 1, '查询成功');
    }
    //消息中心红包消息
    public function packetPush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>2,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>2,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d',$v['create_time']);}
        return json($list, 1, '查询成功');
    }
    //消息中心策略消息
    public function strategyPush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>3,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>3,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d',$v['create_time']);}
        return json($list, 1, '查询成功');
    }
    //消息中心订阅消息
    public function subscribePush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>4,'status'=>1,'is_del'=>1])->update(['is_del'=>2]);
        $list=Db::name('push')->where(['account' => $info['account'],'push_type'=>4,'status'=>1,'is_del'=>2])->order('create_time desc')->select();
        foreach ($list as $k => $v) {$list[$k]['create_time']=date('Y-m-d',$v['create_time']);}
        return json($list, 1, '查询成功');
    }
    //清空一个消息
    public function clearOnePush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['id' => $info['id']])->update(['is_del'=>3]);
        return json('', 1, '清空成功');
    }
    //清空所有的消息中心系统消息
    public function clearSystemPush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>1,'status'=>1])->update(['is_del'=>3]);
        return json('', 1, '清空成功');
    }
    //清空所有的消息中心红包消息
    public function clearPacketPush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>2,'status'=>1])->update(['is_del'=>3]);
        return json('', 1, '清空成功');
    }
    //清空所有的消息中心策略消息
    public function clearStrategyPush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>3,'status'=>1])->update(['is_del'=>3]);
        return json('', 1, '清空成功');
    }
    //清空所有的消息中心订阅消息
    public function clearSubscribePush() {
        $info = Request::instance()->param();
        Db::name('push')->where(['account' => $info['account'],'push_type'=>4,'status'=>1])->update(['is_del'=>3]);
        return json('', 1, '清空成功');
    }
    
    //发送短信
    public function sendMessage() {
        $mobile = input('mobile');
        $rand = rand(1000, 9999);
        $json_data=sendMessage($rand,$mobile);
        $array = json_decode($json_data, true);
        if (isset($array['status']) && $array['status'] == 1) {
            return json($list['rand'] = strval(md5($rand)), 1, '发送成功');
        } else {
            return json('', 0, '发送失败');
        }
    }

	//大盘股指实时行情_批量 new
    public function grailindex() {
        $shipanstock=new Stock();
        if(date('Hi')>=930){
            $res1=$shipanstock->grailindex1();
            $res2=$shipanstock->grailindex2();
            $res3=$shipanstock->grailindex3();

            $arr1=explode(",",explode("\"",$res1)[1]);
            $arr2=explode(",",explode("\"",$res2)[1]);
            $arr3=explode(",",explode("\"",$res3)[1]);

            $stock=[
                'showapi_res_body'=>[
                    'indexList'=>[
                        [
                            'name'=>'上证指数',
                            'nowPoint'=>round($arr1[1],2),
                            'nowPrice'=>round($arr1[2],2),
                            'diff_rate'=>round($arr1[3],2),
                        ],[
                            'name'=>'深证成指',
                            'nowPoint'=>round($arr2[1],2),
                            'nowPrice'=>round($arr2[2],2),
                            'diff_rate'=>round($arr2[3],2),
                        ],[
                            'name'=>'创业板指',
                            'nowPoint'=>round($arr3[1],2),
                            'nowPrice'=>round($arr3[2],2),
                            'diff_rate'=>round($arr3[3],2),
                        ]
                    ]
                ]
            ];
        }else{
            $res1=$shipanstock->Hq_real('sh000001');
            $res2=$shipanstock->Hq_real('sz399001');
            $res3=$shipanstock->Hq_real('sz399006');

            $arr1=json_decode($res1,true)['result'][0]['data'];
            $arr2=json_decode($res2,true)['result'][0]['data'];
            $arr3=json_decode($res3,true)['result'][0]['data'];

            $stock=[
                'showapi_res_body'=>[
                    'indexList'=>[
                        [
                            'name'=>'上证指数',
                            'nowPoint'=>round($arr1['nowPri'],2),
                            'nowPrice'=>round($arr1['increase'],2),
                            'diff_rate'=>round($arr1['increPer']*100,2),
                        ],[
                            'name'=>'深证成指',
                            'nowPoint'=>round($arr2['nowPri'],2),
                            'nowPrice'=>round($arr2['increase'],2),
                            'diff_rate'=>round($arr2['increPer']*100,2),
                        ],[
                            'name'=>'创业板指',
                            'nowPoint'=>round($arr3['nowPri'],2),
                            'nowPrice'=>round($arr3['increase'],2),
                            'diff_rate'=>round($arr3['increPer']*100,2),
                        ]
                    ]
                ]
            ]; 
        }
        
        return json($stock, 1, '查询成功');
    }
    //涨跌幅榜
    public function updown_stock(){
        if(cache('up_stock')){
            $str=cache('up_stock');
        }else{
            
          
          	$stock=new Stock();
            $str=$stock->zhangfu();
          
            cache('up_stock',$str,60);
        }
        $str=iconv("gb2312", "utf-8//IGNORE",$str);
        $arr = explode('},', substr($str, 1,-1));

        $new_arr = [];
        foreach ($arr as $k => $v) {
            $shuzu=explode(",", substr($v, 1));
            $new_arr[$k]['name'] = substr(explode(":", $shuzu[2])[1],1,-1);
            $new_arr[$k]['trade'] = substr(explode(":", $shuzu[3])[1],1,-1);
            $new_arr[$k]['changepercent'] = substr(explode(":", $shuzu[5])[1],1,-1);
            $new_arr[$k]['code'] = substr(explode(":", $shuzu[0])[1],1,-1);
        }
        foreach ($new_arr as $k => $v) {
            if($k>12){
                unset($new_arr[$k]);
            }
        }

        if(cache('down_stock')){
            $str2=cache('down_stock');
        }else{
            $str2 = file_get_contents("http://vip.stock.finance.sina.com.cn/quotes_service/api/json_v2.php/Market_Center.getHQNodeData?page=1&num=40&sort=changepercent&asc=1&node=hs_a&symbol=&_s_r_a=init");
            cache('down_stock',$str2,60);
        }
        $str2=iconv("gb2312", "utf-8//IGNORE",$str2);
        $arr2 = explode('},', substr($str2, 1,-1));

        $new_arr2 = [];
        foreach ($arr2 as $k => $v) {
            $shuzu=explode(",", substr($v, 1));
            $new_arr2[$k]['name'] = substr(explode(":", $shuzu[2])[1],1,-1);
            $new_arr2[$k]['trade'] = substr(explode(":", $shuzu[3])[1],1,-1);
            $new_arr2[$k]['changepercent'] = substr(explode(":", $shuzu[5])[1],1,-1);
            $new_arr2[$k]['code'] = substr(explode(":", $shuzu[0])[1],1,-1);
        }

        foreach ($new_arr2 as $k => $v) {
            if($k>12){
                unset($new_arr2[$k]);
            }
        }

        return json($new_arr, 1, '成功', $new_arr2);
    }

    //获取单个股票数据(包括市盈率)
    public function stockDetail() {
        $list=stockDetail(input('gid'));
        return json($list, 1, '查询成功');
    }
      
    //获取单个股票数据
    public function getOneStock() {
        $list=getOneStock(input('gid'));
        return json($list, 1, '查询成功');
    }

    //首页banner图
    public function bannerList() {
        $list=Db::name('banner')->where(['is_del'=>1,'ban_id'=>1])->order('sort desc,create_time desc')->select();
        return json($list, 1, '查询成功');
    }
    //策略banner图
    public function bannerList_ce() {
        $list=Db::name('banner')->where(['is_del'=>1,'ban_id'=>2])->order('sort desc,create_time desc')->find();
        return json($list, 1, '查询成功');
    }
    //排行banner图
    public function bannerList_pai() {
        $list=Db::name('banner')->where(['is_del'=>1,'ban_id'=>3])->order('sort desc,create_time desc')->find();
        return json($list, 1, '查询成功');
    }
  	//首页banner图PC
    public function bannerListPC() {
        $list=Db::name('web_banner')->where(['is_del'=>1,'type'=>1])->order('sort desc,create_time desc')->select();
        return json($list, 1, '查询成功');
    }
  	//新闻banner图PC
    public function news_bannerListPC() {
        $list=Db::name('web_banner')->where(['is_del'=>1,'type'=>2])->order('sort desc,create_time desc')->select();
        return json($list, 1, '查询成功');
    }
  	//炒股大赛banner图PC
    public function sai_bannerListPC() {
        $list=Db::name('web_banner')->where(['is_del'=>1,'type'=>3])->order('sort desc,create_time desc')->select();
        return json($list, 1, '查询成功');
    }
  	//操作指南banner图PC
    public function zhinan_bannerListPC() {
        $list=Db::name('web_banner')->where(['is_del'=>1,'type'=>4])->order('sort desc,create_time desc')->select();
        return json($list, 1, '查询成功');
    }
    //首页公告列表
    public function noticeList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $notice = Db::name('news')->field('id,title,create_time')->where(['cid'=>6,'is_del'=>1])->order('sort desc,create_time desc')->select();
        foreach($notice as $k=>$v){
            $notice[$k]['create_time']=date('Y-m-d',$v['create_time']);
        }
        return json($notice, 1, '查询成功');
    }
    //更多的公告列表
    public function allnoticeList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);

        $geshu = 5;//每页个数
        if(array_key_exists('page',$info)&&$info['page']){
            $rule = ['page' =>  "^[0-9]*[1-9][0-9]*$"];
            $msg = ['page' =>  '页数必须为数字'];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($info)){
                return json('', 0, $validate->getError());
            }
        }else{
            $info['page']=1;
        }

        //总新闻数量
        $count = Db::name('news')
            ->where(['cid'=>6,'is_del'=>1])
            ->count();
        if($count){
            $allyeshu=ceil($count/$geshu);
        }else{
            $allyeshu=floatval(1);
        }

        if($info['page']>$allyeshu){
            return json('', 0, '页数不存在');
        }

        $begin=$geshu*($info['page']-1);
        $notice = Db::name('news')
            ->field('id,title,synopsis,words,picurl,create_time')
            ->where(['cid'=>6,'is_del'=>1])
            ->order('sort desc,create_time desc')
            ->limit($begin,$geshu)
            ->select();
        foreach($notice as $k=>$v){
            $notice[$k]['create_time']=date('Y-m-d',$v['create_time']);
        }
        $list=[
            'allnum'=>$count,
            'geshu'=>$geshu,
            'list'=>$notice,
        ];
        return json($list, 1, '查询成功');
    }
    //首页新闻列表
    public function newsIndex() {
        $info = Db::name('news')->where(['is_recommend' => 1,'is_del'=>1,'cid'=>['neq',6]])->order('sort desc,create_time desc')->limit(15)->select();
        foreach ($info as $k => $v) {
            $info[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $info[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $info[$k]['picurl'][$kk]=$vv;
            }
            $info[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
        }
        return json($info, 1, '查询成功');
    }
    //APP新闻列表页，传分类id
    public function newsCateList() {
        $info = Request::instance()->param();
        $list = Db::name('news')->where(['cid' => $info['Cid'],'is_del'=>1])->order('sort desc,create_time desc')->select();
        foreach($list as $k => $v){
            $list[$k]['create_time'] = date('Y-m-d', $v['create_time']);
            $arr=explode(',',substr($v['picurl'],0,-1));
            $list[$k]['picurl']=[];
            foreach($arr as $kk=>$vv){
                $list[$k]['picurl'][$kk]=$vv;
            }
            $list[$k]['content']=str_replace('"','\'',htmlspecialchars($v['content'],ENT_NOQUOTES));
            $list[$k]['is_new']='';
            if($k<=4){
                $list[$k]['is_new']=1;
            }else{
                $list[$k]['is_new']=2;
            }
        }
        return json($list, 1, '查询成功');
    }
    //新闻分类
    public function newsCategory() {
        $category = Db::name('news_category')->where(['cid'=>['neq',6]])->select();
        return json($category, 1, '查询成功');
    }
    //PC新闻列表
    public function allnewsListPC(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param(true);

        $geshu = 5;//每页个数
        if(array_key_exists('page',$info)&&$info['page']){
            $rule = ['page' =>  "^[0-9]*[1-9][0-9]*$"];
            $msg = ['page' =>  '页数必须为数字'];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($info)){
                return json('', 0, $validate->getError());
            }
        }else{
            $info['page']=1;
        }

        //总新闻数量
        $count = Db::name('news')
            ->where(['cid'=>['neq',6],'is_del'=>1])
            ->count();
        if($count){
            $allyeshu=ceil($count/$geshu);
        }else{
            $allyeshu=floatval(1);
        }

        if($info['page']>$allyeshu){
            return json('', 0, '页数不存在');
        }

        $begin=$geshu*($info['page']-1);
        $notice = Db::name('news')
            ->field('id,title,synopsis,words,picurl,create_time')
            ->where(['cid'=>['neq',6],'is_del'=>1])
           ->order('sort desc,create_time desc')
            ->limit($begin,$geshu)
            ->select();
        foreach($notice as $k=>$v){
            $notice[$k]['create_time']=date('Y-m-d');
        }
        $list=[
            'allnum'=>$count,
            'geshu'=>$geshu,
            'list'=>$notice,
        ];
        return json($list, 1, '查询成功');
    }
    //新闻详情页
    public function newsdetail() {
        //浏览次数加1
        Db::name('news')->where(['id' => input('id')])->setInc('times');
        $info = Db::name('news')->where(['id' => input('id')])->find();
        $info['create_time'] = date('Y-m-d H:i:s', $info['create_time']);
        return json($info, 1, '查询成功');
    }
    //PC新闻详情页
    public function newsdetailPC() {
        $id=input('id');
        $news = Db::name('news')->where(['id' => $id])->find();
        if(!$news){return json('', 0, '参数错误');}

        Db::name('news')->where(['id' => $id])->setInc('times');   //浏览次数加1
        $news['create_time'] = date('Y-m-d H:i:s', $news['create_time']);
        $list['list']=$news;

        //上一篇新闻
        $up = Db::name('news')
            ->field('id,title,content,create_time')
            ->where(['cid'=>$news['cid'],'is_del'=>1,'id'=>['lt',$id]])
            ->order('id desc')
            ->limit(1)
            ->find();
        //下一篇新闻
        $down = Db::name('news')
            ->field('id,title,content,create_time')
            ->where(['cid'=>$news['cid'],'is_del'=>1,'id'=>['gt',$id]])
            ->order('id asc')
            ->limit(1)
            ->find();

        empty($up) ? $list['up']=['id'=>0,'title'=>'暂无'] : $list['up']=['id'=>$up['id'],'title'=>$up['title']];
        empty($down) ? $list['down']=['id'=>0,'title'=>'暂无'] : $list['down']=['id'=>$down['id'],'title'=>$down['title']];

        return json($list, 1, '查询成功');
    }
    //查看我的推广
    public function extendIndex() {

        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if(!isset($info['admin_id'])||empty($info['admin_id'])) return json([],0,'参数有误#5');
        $bankcart = Db::name('extend')
            ->alias('ex')
            ->field('ad.account,ad.balance,ad.tactics_balance,ad.card_id,ad.nick_name,ad.true_name,ad.is_del,ex.id,ex.create_time')
            ->join('link_admin ad', 'ad.id=ex.extend_id')
            ->where(['ex.admin_id' => $info['admin_id']])
            ->select();
        if ($bankcart) {
            return json($bankcart, 1, '查询成功');
        } else {
            return json('', 0, '查询失败');
        }
    }
    //判断股票是否停盘（股票页面）
    public function checkstockstop() {
        $res=thischeckstockstop(input('gid'));
        if($res==200){
            return json('', 200, '查询成功');
        }else{
            return json('', 201, '昨日或今日达到涨停');
        }
    }
    //查看后台添加的模拟策略
    public function strategyAnalog() {
        $zixuan = Db::name("strategy_analog")->where(["is_del"=>1])->order('create_time desc')->select();
        if($zixuan){
            foreach($zixuan as $k=>$v){
                $zixuan[$k]['create_time']=$this->gettimeago($v['create_time']);
                $zixuan[$k]['account']=substr($v['account'],0,3).'****'.substr($v['account'],7);
            }
        }
        return json($zixuan, 1, '查询成功');
    }
    //获取距离现在时间的长度
    public function gettimeago($time){
        $range=time()-$time;
        if($range<60){
            $content=$range.'秒前';
        }elseif($range<3600){
            $minute=floor($range/60);
            $content=$minute.'分钟前';
        }elseif($range<3600*24){
            $hour=floor($range/3600);
            $content=$hour.'小时前';
        }elseif($range<3600*24*30){
            $day=floor($range/(3600*24));
            $content=$day.'天前';
        }elseif($range<3600*24*30*12){
            $month=floor($range/(3600*24*30));
            $content=$month.'月前';
        }else{
            $year=floor($range/(3600*24*30*12));
            $content=$year.'年前';
        }
        return $content;
    }
  
   	//月K
  	public function Kmonth(){
        $stock = new Stock;
        $stock_data = $stock->Kmonth(input('code'));
      	return $stock_data;
    }
  	//周K
    public function Kweek(){
        $stock = new Stock;
        $stock_data = $stock->Kweek(input('code'));
      	return $stock_data;
    }
  	//日K
  	public function kday(){
        $stock = new Stock;
        $stock_data = $stock->Kday(input('code'));
        return $stock_data;
    }
  	//分时
    public function minhour(){
        $stock = new Stock;
        $stock_data = $stock->Minhour(input('code'));
        return $stock_data;
    }
  
    //查看我的自选
    public function myChooseStock() {
        $info = Request::instance()->param();
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#7');
        $zixuan = Db::name("stock_zixuan")->where(['account'=>$info['account'],"is_del"=>1])->order('id desc')->limit(6)->select();
        if($zixuan){
            $temp = array_column($zixuan,'stock_code');
            $hot_code = implode(',',$temp);
            $shipanstock=new Stock();
          	if(date('Hi')>930){
                $result=$shipanstock->Hq_list($hot_code);
                $arr=explode(';',$result);
                unset($arr[count($arr)-1]);
                foreach($arr as $k=>$v){
                    $zixuan[$k]['nowprice']=explode(',',$v)[3];
                    $zixuan[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $result=$shipanstock->Hq_real_list($hot_code);
                $arr=json_decode($result,true);
                foreach($arr as $k=>$v){
                    $zixuan[$k]['nowprice']=$v['nowPri'];
                    $zixuan[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }
        return json($zixuan, 1, '查询成功');
    }
  	//判断是否是我的自选
    public function judgeChooseStock() {
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#8');
        if(!isset($info['stock_code'])||empty($info['stock_code'])) return json([],0,'参数有误#9');
        $info = Request::instance()->param();
        if(Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find()){
            return json('', 1, '属于我的自选');
        }else{
            return json('', 0, '不属于我的自选');
        }
    }
    //添加至我的自选
	public function addChooseStock() {
        $info = Request::instance()->param();
        if(!isset($info['stock_code'])||empty($info['stock_code'])) return json([],0,'参数有误#10');
        if(!isset($info['stock_name'])||empty($info['stock_name'])) return json([],0,'参数有误#11');
        if(!isset($info['account'])||empty($info['account'])) return json([],0,'参数有误#12');
        if(!Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find()){
            $data=[
                'account'=>$info['account'],
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
            ];
            $res = Db::name('stock_zixuan')->insert($data);
            if(!$res) return json([],1,'添加失败');
        }
        return json('', 1, '添加成功');
    }
    //删除某个我的自选
    public function deleteChooseStock() {
        $info = Request::instance()->param();
        $res = Db::name('stock_zixuan')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code']])->update(['is_del'=>2]);
        if(!$res) return json([],0,'更新失败');
        return json('', 1, '删除成功');
    }
    //清空我的自选
    public function deleteAllChooseStock() {
        $info = Request::instance()->param();
        $res = Db::name('stock_zixuan')->where(['account'=>$info['account']])->update(['is_del'=>2]);
        if(!$res) return json([],0,'更新失败');
        return json('', 1, '清空成功');
    }
    //添加至我的历史搜索
    public function addHistoryStock() {
        $info = Request::instance()->param();
        if(!Db::name('stock_search')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code'],'is_del'=>1])->find()){
            $data=[
                'account'=>$info['account'],
                'stock_name'=>$info['stock_name'],
                'stock_code'=>$info['stock_code'],
            ];
            $res = Db::name('stock_search')->insert($data);
            if(!$res) return json([],0,'添加失败');
        }
        return json('', 1, '添加成功');
    }
    //删除某个我的历史搜索
    public function deleteHistoryStock() {
        $info = Request::instance()->param();
        $res = Db::name('stock_search')->where(['account'=>$info['account'],'stock_code'=>$info['stock_code']])->update(['is_del'=>2]);
        if(!$res) return json([],0,'删除失败');
        return json('', 1, '删除成功');
    }
    //清空我的历史搜索
    public function deleteAllHistoryStock() {
        $info = Request::instance()->param();
        $res = Db::name('stock_search')->where(['account'=>$info['account']])->update(['is_del'=>2]);
        if(!$res) return json([],0,'清空失败');
        return json('', 1, '清空成功');
    }
	//查看热门股票
    public function hot_stock(){
      	$shipanstock=new Stock();
      	$hot = Db::name("stock_recommend")->where(["is_recommend"=>1])->order('id desc')->limit(6)->select();
        if($hot){
            $temp = array_column($hot,'stock_code');
            $hot_code = implode(',',$temp);
            if(date('Hi')>930){
                $hot_result=$shipanstock->Hq_list($hot_code);
                $hot_arr=explode(';',$hot_result);
                unset($hot_arr[count($hot_arr)-1]);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $hot[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $hot[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $hot_result=$shipanstock->Hq_real_list(substr($hot_code,0,-1));
                $hot_arr=json_decode($hot_result,true);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round($v['nowPri'],2);
                    $hot[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $hot[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }
      	return json($hot, 1, '查询成功');
    }
    //股票模糊搜索页面
    public function search_stock_list(){
        $shipanstock=new Stock();
        $info = Request::instance()->param();
        //热门股票 显示3条
        $hot = Db::name("stock_recommend")->where(["is_recommend"=>1])->order('id desc')->limit(3)->select();
        if($hot){
            $temp = array_column($hot,'stock_code');
            $hot_code = implode(',',$temp);
            if(date('Hi')>930){
                $hot_result=$shipanstock->Hq_list($hot_code);
                $hot_arr=explode(';',$hot_result);
                unset($hot_arr[count($hot_arr)-1]);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $hot[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $hot[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $hot_result=$shipanstock->Hq_real_list($hot_code);
                $hot_arr=json_decode($hot_result,true);
                foreach($hot_arr as $k=>$v){
                    $hot[$k]['nowprice']=round($v['nowPri'],2);
                    $hot[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $hot[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }

        //历史搜索 显示3条
        $search = Db::name("stock_search")->where(['account'=>$info['account'],'is_del'=>1])->order('id desc')->limit(10)->select();
        if($search){
            $temp = array_column($search,'stock_code');
            $search_code = implode(',',$temp);
            if(date('Hi')>930){
                $search_result=$shipanstock->Hq_list($search_code);
                $search_arr=explode(';',$search_result);
                unset($search_arr[count($search_arr)-1]);
                foreach($search_arr as $k=>$v){
                    $search[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $search[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $search[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                }
            }else{
                $search_result=$shipanstock->Hq_real_list(substr($search_code,0,-1));
                $search_arr=json_decode($search_result,true);
                foreach($search_arr as $k=>$v){
                    $search[$k]['nowprice']=round($v['nowPri'],2);
                    $search[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $search[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                }
            }
        }
        return json($hot, 1, '查询成功', $search);
    }
    //股票进行模糊搜索
    public function search_stock(){
        $info = Request::instance()->param();

        $stock_new = new Stock;

        //可买股票类型
        $canbuy=Db::name('hide')->where(['id'=>23])->value('value');//sz00,sz30,sh60
        $canbuyarr=explode(',',$canbuy);
        //不可买股票类型
        $cannotbuy=Db::name('hide')->where(['id'=>24])->value('value');//st,pt
        $cannotbuyarr=explode(',',$cannotbuy);
        //不可买的具体股票
        $forbidden_list = Db::name('forbidden_list')->where('is_del',1)->select();

        $stock=[];
        $str = $stock_new->Tosearch($info['content']);
        $str=iconv("gb2312", "utf-8//IGNORE",$str);
        $list=explode(';',explode("\"",$str)[1]);
        foreach($list as $k=>$v){
            $stockcode=explode(',',$v)[3];
            $stockname=explode(',',$v)[4];
            //排除A股之外的基金期货等
            if(strlen($stockcode)==8){
                if($canbuy){
                    //包含可买股票则可以购买
                    $result1=0;
                    foreach($canbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result1=1;
                        }
                    }
                }else{
                    //可买股票类型为空，全都能买
                    $result1=1;
                }
                if($cannotbuy){
                    //包含不可买股票则不可以购买
                    $result2=1;
                    foreach($cannotbuyarr as $kk=>$vv){
                        if(strpos($stockcode,$vv)||strpos($stockcode,$vv)===0){
                            $result2=0;
                        }
                    }
                }else{
                    //不可买股票类型为空，全都能买
                    $result2=1;
                }
                if($forbidden_list){
                    $result3=1;
                    foreach($forbidden_list as $kk=>$vv){
                        if($stockcode==$vv['stock_code']){
                            $result3=0;
                        }
                    }
                }else{
                    $result3=1;
                }
                //两种规则都满足则可以购买
                if($result1==1&&$result2==1&&$result3==1){
                    $stock[]=[
                        'stock_code'=>$stockcode,
                        'stock_name'=>$stockname,
                    ];
                }
            }
        }

        if($stock){
            $code='';
            foreach($stock as $k=>$v){
                $code.=$v['stock_code'].',';
            }

            if(date('Hi')>930){
            	$result=$stock_new->hq_list(substr($code,0,-1));
                $pp=explode(';',$result);
                unset($pp[count($pp)-1]);
                foreach($pp as $k=>$v){
                    $stock[$k]['nowprice']=round(explode(',',$v)[3],2);
                    $stock[$k]['rate_price']=round(explode(',',$v)[3]-explode(',',$v)[2],2);
                    $stock[$k]['rate']=(round((explode(',',$v)[3]-explode(',',$v)[2])/explode(',',$v)[2],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }else{
            	$result=$stock_new->Hq_real_list(substr($code,0,-1));
                $arr=json_decode($result,true);
                foreach($arr as $k=>$v){
                    $stock[$k]['nowprice']=round($v['nowPri'],2);
                    $stock[$k]['rate_price']=round($v['nowPri']-$v['closePrice'],2);
                    $stock[$k]['rate']=(round(($v['nowPri']-$v['closePrice'])/$v['closePrice'],4)*100).'%';
                    if(!$stock[$k]['rate_price']){
                        unset($stock[$k]);
                    }
                }
            }
            $stock=array_values($stock);
        }

        return json($stock, 1, '查询成功');
    }
    //设置交易密码 修改交易密码
    public function setTradePwd() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if(!$info['trade_pwd']){
            return json('', 0, '交易密码为空');
        }
        $data['trade_pwd'] = md5($info['trade_pwd']);
        Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        return json('', 1, '修改成功');
    }
    //设置交易密码 修改交易密码
    public function setTradePwdPC() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if(!$info['trade_pwd']){
            return json('', 0, '交易密码为空');
        }

        if(array_key_exists('trade_pwd_reply',$info)&&$info['trade_pwd_reply']){
            if($info['trade_pwd']!=$info['trade_pwd_reply']){
                return json('', 0, '交易密码输入不一致');
            }
        }

        if(array_key_exists('login_pwd',$info)&&$info['login_pwd']){
            $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
            if($admin['pwd']!=md5($info['login_pwd'])){
                return json('', 0, '登陆密码错误');
            }
        }

        $data['trade_pwd'] = md5($info['trade_pwd']);
        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update($data);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //我的设置
    public function mySettings(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $settings=Db::name('settings')->where(['account' => $info['account']])->find();
        return json($settings, 1, '查询成功');
    }
    //接收系统消息的设置
    public function systemSettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['system_push'=>$info['system_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //收到红包消息的设置
    public function packetSettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['packet_push'=>$info['packet_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //分享红包提醒消息的设置
    public function sharingSettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['sharing_push'=>$info['sharing_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //到期平仓推送的设置
    public function sellStrategySettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['sell_strategy_push'=>$info['sell_strategy_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //递延提醒推送的设置
    public function deferSettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['defer_push'=>$info['defer_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //触发止盈推送的设置
    public function surplusStrategySettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['surplus_strategy_push'=>$info['surplus_strategy_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //触发止损推送的设置
    public function lossStrategySettings(){
        $info = Request::instance()->param();
        $res = Db::name('settings')->where(['account' => $info['account']])->update(['loss_strategy_push'=>$info['loss_strategy_push']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //实盘信息是否开放
    public function isOpenRanking(){
        $info = Request::instance()->param();
        $res = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['is_open_ranking'=>$info['is_open_ranking']]);
        if(!$res) return json([],0,'修改失败');
        return json('', 1, '修改成功');
    }
    //用户反馈新的问题
    public function createFeedback(){
        $info = Request::instance()->param();
        $data=[
            'admin_id'=>$info['account'],
            'content'=>$info['content'],
            'status'=>1,
            'create_time'=>time(),
        ];
        $res = Db::name('feedback')->insert($data);
        if(!$res) return json([],0,'反馈失败');
        return json('', 1, '反馈成功');
    }
    //用户反馈的问题的列表
    public function feedbackList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('feedback')->where(['admin_id'=>$info['account']])->select();
        $arr=[];
        if($list){
            foreach ($list as $k => $v) {
                $arr[$k]['question']=$v;
                $arr[$k]['question']['create_time']=date('Y-m-d H:i:s',$v['create_time']);
                $arr[$k]['list']=Db::name('feedback_talk')->where(['fid'=>$v['id']])->order('create_time asc')->select();
                if($arr[$k]['list']){
                    foreach ($arr[$k]['list'] as $kk => $vv) {
                        $arr[$k]['list'][$kk]['create_time']=date('Y-m-d H:i:s',$vv['create_time']);
                    }
                }
            }
        }
        return json($arr, 1, '查询成功');
    }
    //用户对于反馈的问题进行回复
    public function createFeedbackTalk(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $data=[
            'fid'=>$info['fid'],
            'account'=>$info['account'],
            'content'=>$info['content'],
            'status'=>1,
            'create_time'=>time(),
        ];
        $res = Db::name('feedback_talk')->insert($data);
        if(!$res) return json([],0,'回复失败');
        return json('', 1, '回复成功');
    }
    //用户对于反馈的问题的提问的历史记录
    public function feedbackTalkList(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //查询反馈问题
        $feedback=Db::name('feedback')->where(['id'=>$info['id']])->find();
        //查询问题的所有回复
        $list=Db::name('feedback_talk')->where(['fid'=>$info['id']])->order('create_time asc')->select();
        $arr[0]['id']=0;
        $arr[0]['fid']=$feedback['id'];
        $arr[0]['account']=$feedback['admin_id'];
        $arr[0]['content']=$feedback['content'];
        $arr[0]['status']=1;
        $arr[0]['create_time']=date('m-d H:i:s',$feedback['create_time']);
        foreach ($list as $k => $v) {
            $arr[$k+1]=$v;
            $arr[$k+1]['create_time']=date('m-d H:i:s',$v['create_time']);
        }
        return json($arr, 1, '查询成功');
    }
    //钱包流水
    public function balancerecord(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('trade')
            ->where(['account' => $info['account'],'trade_type'=>['in',['支付宝转账','微信转账','银行卡转账','后台充值','提现','转入钱包','转入策略余额','除权派息','提现手续费']],'is_del'=>1,'trade_status'=>['in',[1,5]]])
            ->order('create_time desc')
            ->select();
        if($list){
            foreach($list as $k=>$v){
              	if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'){
                    $list[$k]['trade_type']='充值';
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
                if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'||$v['trade_type']=='转入钱包'||$v['trade_type']=='除权派息'){
                    $list[$k]['trade_price']='+'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }elseif($v['trade_type']=='转入策略余额'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
              	if($v['trade_type']=='提现'){
                	$list[$k]['trade_price']='-'.($v['trade_price']+$v['service_charge']);
                  	$list[$k]['create_time']=date('m-d',$v['create_time']);
                }
                if($v['trade_type']=='提现手续费'){
                    $trade_status=Db::name('trade')->where(['detailed'=>$v['charge_num']])->value('trade_status');
                    if($trade_status==4){
                        unset($list[$k]);
                    }else{
                        $list[$k]['trade_price']='-'.$v['trade_price'];
                        $list[$k]['create_time']=date('m-d',$v['create_time']);
                    }
                }
                if($v['trade_type']!='提现' && $v['trade_status']!=1){
                    unset($list[$k]);
                }
                if($v['trade_type']=='提现' && ($v['trade_status']==2 || $v['trade_status']==4)){
                    unset($list[$k]);
                }
            }
            foreach($list as $k=>$v){
                $arr[$v['create_time']][]=$v;
            }
            foreach($arr as $k=>$v){
                $water[$k]['time']=$k;
                $water[$k]['list']=$v;
            }
            $result=array_values($water);
        }else{
            $result=[];
        }
        return json($result, 1, '查询成功');
    }

    //钱包流水PC
    public function balancerecordPC(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $geshu = 5;//每页个数
        if(array_key_exists('page',$info)&&$info['page']){
            $rule = ['page' =>  "^[0-9]*[1-9][0-9]*$"];
            $msg = ['page' =>  '页数必须为数字'];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($info)){
                return json('', 0, $validate->getError());
            }
        }else{
            $info['page']=1;
        }
        $page=$info['page'];

        $list=Db::name('trade')
            ->where(['account' => $info['account'],'trade_type'=>['in',['充值','后台充值','支付宝转账','微信转账','银行卡转账','提现','转入钱包','转入策略余额','除权派息','提现手续费']],'is_del'=>1,'trade_status'=>['in',[1,5]]])
            ->order('create_time desc')
            ->select();
        if($list){
            foreach($list as $k=>$v){
                if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'){
                    $list[$k]['trade_type']='充值';
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
                if($v['trade_type']=='充值'||$v['trade_type']=='后台充值'||$v['trade_type']=='支付宝转账'||$v['trade_type']=='微信转账'||$v['trade_type']=='银行卡转账'||$v['trade_type']=='转入钱包'||$v['trade_type']=='除权派息'){
                    $list[$k]['trade_price']='+'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }elseif($v['trade_type']=='提现'||$v['trade_type']=='转入策略余额'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                    $list[$k]['create_time']=date('m-d',$v['create_time']);
                }
                if($v['trade_type']=='提现手续费'){
                    $trade_status=Db::name('trade')->where(['detailed'=>$v['charge_num']])->value('trade_status');
                    if($trade_status==4){
                        unset($list[$k]);
                    }else{
                        $list[$k]['trade_price']='-'.$v['trade_price'];
                        $list[$k]['create_time']=date('m-d',$v['create_time']);
                    }
                }
                if($v['trade_type']!='提现' && $v['trade_status']!=1){
                    unset($list[$k]);
                }
                if($v['trade_type']=='提现' && ($v['trade_status']==2 || $v['trade_status']==4)){
                    unset($list[$k]);
                }
            }

            $count = count($list);
            $count ? $allyeshu=ceil($count/$geshu) : $allyeshu=floatval(1);
            if($page>$allyeshu){return json('', 0, '页数不存在');}
            if($count){
                foreach($list as $k=>$v){
                    if($k>=($page-1)*$geshu && $k<$page*$geshu){
                        $arr[]=$v;
                    }
                }
            }else{
                $arr=[];
            }
        }else{
            $count=0;
            $arr=[];
        }

        $result=[
            'allnum'=>$count,
            'geshu'=>$geshu,
            'list'=>$arr,
        ];
        return json($result, 1, '查询成功');
    }

    //策略交易流水
    public function strategyrecord(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');

        $info = Request::instance()->param();
        $list=Db::name('trade')
            ->alias('tr')
            ->field('tr.trade_price,tr.trade_type,tr.create_time,str.stock_name,str.stock_code,str.stock_number,str.credit,str.credit_add,
            str.market_value,str.true_getmoney,str.sell_price,str.buy_poundage,str.sell_poundage')
            ->join('link_strategy str','str.strategy_num=tr.charge_num')
            ->where(['tr.account' => $info['account'],'tr.trade_type'=>['in',['策略创建','策略卖出','追加信用金','扣除递延费']]])
            ->where(['str.is_cancel_order'=>1])
            ->order('tr.create_time desc')
            ->select();
        if($list){
            $shuzu=[];
            foreach($list as $k=>$v){
                $list[$k]['create_time']=date('m-d',$v['create_time']);
                if($v['trade_type']=='策略创建'){
                    $list[$k]['trade_price']='-'.$v['credit'];
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['buy_poundage'],
                        'trade_type'=>'买手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                }elseif($v['trade_type']=='策略卖出'){
                    $market_chazhi=$v['sell_price']*$v['stock_number']-$v['market_value'];
                    $trade_price=$market_chazhi+$v['credit']+$v['credit_add'];
                    if($trade_price>=0){
                        $list[$k]['trade_price']='+'.$trade_price;
                    }else{
                        $list[$k]['trade_price']=''.$trade_price;
                    }
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['sell_poundage'],
                        'trade_type'=>'卖手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                    if($chengshu!=1){
                        if($market_chazhi>0){
                            $fencheng_money=$market_chazhi-($v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                            $shuzu[]=[
                                'trade_price'=>'-'.$fencheng_money,
                                'trade_type'=>'分成',
                                'create_time'=>date('m-d',$v['create_time']),
                                'stock_name'=>$v['stock_name'],
                                'stock_code'=>$v['stock_code'],
                                'credit'=>$v['credit'],
                            ];
                        }
                    }
                }elseif($v['trade_type']=='追加信用金'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }elseif($v['trade_type']=='扣除递延费'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }
                unset($list[$k]['stock_number']);
                unset($list[$k]['credit']);
                unset($list[$k]['credit_add']);
                unset($list[$k]['true_getmoney']);
                unset($list[$k]['sell_price']);
                unset($list[$k]['buy_poundage']);
                unset($list[$k]['sell_poundage']);
            }
            if($shuzu){
                $list=array_merge($list,$shuzu);
            }
            foreach($list as $k=>$v){
                $arr[$v['create_time']][]=$v;
            }
            foreach($arr as $k=>$v){
                $water[$k]['create_time']=$k;
                $water[$k]['list']=$v;
            }
            $result=array_values($water);
        }else{
            $result=[];
        }
        return json($result, 1, '查询成功');
    }
    //策略交易流水PC
    public function strategyrecordPC(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');

        $geshu = 5;//每页个数
        if(array_key_exists('page',$info)&&$info['page']){
            $rule = ['page' =>  "^[0-9]*[1-9][0-9]*$"];
            $msg = ['page' =>  '页数必须为数字'];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($info)){
                return json('', 0, $validate->getError());
            }
        }else{
            $info['page']=1;
        }
        $page=$info['page'];

        $list=Db::name('trade')
            ->alias('tr')
            ->field('tr.trade_price,tr.trade_type,tr.create_time,tr.charge_num,str.stock_name,str.stock_code,str.stock_number,str.credit,str.credit_add,
            str.market_value,str.true_getmoney,str.sell_price,str.buy_poundage,str.sell_poundage')
            ->join('link_strategy str','str.strategy_num=tr.charge_num')
            ->where(['tr.account' => $info['account'],'tr.trade_type'=>['in',['策略创建','策略卖出','追加信用金','扣除递延费']]])
            ->where(['str.is_cancel_order'=>1])
            ->order('tr.create_time desc')
            ->select();
        if($list){
            $shuzu=[];
            foreach($list as $k=>$v){
                $list[$k]['create_time']=date('m-d',$v['create_time']);
                if($v['trade_type']=='策略创建'){
                    $list[$k]['trade_price']='-'.$v['credit'];
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['buy_poundage'],
                        'trade_type'=>'买手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                }elseif($v['trade_type']=='策略卖出'){
                    $market_chazhi=$v['sell_price']*$v['stock_number']-$v['market_value'];
                    $trade_price=$market_chazhi+$v['credit']+$v['credit_add'];
                    if($trade_price>=0){
                        $list[$k]['trade_price']='+'.$trade_price;
                    }else{
                        $list[$k]['trade_price']=''.$trade_price;
                    }
                    $shuzu[]=[
                        'trade_price'=>'-'.$v['sell_poundage'],
                        'trade_type'=>'卖手续费',
                        'create_time'=>date('m-d',$v['create_time']),
                        'stock_name'=>$v['stock_name'],
                        'stock_code'=>$v['stock_code'],
                        'credit'=>$v['credit'],
                    ];
                    if($chengshu!=1){
                        if($market_chazhi>0){
                            $fencheng_money=$market_chazhi-($v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                            $shuzu[]=[
                                'trade_price'=>'-'.$fencheng_money,
                                'trade_type'=>'分成',
                                'create_time'=>date('m-d',$v['create_time']),
                                'stock_name'=>$v['stock_name'],
                                'stock_code'=>$v['stock_code'],
                                'credit'=>$v['credit'],
                            ];
                        }
                    }
                }elseif($v['trade_type']=='追加信用金'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }elseif($v['trade_type']=='扣除递延费'){
                    $list[$k]['trade_price']='-'.$v['trade_price'];
                }
                unset($list[$k]['stock_number']);
                unset($list[$k]['credit']);
                unset($list[$k]['credit_add']);
                unset($list[$k]['true_getmoney']);
                unset($list[$k]['sell_price']);
                unset($list[$k]['buy_poundage']);
                unset($list[$k]['sell_poundage']);
            }
            if($shuzu){
                $list=array_merge($list,$shuzu);
            }
            $count = count($list);
            $count ? $allyeshu=ceil($count/$geshu) : $allyeshu=floatval(1);
            if($page>$allyeshu){return json('', 0, '页数不存在');}
            if($count){
                foreach($list as $k=>$v){
                    if($k>=($page-1)*$geshu && $k<$page*$geshu){
                        $arr[]=$v;
                    }
                }
            }else{
                $arr=[];
            }
        }else{
            $count=0;
            $arr=[];
        }

        $result=[
            'allnum'=>$count,
            'geshu'=>$geshu,
            'list'=>$arr,
        ];
        return json($result, 1, '查询成功');
    }
    //转入策略余额--从钱包转到策略余额
    public function transferin(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $money = $info['money'];
        $account = $info['account'];
        //账户未转时的余额
        $admin = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->find();
        if($info['money']>$admin['balance']){
            return json('', 0, '余额不足');
        }
        if($admin['balance']<=0){
            return json('', 0, '余额小于0');
        }
        $trade=Db::name('trade')->field('create_time')->where(['account'=>$info['account'],'trade_type'=>'转入策略余额'])->order('create_time desc')->limit(1)->find();
        if(time()-$trade['create_time']<10){
            return json('', 0, '提交过于频繁');
        }

        //减少账户余额，增加策略余额
        Db::startTrans();
        $res = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->setDec('balance',$money);
        $res2 = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->setInc('tactics_balance',$money);

        //添加交易记录
        $data=[
            'charge_num'=>date('YmdHis').rand(1000,9999),
            'account'=>$account,
            'trade_status'=>1,
            'create_time'=>time(),
            'trade_type'=>'转入策略余额',
            'trade_price'=>$money,
            'xbalance'=>$admin['balance']-$money,
        ];
        $res3 = Db::name('trade')->insert($data);

        if(!$res || !$res2 || !$res3){
            Db::rollback();
            return json('', 0, '转入策略余额失败');
        }
        Db::commit();

        logg(1,$info['account'].'的余额由'.$admin['balance'].'变成了'.($admin['balance']-$money).'，变化了'.$money.'，策略余额由'.
            $admin['tactics_balance'].'变成了'.($admin['tactics_balance']+$money).'，变化了'.$money);
        return json('', 1, '转入策略余额成功');
    }
    //转入钱包--从策略余额转到钱包
    public function transferout(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $money = $info['money'];
        $account = $info['account'];
        //账户未转时的余额
        $balance = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->value('balance');
        $tactics_balance = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->value('tactics_balance');
        if($info['money']>$tactics_balance){
            return json('', 0, '策略余额不足');
        }
        if($tactics_balance<=0){
            return json('', 0, '策略余额小于0');
        }
        $trade=Db::name('trade')->field('create_time')->where(['account'=>$info['account'],'trade_type'=>'转入钱包'])->order('create_time desc')->limit(1)->find();
        if(time()-$trade['create_time']<10){
            return json('', 0, '提交过于频繁');
        }

        //减少策略余额，增加账户余额
        Db::startTrans();
        $res = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->setInc('balance',$money);
        $res2 = Db::name('admin')->where(['account'=>$account,'is_del'=>1])->setDec('tactics_balance',$money);
        //添加交易记录
        $data=[
            'charge_num'=>date('YmdHis').rand(1000,9999),
            'account'=>$account,
            'trade_status'=>1,
            'create_time'=>time(),
            'trade_type'=>'转入钱包',
            'trade_price'=>$money,
            'xbalance'=>$balance+$money,
        ];
        $res3 = Db::name('trade')->insert($data);
        if(!$res || !$res2 || !$res3){
            Db::rollback();
            return json('', 0, '转入钱包失败');
        }
        Db::commit();

        logg(2,$info['account'].'的余额由'.$balance.'变成了'.($balance+$money).'，变化了'.$money.'，策略余额由'.$tactics_balance.
            '变成了'.($tactics_balance-$money).'，变化了'.$money);
        return json('', 1, '转入钱包成功');
    }

    //策略协议列表
    public function xieyilist() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('agreement')
            ->alias('ag')
            ->field('ag.*')
            ->join('link_admin ad','ad.true_name=ag.account_truename')
            ->where(['ad.account' => $info['account']])
            ->order('ag.sign_date desc')
            ->select();
        foreach($list as $k=>$v){
            $list[$k]['stock_name']=explode('[',$v['stock'])[0];
        }
        return json($list, 1, '查询成功');
    }
    //查看策略协议
    public function xieyicontent() {
        $info = Request::instance()->param();
        $xieyi=Db::name('agreement')
            ->alias('ag')
            ->field('ag.*,str.surplus_value,str.loss_value,str.buy_price')
            ->join('link_strategy str','str.strategy_num = ag.strategy_num')
            ->where(['ag.id' => $info['id']])
            ->find();
        $xieyi['zhiyinglv']=round(($xieyi['surplus_value']-$xieyi['buy_price'])/$xieyi['buy_price']*100,1);
        $xieyi['zhisunlv']=round(($xieyi['buy_price']-$xieyi['loss_value'])/$xieyi['buy_price']*100,1);
        $this->assign('xieyi',$xieyi);
      
      	$title=Db::name('hide')->where(['id' => 40])->value('value');
        $this->assign('title',$title);
        return $this->fetch();
    }

    //用户红包列表
    public function redPacketList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $packet=Db::name('packet')->where(['account' => $info['account'],'is_use'=>2,'daoqi_time'=>['gt',time()]])->select();
        foreach ($packet as $k => $v) {
            $packet[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $packet[$k]['daoqi_time']=date('Y-m-d',$v['daoqi_time']);
        }
        return json($packet, 1, '查询成功');
    }
    //判断用户的策略余额是否充足
    public function isbalance() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //用户策略余额
        $tactics_balance = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->value('tactics_balance');
        //此策略订单总共所需钱数
        if($tactics_balance<=($info['credit']+$info['buy_poundage'])){
            return json('', 0, '余额不足');
        }else{
            return json('', 1, '余额充足');
        }
    }

    //查看持仓中的策略
    public function checkStrategy() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $shipanstock=new Stock();
        
        $info = Request::instance()->param();
        $list=Db::name('strategy')
            ->where(['is_cancel_order'=>['in',[1,2]]])
            ->where(['account'=>$info['account'],'status'=>['in',[1,2,3]]])
            ->order('buy_time desc')
            ->select();

        //app按钮是否能点
        if(isHoliday()!=0){
            foreach($list as $k=>$v){
                $list[$k]['cansell']=2;
            }
        }else{
            foreach($list as $k=>$v){
                if(date('Ymd')>date('Ymd',$v['buy_time'])){
                    $list[$k]['cansell']=1;
                }else{
                    $list[$k]['cansell']=2;
                }
            }
        }

        // //查询现价
    //     if($list){
    //         if(date('Hi')>931){
    //             $str='';
    //             foreach($list as $k=>$v){
    //                 $str.=$v['stock_code'].',';
    //             }
    //             $result=$shipanstock->Hq_list(substr($str,0,-1));
    //             $para=explode(';',$result);
    //             unset($para[count($para)-1]);
    //             foreach($para as $k=>$v){
    //                 $list[$k]['nowprice']=explode(',',$v)[3];
    //             }
    //         }else{
    //             $times=1;
    //             for($i=1;$i<12;$i++){
    //                 if(isHoliday($i)!=0){
    //                     $times+=1;
    //                 }else{
    //                     break;
    //                 }
    //                 continue;
    //             }
    //             foreach($list as $k=>$v){
    //                 $str=$shipanstock->Hq_list($v['stock_code']);
    //                 $arr=explode(',',substr($str,1,-1));
    //                 foreach($arr as $kk=>$vv){
    //                     if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
    //                         $list[$k]['nowprice']=substr($arr[$kk+4],7,5);
    //                     }
    //                 }
    //             }
    //         }
    //     }
      
      
      	//查询现价
        if($list){
            $str='';
            foreach($list as $k=>$v){
                $str.=$v['stock_code'].',';
            }
            $str=substr($str,0,-1);
            $result=$shipanstock->Hq_real_list($str);
            $para=json_decode($result,true);
            foreach($para as $k=>$v){
                $list[$k]['nowprice']=$v['nowPri'];
            }
        }

        //显示的平仓时间
        foreach ($list as $k => $v) {
            if($v['strategy_type']==2){
                $time=0;
                for($i=1;$i<12;$i++){
                    if(isHoliday($i)==0){$time+=1;}
                    if($time==4){
                        $list[$k]['day']=date('m-d 14:50', time()+$i*24*3600);
                        break;
                    }
                    continue;
                }
            }else{
                $list[$k]['day']='下交易日14:50发起平仓请求';
            }
            $list[$k]['credit']=strval($v['credit']+$v['credit_add']);
          	$list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
        }

        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>sprintf("%.2f",$admin['frozen_money']+$admin['tactics_balance']),
            //冻结金额
            'frozen_money'=>$admin['frozen_money'],
            //可用余额
            'can_money'=>sprintf("%.2f",$admin['tactics_balance']),
        ];
        return json($list, 1, '查询成功',$arr);
    }
    //查看已平仓的策略
    public function checkStrategyHistory() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $chengshu=Db::name('hide')->where(['id'=>3])->value('value');

        //已平仓的策略
        $list=Db::name('strategy')
            ->where(['is_cancel_order'=>1])
            ->where(['account'=>$info['account'],'status'=>4])
            ->order('sell_time desc')
            ->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
              	$list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
                $list[$k]['sell_time']=date('m-d H:i',$v['sell_time']);
                if($v['sell_price']>$v['buy_price']){
                    //交易盈亏
                    $list[$k]['getprice']=sprintf("%.2f",($v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add'])/$chengshu);
                    //盈利分配
                    $list[$k]['truegetprice']=sprintf("%.2f",$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=sprintf("%.2f",$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=sprintf("%.2f",$v['true_getmoney']+$v['sell_poundage']-$v['credit']-$v['credit_add']);
                }
                $win += ($v['true_getmoney']+$v['sell_poundage'] - $v['credit']-$v['credit_add']) ;
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>sprintf("%.2f",$win),
                'rate'=>sprintf("%.2f",($winStrategyNum/$allStrategyNum*100)).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return json($list, 1, '查询成功',$arr);
    }

    //查看已平仓的策略的详情
    public function checkStrategyHistorydetails() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();
        if($strategy['sell_price']>$strategy['buy_price']){
            $chengshu=Db::name('hide')->where(['id'=>3])->value('value');
            //交易盈亏
            $strategy['getprice']=sprintf("%.2f",($strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add'])/$chengshu);
            //盈利分配
            $strategy['truegetprice']=sprintf("%.2f",$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add']);
            //亏损赔付
            $strategy['dropprice']=0;
        }else{
            //交易盈亏
            $strategy['getprice']=sprintf("%.2f",$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add']);
            //盈利分配
            $strategy['truegetprice']=0;
            //亏损赔付
            $strategy['dropprice']=sprintf("%.2f",$strategy['true_getmoney']+$strategy['sell_poundage']-$strategy['credit']-$strategy['credit_add']);
        }
        $strategy['day']=(strtotime(date('Ymd',$strategy['sell_time']))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
        $strategy['buy_time']=date('m-d H:i',$strategy['buy_time']);
        $strategy['sell_time']=date('m-d H:i',$strategy['sell_time']);
        return json($strategy, 1, '查询成功');
    }
    //申请撤单
    public function cancel_order() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

        $shipanstock=new Stock();

        if($strategy['is_real_disk']==1){
            return json('', 1, '此单已经持仓或者平仓了');
            //实盘申请撤单
            if($strategy['status']==1){
                //实盘账户信息
                $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
                //实盘撤销买入
                $success=$shipanstock->cancelorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                    $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],'',$shipaninfo['ExchangeID'],$strategy['buy_contract_num']);
                $success=json_decode($success,true);
                if($success['保留信息']==200){
                    $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update(['is_cancel_order'=>2,'sell_time'=>time()]);
                    if($res){
                        return json('', 1, '申请撤单成功');
                    }else{
                        return json('', 0, '撤单失败');
                    }
                }else{
                    return json('', 0, '申请撤单失败');
                }
            }elseif($strategy['status']==3){
                //实盘账户信息
                $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
                //实盘撤销卖出
                $success=$shipanstock->cancelorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                    $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],'',$shipaninfo['ExchangeID'],$strategy['sell_contract_num']);
                $success=json_decode($success,true);
                if($success['保留信息']==200){
                    $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update(['is_cancel_order'=>2,'sell_time'=>time()]);
                    if($res){
                        return json('', 1, '申请撤单成功');
                    }else{
                        return json('', 0, '撤单失败');
                    }
                }else{
                    return json('', 0, '申请撤单失败');
                }
            }else{
                return json('', 0, '此单已经持仓或者平仓了');
            }
        }else{
            //没走实盘的进行申请撤单
            if($strategy['status']==1||$strategy['status']==3){
                $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update(['is_cancel_order'=>2,'sell_time'=>time()]);
                if($res){
                    return json('', 1, '申请撤单成功');
                }else{
                    return json('', 0, '撤单失败');
                }
            }else{
                return json('', 0, '此单已经持仓或者平仓了');
            }
        }
    }
    //申请递延
    public function changeDefer() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

        $day=0;//工作日天数
        $times=1;//休息天数
        //t+1的时间为1天，t+5的时间为4天
        if($strategy['strategy_type']==1){
            $free=1;
        }else{
            $free=4;
        }
        //当前时间到买入的时间的天数
        $all=(strtotime(date('Ymd'))-strtotime(date('Ymd',$strategy['buy_time'])))/24/3600;
        for($i=1;$i<=$all;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                $day+=1;
            }
            if($day>=$free){
                break;
            }else{
                continue;
            }
        }
        if(isHoliday()!=0){
            $data['rest_day']=$times-1+1;
        }else{
            $data['rest_day']=$times-1;
        }

        $data['defer']=1;
        Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);

        logg(3,'买入时间：'.date('Y-m-d H:i:s',$strategy['buy_time']).
            '，申请时间:'.date('Y-m-d H:i:s')."，休息时间为".$data['rest_day']."天");
        return json('', 1, '申请递延成功');
    }

  	//获取实盘查询的必备参数
    public function getshipaninfo($id,$sh_sz=''){
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipan=[];
        foreach($allshipan_account as $k=>$v){
            if($v['id']==$id){
                $shipan=$v;
            }
        }
        if($sh_sz){
            if($sh_sz=='sh'){
                $shipan['gudongdaima']=$shipan['shholder'];
                $shipan['ExchangeID']=1;
                $shipan['PriceType']=6;
            }else{
                $shipan['gudongdaima']=$shipan['szholder'];
                $shipan['ExchangeID']=0;
                $shipan['PriceType']=1;
            }
        }
        return $shipan;
    }
  
  
    //创建策略
    public function createStrategy() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();

        //股票当前价格(加了5分)
        $nowprice=getOneStock2($info['stock_code'])['nowPri']+0.00;

        //用户的所有策略订单的市值最大额度
        $adminMaxMarket=Db::name('hide')->where(['id' => 7])->value('value');
        $list=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->select();
        $alreadyAdminMaxMarket=0;
        foreach($list as $k=>$v){
            $alreadyAdminMaxMarket+=$v['market_value'];
        }
        //用户的所有策略订单的市值
        if($alreadyAdminMaxMarket+$nowprice*$info['stock_number']>=$adminMaxMarket){
            return json('', 0, '所有策略市值超过最大额度'.($adminMaxMarket/10000).'W');
        }

        //用户信息
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['tactics_balance']<($info['credit']+$info['buy_poundage'])){return json('', 0, '策略余额不足');}

      	if($admin['is_true']!=1){return json('', 0, '用户未实名认证');}
      
        //股数不能为空
        if(!$info['stock_number']){return json('', 0, '股数不能为空');}

        //股票已停牌
        if(thischeckstockstop($info['stock_code'])!=200){return json('', 0, '该股票已被风控，无法购买');}

		if($info['surplus_value']<$nowprice){return json('', 0, '止盈价格不能小于现价');}

      	$beishu=Db::name('hide')->where(['id'=>2])->value('value');
        $beishu=explode(',',$beishu);
        if (!in_array($info['double_value']/$info['credit'],$beishu)){return json('', 0, '倍数不符合规则');}
      	
      	if($info['strategy_type']==1){
        	$credit_min = Db::name('hide')->where('id',16)->value('value');//最低信用金
        	$credit_max = Db::name('hide')->where('id',17)->value('value');//最高信用金
          	if($info['credit']<$credit_min){return json('', 0, '最低信用金为'.$credit_min);}
        	if($info['credit']>$credit_max){return json('', 0, '最高信用金为'.$credit_max);}
        }

        //用户是否进入实盘
        if($admin['is_real_disk']==1){
            $shipanstock=new Stock();

            logg(4,'委托时间:'.date('Y-m-d H:i:s') ."    实盘中的策略，".'用户：'. $info['account'].
                "，股票代码：".$info['stock_code'].'，股票数量：'.$info['stock_number']."想要购买股票");

            //策略使用的实盘账户id
            $data['shipan_account']=$admin['shipan_account'];
            //实盘账户信息
            $shipaninfo=$this->getshipaninfo($admin['shipan_account'],substr($info['stock_code'],0,2));
            //实盘进行委托
            $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],'',0,$shipaninfo['PriceType'],$shipaninfo['gudongdaima'],
                substr($info['stock_code'],2),0,$info['stock_number']);
            $success=json_decode($success,true);
            //有委托编号这个参数名和值，说明委托成功了
            if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                //买入合同编号
                $data['buy_contract_num']=$success['data1']['委托编号'];

                logg(4,'委托时间:'.date('Y-m-d H:i:s') ."    实盘中的策略，".'用户：'. $info['account'].
                    "，股票代码：".$info['stock_code'].'，股票数量：'.$info['stock_number']."通过委托成功才创建的");
            }else{
                logg(4,'委托时间:'.date('Y-m-d H:i:s') ."    委托失败了".
                    '原因：'. $success['data1']['保留信息']);
                return json('', 0, '委托失败');
            }
        }

        //创建策略
        $time=date('YmdHis', time());
        $data['strategy_num']=$time.rand(1000,9999);
        $data['account']=$info['account'];
        $data['strategy_type']=$info['strategy_type'];
        $data['status']=1;
        $data['stock_name']=$info['stock_name'];
        $data['stock_code']=$info['stock_code'];
        $data['stock_number']=$info['stock_number'];
        $data['credit']=$info['credit'];
        $data['market_value']=$nowprice*$info['stock_number'];
        $data['double_value']=$info['double_value'];
        $data['buy_price']=$nowprice;
        $data['surplus_value']=$info['surplus_value'];
        $data['loss_value']=$info['loss_value'];
        //是否进入实盘
        $data['is_real_disk']=$admin['is_real_disk'];
        //计算手续费时,T+1时第一天的递延费免费,T+5时会先收4天的递延费
        $data['buy_poundage']=$info['buy_poundage'];
        $data['all_poundage']='';
        $data['defer']=$info['defer'];
        $data['buy_time']=time();

        //如果使用红包
        if(array_key_exists('packet_id',$info)&&$info['packet_id']){
            //使用红包的记录
            $data['is_packet']=1;
            //红包信息
            $packet=Db::name('packet')->where(['packet_id' => $info['packet_id']])->find();
            $data['all_poundage']=$info['buy_poundage']-$packet['packet_money'];
        }else{
            $data['all_poundage']=$info['buy_poundage'];
        }

        Db::startTrans();
        //修改红包使用记录
        if(array_key_exists('packet_id',$info)&&$info['packet_id']){
            $result1=Db::name('packet')->where(['packet_id' => $info['packet_id']])->update(['strategy_num'=>$data['strategy_num'],'is_use'=>1,'use_time'=>time()]);
            if(!$result1){
                Db::rollback();
                return json('', 0, '红包使用失败');
            }
        }

        //添加策略记录
        $result2=Db::name('strategy')->insert($data);
        //修改用户策略余额和冻结金额
        $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['tactics_balance'=>$admin['tactics_balance']-$data['credit']-$data['all_poundage']]);
        $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->update(['frozen_money'=>$admin['frozen_money']+$data['credit']]);
        //添加交易记录
        $trade=[
            'charge_num'=>$data['strategy_num'],
            'trade_price'=>$data['credit']+$data['all_poundage'],
            'account'=>$info['account'],
            'trade_type'=>'策略创建',
            'trade_status'=>1,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result5=Db::name('trade')->insert($trade);

        //添加订单协议
        $arr=[
            'strategy_num'=>$data['strategy_num'],
            'investor'=>Db::name('hide')->where(['id' => 9])->value('value'),
            'account_truename'=>$admin['true_name'],
            'stock'=>' '.$info['stock_name'].'[股票代码：'.substr($info['stock_code'],2).']',
            'stock_number'=>$info['stock_number'],
            'zhiyinglv'=>'20.0',
            'credit'=>$info['credit'],
            'sign_date'=>date('Y-m-d H:i:s'),
        ];

        if(date('N')==5){
            $t=date('Y-m-d');
            $t1=date('Y-m-d',time()+3*24*3600);
            $t5=date('Y-m-d',time()+7*24*3600);
            if($info['strategy_type']==1){
                $arr['strategy_type']='T+1';
                $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
            }else{
                $arr['strategy_type']='T+5';
                $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
            }
            $arr['t_day']=$t;
            $arr['t_one_day']=$t1;
            $arr['t_five_day']=$t5;
        }else{
            $t=date('Y-m-d');
            $t1=date('Y-m-d',time()+24*3600);
            $t5=date('Y-m-d',time()+4*24*3600);
            if($info['strategy_type']==1){
                $arr['strategy_type']='T+1';
                $arr['pingcang_time']=$t1.' 09:30:00-'.$t1.' 14:49:59';
            }else{
                $arr['strategy_type']='T+5';
                $arr['pingcang_time']=$t5.' 09:30:00-'.$t5.' 14:49:59';
            }
            $arr['t_day']=$t;
            $arr['t_one_day']=$t1;
            $arr['t_five_day']=$t5;
        }
        $result6=Db::name('agreement')->insert($arr);
        if(!$result2 || !$result3 || !$result4 || !$result5 || !$result6){
            Db::rollback();
            return json('', 0, '撮合失败');
        }
        Db::commit();
        return json(['strategy_num'=>$data['strategy_num']], 1, '撮合成功');
    }
    //修改止损
    public function updateLossValue() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $strategy = Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->find();
        if($info['trues']==1){
            Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }else{
            Db::name('strategy_contest')->where(['id' => $info['id']])->update(['loss_value'=>$info['loss_value']]);
        }

        logg(6,$strategy['account'].'的'.$strategy['stock_name'].'修改了止损，由'.$strategy['loss_value'].'变成了'.$info['loss_value']);
        return json('', 1, '修改止损成功');
    }
  	//修改止盈
    public function update_surplus_value() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        if($info['trues']==1){
            Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id' => $info['id']])->update(['surplus_value'=>$info['surplus_value']]);
        }else{
            Db::name('strategy_contest')->where(['id' => $info['id']])->update(['surplus_value'=>$info['surplus_value']]);
        }
        return json('', 1, '修改止盈成功');
    }
    //策略发起平仓，卖出股票（改变状态）
    public function sellStrategy() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return json('', 0, '非交易时间禁止平仓');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return json('', 0, '非交易时间禁止平仓');
            }
        }

        $info = Request::instance()->param();
        //策略信息
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->find();

        if($strategy['status']>2){
            return json('', 0, '此策略正在卖出');
        }

        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return json('', 0, '当天禁止卖出策略');
        }

        if(thischeckstockstop($strategy['stock_code'])!=200){
            return json('', 0, '该股票已被风控，无法卖出');
        }

        //实盘委托
        if($strategy['is_real_disk']==1){
            $shipanstock=new Stock();

            logg(5,$strategy['account']."实盘中的策咯".$strategy['strategy_num']."在实盘系统想主动卖出，开始准备委托");

            //实盘账户信息
            $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
            //实盘进行委托
            $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],'',1,$shipaninfo['PriceType'],$shipaninfo['gudongdaima'],
                substr($strategy['stock_code'],2),0,$strategy['stock_number']);
            $success=json_decode($success,true);
            //有委托编号这个参数名和值，说明委托成功了
            if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                //卖出合同编号
                $data['sell_contract_num']=$success['data1']['委托编号'];

                logg(5,$strategy['account']."实盘中的策咯".$strategy['strategy_num']."在实盘想主动卖出，委托成功");
            }else{
                logg(5,$strategy['account']."实盘中的策咯".$strategy['strategy_num']."在实盘想主动卖出，但是委托失败了");
                return json('', 0, '委托失败');
            }
        }

        $data['status']=3;
        $data['sell_type']=1;
        $data['sell_time']=time();
        //修改策略记录
        $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$info['id']])->update($data);
        if($res){
            return json('', 1, '平仓申请成功');
        }else{
            return json('', 0, '平仓申请失败');
        }
    }

  	//判断用户的炒股大赛余额是否充足
    public function isbalanceContest() {
        $info = Request::instance()->param();
        //用户炒股大赛余额
        $analog_money = Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->value('analog_money');
        //此策略订单总共所需钱数
        if($analog_money<$info['credit']){
            return json('', 0, '炒股大赛余额不足');
        }else{
            return json('', 1, '炒股大赛余额充足');
        }
    }

    //修改炒股大赛中已申报的策略为持仓中
    public function changeStrategyStatusContest() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>1])->update(['status'=>2]);
        return json('', 1, '修改成功');
    }

    //查看炒股大赛的持仓中的策略
    public function checkStrategyContest() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();

        //查询现价
        if($list){

            $shipanstock=new Stock();

            if(date('Hi')>931){
                $str='';
                foreach($list as $k=>$v){
                    $str.=$v['stock_code'].',';
                }

                $result=$shipanstock->Hq_list(substr($str,0,-1));
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $list[$k]['nowprice']=explode(',',$v)[3];
                }
            }else{
                $times=1;
                for($i=1;$i<12;$i++){
                    if(isHoliday($i)!=0){
                        $times+=1;
                    }else{
                        break;
                    }
                    continue;
                }
                foreach($list as $k=>$v){
                    $str=$shipanstock->Stop($v['stock_code']);
                    $arr=explode(',',substr($str,1,-1));
                    foreach($arr as $kk=>$vv){
                        if(strpos($vv,date('Y-m-d',time()-$times*3600*24).' 15:00:00')){
                            $list[$k]['nowprice']=substr($arr[$kk+4],7,5);
                        }
                    }
                }
            }
        }

        //买入时间
        foreach($list as $k=>$v){
          	$list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
            $list[$k]['day']='';
        }

        //用户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $arr=[
            //总计余额
            'all_money'=>$admin['analog_money']+$admin['frozen_analog_money'],
            //冻结金额
            'frozen_money'=>$admin['frozen_analog_money'],
            //可用余额
            'can_money'=>$admin['analog_money'],
        ];
        return json($list, 1, '查询成功',$arr);
    }

    //查看炒股大赛的已平仓的策略
    public function checkStrategyHistoryContest() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //已平仓的策略
        $list=Db::name('strategy_contest')->where(['account'=>$info['account'],'status'=>['in',[3,4]]])->order('sell_time desc')->select();
        if($list){
            //总策略数
            $allStrategyNum=count($list);
            //盈利策略数
            $winStrategyNum=0;
            //总盈利
            $win=0;
            foreach($list as $k=>$v) {
                $list[$k]['buy_time']=date('m-d H:i',$v['buy_time']);
                $list[$k]['sell_time']=date('m-d H:i',$v['sell_time']);
                if($v['sell_price']>=$v['buy_price']){
                    //交易盈亏
                    $list[$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //盈利分配
                    $list[$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //亏损赔付
                    $list[$k]['dropprice']=0;
                    $winStrategyNum+=1;
                }else{
                    //交易盈亏
                    $list[$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    //盈利分配
                    $list[$k]['truegetprice']=0;
                    //亏损赔付
                    $list[$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                }
                $win += ($v['sell_price'] - $v['buy_price']) * $v['stock_number'];
            }
            $arr=[
                'allStrategyNum'=>$allStrategyNum,
                'winStrategyNum'=>$winStrategyNum,
                'win'=>$win,
                'rate'=>round(($winStrategyNum/$allStrategyNum*100),2).'%',
            ];
        }else{
            $arr=[
                'allStrategyNum'=>0,
                'winStrategyNum'=>0,
                'win'=>0,
                'rate'=>'0%',
            ];
        }
        return json($list, 1, '查询成功',$arr);
    }

    //查看已平仓的炒股大赛的详情
    public function checkStrategyHistoryContestdetails() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();
        if($strategy['sell_price']>$strategy['buy_price']){
            //交易盈亏
            $strategy['getprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //盈利分配
            $strategy['truegetprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //亏损赔付
            $strategy['dropprice']=0;
        }else{
            //交易盈亏
            $strategy['getprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
            //盈利分配
            $strategy['truegetprice']=0;
            //亏损赔付
            $strategy['dropprice']=($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
        }
        $strategy['day']=date('Ymd',$strategy['sell_time'])-date('Ymd',$strategy['buy_time']);
        $strategy['buy_time']=date('m-d H:i',$strategy['buy_time']);
        $strategy['sell_time']=date('m-d H:i',$strategy['sell_time']);
        $strategy['fanhuan']=$strategy['credit']+($strategy['sell_price']-$strategy['buy_price'])*$strategy['stock_number'];
        return json($strategy, 1, '查询成功');
    }

    //创建炒股大赛策略
    public function createStrategyContest() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

      	//是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return json('', 0, '非交易时间禁止创建');
            }
        }
        $info = Request::instance()->param();

        //用户信息
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['analog_money']<$info['stock_number']*$info['buy_price']){
            return json('', 0, '炒股大赛余额不足');
        }
        //股数不能为空
        if(!$info['stock_number']){
            return json('', 0, '股数不能为空');
        }

        //股票已停牌
        if(thischeckstockstop($info['stock_code'])!=200){
            return json('', 0, '该股票已被风控，无法购买');
        }

        //股票当前价格
        $nowprice=getOneStock2($info['stock_code'])['nowPri'];

        //创建策略
        $time=date('YmdHis', time());
        $data=[
            'strategy_num'=>$time.rand(1000,9999),
            'account'=>$info['account'],
            'status'=>2,
            'stock_name'=>$info['stock_name'],
            'stock_code'=>$info['stock_code'],
            'stock_number'=>$info['stock_number'],
            'credit'=>$info['credit'],
            'buy_price'=>$nowprice,
            'surplus_value'=>$info['surplus_value'],
            'loss_value'=>$info['loss_value'],
            'buy_time'=>time(),
        ];

        Db::startTrans();
        //添加策略记录
        $result2=Db::name('strategy_contest')->insert($data);
        //修改用户策略余额和冻结金额
        $result3=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setDec('analog_money',$data['credit']);
        $result4=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->setInc('frozen_analog_money',$data['credit']);
        if(!$result2 || !$result3 || !$result4){
            Db::rollback();
            return json('', 0, '策略创建失败');
        }
        Db::commit();
        return json('', 1, '策略创建成功');
    }
    //炒股大赛的策略发起平仓，卖出股票
    public function sellStrategyContest() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return json('', 0, '非交易时间禁止创建');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return json('', 0, '非交易时间禁止创建');
            }
        }

        $info = Request::instance()->param();
        //策略信息
        $strategy=Db::name('strategy_contest')->where(['id'=>$info['id']])->find();

        if($strategy['status']!=2){
            return json('', 0, '此策略正在卖出');
        }

        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return json('', 0, '当天禁止卖出策略');
        }

        //股票当前价格
        $nowprice=getOneStock2($strategy['stock_code'])['nowPri'];

        //修改策略
        $data=[
            'status'=>4,
            'sell_price'=>$nowprice,
            'sell_time'=>time(),
            'sell_type'=>1,
        ];

        Db::startTrans();
        //修改策略记录
        $result1=Db::name('strategy_contest')->where(['id'=>$info['id']])->update($data);
        //修改用户策略余额和冻结金额
        $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setInc('analog_money',($nowprice-$strategy['buy_price'])*$strategy['stock_number']+$strategy['credit']);
        $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->setDec('frozen_analog_money',$strategy['credit']);

        if(!$result1 || !$result2 || !$result3){
            Db::rollback();
            return json('', 0, '平仓申请失败');
        }
        Db::commit();
        return json('', 1, '平仓申请成功');
    }
    //炒股大赛的策略发起平仓，卖出股票
    public function sellStrategyContest2($id,$type=1) {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        //策略信息
        $strategy=Db::name('strategy_contest')->where(['id'=>$id])->find();

        if($strategy['status']!=2){
            return json('', 0, '此策略正在卖出');
        }

        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return json('', 0, '当天禁止卖出策略');
        }

        //股票当前价格
        $nowprice=getOneStock2($strategy['stock_code'])['nowPri'];

        //修改策略
        $data=[
            'status'=>4,
            'sell_price'=>$nowprice,
            'sell_time'=>time(),
            'sell_type'=>$type,
        ];

        Db::startTrans();
        //修改策略记录
        $result1=Db::name('strategy_contest')->where(['id'=>$id])->update($data);
        //修改用户策略余额和冻结金额
        $admin=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->find();
        $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->update(['analog_money'=>$admin['analog_money']+($nowprice-$strategy['buy_price'])*$strategy['stock_number']+$strategy['credit']]);
        $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->update(['frozen_analog_money'=>$admin['frozen_analog_money']-$strategy['credit']]);
        if(!$result1 || !$result2 || !$result3){
            Db::rollback();
            return 0;
        }
        Db::commit();
        return 1;
    }

    public function dayrankingList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $list=$this->ranking($day);
        return json($list, 1, '查询成功');
    }
    public function weekrankingList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $list=$this->ranking($week);
        return json($list, 1, '查询成功');
    }
    public function monthrankingList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];
        $list=$this->ranking($month);
        return json($list, 1, '查询成功');
    }
    public function allrankingList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $list=$this->ranking();
        return json($list, 1, '查询成功');
    }
    //排行榜列表
    public function rankingList() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];

        $info = Request::instance()->param(true);
        if(array_key_exists('pchead',$info)&&$info['pchead']==1){
            $list['pchead']=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->value('headurl');
        }

        $list['day']=$this->ranking($day);
        $list['week']=$this->ranking($week);
        $list['month']=$this->ranking($month);
        $list['all']=$this->ranking();
      
      
      	foreach($list['all'] as $k=>$v){
          	$list['all'][$k]['username']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4);
		}
      
      	foreach($list['day'] as $k=>$v){
          	$list['day'][$k]['username']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4);
		}
      
      	foreach($list['month'] as $k=>$v){
          	$list['month'][$k]['username']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4);
		}
      
      	foreach($list['week'] as $k=>$v){
          	$list['week'][$k]['username']=substr($v['subscribe_account'],0,3).'****'.substr($v['subscribe_account'],7,4);
		}

        return json($list, 1, '查询成功');
    }
    //获取排行榜的人员，策略收益
    public function ranking($where=[]) {
        $where['status']=['in',[3,4]];
        //信息是否开放(排行榜是否看见)的会员
        $admin=Db::name('admin')->where(['is_open_ranking'=>1,'is_del'=>1])->select();

        //真实策略
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
        $list=[];
        $arr=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list[$k][$kk]=$vv;
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list[$k][$kk]['headurl']=$v['headurl'];
                }
            }
        }
        //真实的策略
        if($list){
            //对数组的里层进行重新排序，改变key
            foreach($list as $k=>$v){
                $list[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称
            foreach($list as $k=>$v){
                $arr[$k]['trues']='1';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr[$k]['headurl']=$v[0]['headurl'];
                $arr[$k]['username']=$v[0]['account'];
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    //计算胜率
                    if($vv['true_getmoney']>$vv['credit']){$win+=1;}
                }
                //收益率
                $arr[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr[$k]['win']=(round($win/count($v)*100,2)).'%';
            }
        }

        //炒股大赛策略
        $strategy_contest=Db::name('strategy_contest')->where($where)->select();
        $list_contest=[];
        $arr_contest=[];
        //获取到一级为会员，二级为他的策略的多维数组
        foreach($admin as $k=>$v){
            foreach($strategy_contest as $kk=>$vv){
                if($v['account']==$vv['account']){
                    $list_contest[$k][$kk]=$vv;
                    $list_contest[$k][$kk]['nick_name']=$v['nick_name'];
                    //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                    $list_contest[$k][$kk]['subscribe_account']=$vv['account'];
                    //用户头像
                    $list_contest[$k][$kk]['headurl']=$v['headurl'];
                }
            }
        }
        //炒股大赛的策略
        if($list_contest){
            //对数组的里层进行重新排序，改变key
            foreach($list_contest as $k=>$v){
                $list_contest[$k]=array_values($v);
            }
            //获取到收益率和胜率的新数组，真实策略为1，名字为手机号，炒股大赛为2，名字为昵称(没有的话为手机号)
            foreach($list_contest as $k=>$v){
                $arr_contest[$k]['trues']='2';
                //subscribe_account为获得详情的参数，为了与我的订阅中跳转参数一致
                $arr_contest[$k]['subscribe_account']=$v[0]['subscribe_account'];
                //用户头像
                $arr_contest[$k]['headurl']=$v[0]['headurl'];
                if($v[0]['nick_name']){
                    $arr_contest[$k]['username']=$v[0]['nick_name'];
                }else{
                    $arr_contest[$k]['username']=$v[0]['account'];
                }
                //收益，(卖出价-买入价)*股数
                $shouyi=0;
                //信用金
                $credit=0;
                //盈利次数
                $win=0;
                foreach($v as $kk=>$vv){
                    $credit+=$vv['credit'];
                    $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    //计算胜率
                    if($vv['sell_price']>$vv['buy_price']){$win+=1;}
                }
                //收益率
                $arr_contest[$k]['profit']=(round($shouyi/$credit*100,2));
                //胜率
                $arr_contest[$k]['win']=(round($win/count($v)*100,2)).'%';
            }
        }
        //数组合并
        $para=array_merge($arr,$arr_contest);

        //排序
        array_multisort(array_column($para,'profit'),SORT_DESC,$para);

        //收益率加上%
        foreach ($para as $k => $v) {
            if($v['profit']<0){
                unset($para[$k]);
            }else{
                $para[$k]['id']=$k+1;
                $para[$k]['profit']=$v['profit'].'%';
            }
        }

        if(count($para)>10){
            foreach ($para as $k => $v) {
                if($k>=10){unset($para[$k]);}
            }
        }

        return $para;
    }
    //订阅和取消订阅
    public function createSubscribe() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //订阅
        if($info['is_subscribe']==1){
            $data=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
                'subscribe_nick_name'=>$info['subscribe_nick_name'],
                'is_del'=>1,
                'create_time'=>time(),
            ];
            Db::name('subscribe')->insert($data);

            //进行订阅推送
            $account=Db::name('settings')
                ->where(['account' => $info['subscribe_account'],'is_del'=>1])
                ->find();
            $content='有一位会员关注了您，请注意查看哦';
            
            push($account['account'],$content,4);

            return json('', 1, '关注成功');
            //取消订阅
        }elseif($info['is_subscribe']==2){
            $where=[
                'account'=>$info['account'],
                'subscribe_account'=>$info['subscribe_account'],
                'trues'=>$info['trues'],
            ];
            Db::name('subscribe')->where($where)->update(['is_del'=>2]);
            return json('', 1, '取消关注成功');
        }
    }
    //我的订阅
    public function mysubscribe() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $list = Db::name('subscribe')->where(['account' => $info['account'],'is_del'=>1])->select();
        foreach($list as $k=>$v){
            //true为1查询真实策略，2查询炒股大赛
            if($v['trues']==1){
                $store=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])
                    ->select();
                if($store){
                    //收益，平台分成后的钱（包括信用金）
                    $shouyi=0;
                    //信用金（交易本金）
                    $credit=0;
                    foreach($store as $kk=>$vv){
                        $credit+=$vv['credit'];
                        $shouyi+=($vv['true_getmoney']-$vv['credit']);
                    }
                    //收益率
                    $list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                }else{
                    $list[$k]['profit']='0%';
                }
                $list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
            }else{
                $store=Db::name('strategy_contest')->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])->select();
                if($store){
                    //收益，(卖出价-买入价)*股数
                    $shouyi=0;
                    //信用金
                    $credit=0;
                    foreach($store as $kk=>$vv){
                        $credit+=$vv['credit'];
                        $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                    }
                    //收益率
                    $list[$k]['profit']=(round($shouyi/$credit*100,2)).'%';
                }else{
                    $list[$k]['profit']='0%';
                }
                $list[$k]['headurl']=Db::name('admin')->where(['account' => $v['subscribe_account'],'is_del'=>1])->value('headurl');
            }
        }
        return json($list, 1, '查询成功');
    }
    //查看其余人的战绩
    public function subscribeInfo() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $shipanstock=new Stock();

        $times=1;
        for($i=1;$i<12;$i++){
            if(isHoliday($i)!=0){
                $times+=1;
            }else{
                break;
            }
            continue;
        }

        $day['sell_time']=['between',[strtotime(date('Y-m-d'))-$times*24*3600,strtotime(date('Y-m-d'))]];
        $week['sell_time']=['between',[strtotime(date('Y-m-d'))-(date('N')-1)*24*3600-7*24*3600,strtotime(date('Y-m-d'))-(date('N')-1)*24*3600]];
        $month['sell_time']=['between',[strtotime(date('Y-m'))-30*24*3600,strtotime(date('Y-m'))]];

        $info = Request::instance()->param();
        //订阅的人的策略  true为1查询真实策略，2查询炒股大赛
        if($info['trues']==1){
            $list['day']=$this->shouyiInfo($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfo($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfo($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfo($info['subscribe_account']);
        }else{
            $list['day']=$this->shouyiInfoContest($info['subscribe_account'],$day);
            $list['week']=$this->shouyiInfoContest($info['subscribe_account'],$week);
            $list['month']=$this->shouyiInfoContest($info['subscribe_account'],$month);
            $list['all']=$this->shouyiInfoContest($info['subscribe_account']);
        }

        //此会员的会员信息
        $admin=Db::name('admin')->where(['account' => $info['subscribe_account'],'is_del'=>1])->find();
        //if($admin['nick_name']){
         //   $list['nick_name']=$admin['nick_name'];
        //}else{
         //   $list['nick_name']=$admin['account'];
        //}
        $list['nick_name']=substr($admin['account'],0,3).'****'.substr($admin['account'],7,4);
        $list['headurl']=$admin['headurl'];

        //总排名
        $ranking=$this->ranking();
        foreach($ranking as $k=>$v){
            if(($v['subscribe_account']==$info['subscribe_account'])&&($v['trues']==$info['trues'])){
                $list['ranking']=$k+1;
            }
        }

        //判断我是否订阅此人
        $subscribe = Db::name('subscribe')
            ->where(['account'=>$info['account'],'subscribe_account' => $info['subscribe_account'],'trues'=>$info['trues'],'is_del'=>1])
            ->find();
        if($subscribe){
            $list['is_subscribe']=1;
        }else{
            $list['is_subscribe']=2;
        }
        //被订阅数
        $list['subscribe_sum'] = Db::name('subscribe')->where(['subscribe_account' => $info['subscribe_account'],'is_del'=>1])->count();

        //战绩图
        $war[date('Ymd')-6]=[];
        $war[date('Ymd')-5]=[];
        $war[date('Ymd')-4]=[];
        $war[date('Ymd')-3]=[];
        $war[date('Ymd')-2]=[];
        $war[date('Ymd')-1]=[];
        $war[date('Ymd')]=[];

        //策略创建的历史记录
        if($info['trues']==1){
            $store=Db::name('strategy')
                ->where(['is_cancel_order'=>1])
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
                ->order('sell_time desc')
                ->select();
            if($store){
                //用户卖出股票时所能拿到的成数
                $chengshu=Db::name('hide')->where(['id' => 3])->value('value');
                foreach($store as $k=>$v){
                    //策略创建的记录
                    $list['record'][$k]=$v;
                    if($v['true_getmoney']>$v['credit']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=round(($v['true_getmoney']-$v['credit'])/$chengshu,2);
                        //盈利分配
                        $list['record'][$k]['truegetprice']=$v['true_getmoney']-$v['credit'];
                        //亏损赔付
                        $list['record'][$k]['dropprice']=0;
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=$v['true_getmoney']-$v['credit'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=0;
                        //亏损赔付
                        $list['record'][$k]['dropprice']=$v['true_getmoney']-$v['credit'];
                    }
                    //战绩图
                    if($v['sell_time']<strtotime(date('Ymd')-5)){
                        $war[date('Ymd')-6][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-4)){
                        $war[date('Ymd')-5][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-3)){
                        $war[date('Ymd')-4][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-2)){
                        $war[date('Ymd')-3][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-1)){
                        $war[date('Ymd')-2][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd'))){
                        $war[date('Ymd')-1][]=$v;
                    }
                    if($v['sell_time']<time()){
                        $war[date('Ymd')][]=$v;
                    }
                }
                //战绩图
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                foreach($war as $k=>$v){
                    $list['war'][$k]['time']=$k;
                    if($v){
                        foreach($v as $kk=>$vv){
                            $credit+=$vv['credit'];
                            $shouyi+=($vv['true_getmoney']-$vv['credit']);
                        }
                        //收益率
                        $list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
                    }else{
                        $list['war'][$k]['income']='0%';
                    }
                }
                $list['war']=array_values($list['war']);

            }else{
                //没有策略创建的历史记录
                $list['record']=[];
            }
        }else{
            $store=Db::name('strategy_contest')
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[3,4]]])
                ->order('sell_time desc')
                ->select();
            if($store){
                foreach($store as $k=>$v){
                    $list['record'][$k]=$v;
                    if($v['sell_price']>=$v['buy_price']){
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //亏损赔付
                        $list['record'][$k]['dropprice']=0;
                    }else{
                        //交易盈亏
                        $list['record'][$k]['getprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                        //盈利分配
                        $list['record'][$k]['truegetprice']=0;
                        //亏损赔付
                        $list['record'][$k]['dropprice']=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
                    }
                    //战绩图
                    if($v['sell_time']<strtotime(date('Ymd')-5)){
                        $war[date('Ymd')-6][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-4)){
                        $war[date('Ymd')-5][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-3)){
                        $war[date('Ymd')-4][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-2)){
                        $war[date('Ymd')-3][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd')-1)){
                        $war[date('Ymd')-2][]=$v;
                    }
                    if($v['sell_time']<strtotime(date('Ymd'))){
                        $war[date('Ymd')-1][]=$v;
                    }
                    if($v['sell_time']<time()){
                        $war[date('Ymd')][]=$v;
                    }
                }
                //战绩图
                //收益，平台分成后的钱（包括信用金）
                $shouyi=0;
                //信用金（交易本金）
                $credit=0;
                foreach($war as $k=>$v){
                    $list['war'][$k]['time']=$k;
                    if($v){
                        foreach($v as $kk=>$vv){
                            $credit+=$vv['credit'];
                            $shouyi+=($vv['sell_price']-$vv['buy_price'])*$vv['stock_number'];
                        }
                        //收益率
                        $list['war'][$k]['income']=(round($shouyi/$credit*100,2)).'%';
                    }else{
                        $list['war'][$k]['income']='0%';
                    }
                }
                $list['war']=array_values($list['war']);
            }else{
                //没有炒股大赛创建的历史记录
                $list['record']=[];
            }
        }
        if($list['record']){
            foreach ($list['record'] as $k => $v) {
                $list['record'][$k]['buy_time']=date('Y-m-d',$v['buy_time']);
                $list['record'][$k]['sell_time']=date('Y-m-d',$v['sell_time']);
            }
        }

        //策略创建的持股记录
        if($info['trues']==1){
            $buy_store=Db::name('strategy')
                ->where(['is_cancel_order'=>1])
                ->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])
                ->order('buy_time desc')
                ->select();
            if($buy_store){
                //查询现价
                $str='';
                foreach($buy_store as $k=>$v){
                    $str.=$v['stock_code'].',';
                }

                $result=$shipanstock->Hq_list(substr($str,0,-1));
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $buy_store[$k]['nowprice']=explode(',',$v)[3];
                }

                foreach($buy_store as $k=>$v){
                    $list['buy_record'][$k]=$v;
                    $list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
                    //策略盈亏(收益)
                    $list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
                }

            }else{
                //没有策略创建的持股记录
                $list['buy_record']=[];
            }
        }else{
            $buy_store=Db::name('strategy_contest')->where(['account' => $info['subscribe_account'],'status'=>['in',[1,2]]])->order('buy_time desc')->select();
            if($buy_store){
                //查询现价
                $str='';
                foreach($buy_store as $k=>$v){
                    $str.=$v['stock_code'].',';
                }

                $result=$shipanstock->Hq_list(substr($str,0,-1));
                $para=explode(';',$result);
                unset($para[count($para)-1]);
                foreach($para as $k=>$v){
                    $buy_store[$k]['nowprice']=explode(',',$v)[3];
                }

                foreach($buy_store as $k=>$v){
                    $list['buy_record'][$k]=$v;
                    $list['buy_record'][$k]['buy_time']=date('m-d',$v['buy_time']);
                    //策略盈亏(收益)
                    $list['buy_record'][$k]['income']=($v['nowprice']-$v['buy_price'])*$v['stock_number'];
                }
            }else{
                //没有炒股大赛创建的持股记录
                $list['buy_record']=[];
            }
        }

        return json($list, 1, '查询成功');
    }
    //计算某个会员真实策略里的收益率
    public function shouyiInfo($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy')->where(['is_cancel_order'=>1])->where($where)->select();
        if($store){
            //收益，平台分成后的钱（包括信用金）
            $shouyi=0;
            //信用金（交易本金）
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['true_getmoney']-$v['credit']);
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
    //计算某个会员炒股大赛里的收益率
    public function shouyiInfoContest($account,$where=[]) {
        $where['account']=$account;
        $where['status']=['in',[3,4]];
        $store=Db::name('strategy_contest')->where($where)->select();
        if($store){
            //收益，(卖出价-买入价)*股数
            $shouyi=0;
            //信用金，市值，是一样的
            $credit=0;
            foreach($store as $k=>$v){
                $credit+=$v['credit'];
                $shouyi+=($v['sell_price']-$v['buy_price'])*$v['stock_number'];
            }
            //收益率
            return (round($shouyi/$credit*100,2)).'%';
        }else{
            return '0%';
        }
    }
    //PC关注动态 我的订阅的策略
    public function myfollowlist(){
        $info = Request::instance()->param();
        $list = Db::name('subscribe')->where(['account' => $info['account'],'is_del'=>1])->select();
        $arr=[];
        foreach($list as $k=>$v){
            $store=Db::name('strategy')
                ->field('id,account,stock_code,stock_name,credit,buy_price,sell_price,true_getmoney,buy_time,sell_time')
                ->where(['is_cancel_order'=>1])
                ->where(['account' => $v['subscribe_account'],'status'=>['in',[3,4]]])
                ->select();
            if($store){
                foreach($store as $kk=>$vv){
                    $store[$kk]['profit'] = (round(($vv['true_getmoney']-$vv['credit'])/$vv['credit']*100,2)).'%';//收益率
                    $store[$kk]['buy_time'] = date('Y-m-d H:i:s',$vv['buy_time']);
                    $store[$kk]['day']=(strtotime(date('Y-m-d',$vv['sell_time']).' 00:00:00')-strtotime(date('Y-m-d',$vv['buy_time']).' 00:00:00'))/24/3600;
                }
            }
            $arr=array_merge($arr,$store);
        }

        $geshu = 5;//每页个数
        if(array_key_exists('page',$info)&&$info['page']){
            $rule = ['page' =>  "^[0-9]*[1-9][0-9]*$"];
            $msg = ['page' =>  '页数必须为数字'];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($info)){
                return json('', 0, $validate->getError());
            }
        }else{
            $info['page']=1;
        }
        $page=$info['page'];
        $count = count($arr);
        $count ? $allyeshu=ceil($count/$geshu) : $allyeshu=floatval(1);
        if($page>$allyeshu){return json('', 0, '页数不存在');}
        if($count){
            foreach($arr as $k=>$v){
                if($k>=($page-1)*$geshu && $k<$page*$geshu){
                    $shuzu[]=$v;
                }
            }
        }else{
            $shuzu=[];
        }

        $result=[
            'allnum'=>$count,
            'geshu'=>$geshu,
            'list'=>$shuzu,
        ];
        return json($result, 1, '查询成功');
    }




    //查询银行卡信息
    public function selectBankCard() {
        $info = Request::instance()->param();
        $list=Db::name('bankcart')->where(['bank_require'=>1,'is_del'=>1,'account'=>$info['account']])->find();
        return json($list, 1, '提交成功');
    }


    //提交银行卡信息
    public function checkBankCard() {
        $info = Request::instance()->param();

        $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_num'].'&cardBinCheck=true');
        if(strpos($str,'errorCodes')){
            return json('', 0, '卡号输入有误，请重新输入');
        }

        $banktype=explode('"',explode(':',$str)[2])[1];
        foreach(config('terminal') as $k => $v) {
            if($v['banknum']==$banktype){
                $data['logo']=$v['logo'];
                $data['background']=$v['background'];
                $data['bank_name']=$v['name'];
                $data['bank_code']=$v['banknum'];
            }
        }

        $data['account']=$info['account'];
        $data['bank_num']=$info['bank_num'];
        $data['province']=$info['province'];
        $data['city']=$info['city'];
        $data['branchname']=$info['branchname'];
        $data['card_type']=2;
        $data['bank_require']=1;
        $data['is_del']=1;
        $data['create_time']=time();

        Db::name('bankcart')->insert($data);
        return json('', 1, '提交成功');
    }


    
    public function credit_number(){

        $list['credit_bs'] = Db::name('hide')->where('id',15)->value('value');//信用金的倍数
        $list['credit_min'] = Db::name('hide')->where('id',16)->value('value');//最低信用金
        $list['credit_max'] = Db::name('hide')->where('id',17)->value('value');//最高信用金

        return json($list, 1, '查询成功');

    }




    /**
     * 追加信用金
     * @param  int    account                     用户
     * @param  int    str_id                      策略id
     * @param  int    credit_add                  追加的信用金
     */
    public function credit_add(){

        $info = Request::instance()->param();

        if(!preg_match("/^[1-9][0-9]*$/",$info['credit_add'])){
            return json('', 0, '追加的信用金为正整数');
        }

        $strategy = Db::name('strategy')->where('id',$info['str_id'])->find();
        if(!$strategy){
            return json('', 0, '该策略不存在');
        }

        //追加的信用金的上限（倍数）
        $credit_max = Db::name('hide')->where('id',19)->value('value');
        //允许追加信用金次数
        $credit_allow_num = Db::name('hide')->where('id',20)->value('value');
        //追加信用金记录表
        $credit_add_num = Db::name('credit_add_record')->where('str_id',$info['str_id'])->order('create_time asc')->count();

        //追加的信用金的上限（具体的值）
        $credit_max = $credit_max*$strategy['credit'];

        if(($strategy['credit_add']+$info['credit_add'])>$credit_max){
            return json('', 0, '超过可允许追加的最大信用金'.$credit_max);
        }

        if(!$credit_allow_num){
            return json('', 0, '禁止追加信用金');
        }

        if($credit_add_num>=$credit_allow_num){
            return json('', 0, '您已超过信用金可追加次数');
        }

        $tactics_balance=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->value('tactics_balance');
        if($tactics_balance<$info['credit_add']){
            return json('', 0, "策略余额不足");
        }

        //止损股价 = （市值-0.75*追加后的信用金）/股数
        $after_credit = $strategy['credit']+$strategy['credit_add']+$info['credit_add'];//追加后的信用金
        $bfb = Db::name('hide')->where('id',18)->value('value');
        $loss_value = round(($strategy['market_value']-$bfb*$after_credit)/$strategy['stock_number'],2);
        $than_loss_value = round($loss_value,2);
        if($loss_value>$than_loss_value){
            //四舍 后加0.01
            $loss_value=$than_loss_value+0.01;
        }else{
            //五入
            $loss_value = round($loss_value,2);
        }
        //改变改策略的止损价
        Db::name('strategy')->where('id',$info['str_id'])->update(['loss_value'=>$loss_value]);

        //减少用户的策略余额 增加用户的冻结金额
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        $bool = Db::name('admin')
            ->where(['account'=>$info['account'],'is_del'=>1])
            ->update([
                'tactics_balance'=>$admin['tactics_balance']-$info['credit_add'],
                'frozen_money'=>$admin['frozen_money']+$info['credit_add'],
            ]);
        $bool2 = Db::name('strategy')->where('id',$info['str_id'])->setInc('credit_add',$info['credit_add']); //增加该策略的信用金

        //存记录表
        $data = [
            'account'=>$info['account'],
            'str_id'=>$info['str_id'],
            'credit_before'=>$strategy['credit'],
            'credit_add'=>$info['credit_add'],
            'create_time'=>time(),

        ];
        $bool3 = Db::name('credit_add_record')->insert($data);

        $trade=[
            'account'=>$info['account'],
            'charge_num'=>$strategy['strategy_num'],
            'trade_price'=>$info['credit_add'],
            'service_charge'=>0,
            'xbalance'=>0,
            'trade_type'=>'追加信用金',
            'trade_status'=>1,
            'detailed'=>'',
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $bool4 = Db::name('trade')->insert($trade);

        if(!$bool || !$bool2 || !$bool3 || !$bool4){
            return json('', 1, '追加信用金失败');
        }
        return json('', 1, '追加信用金成功');

    }
  
  
  
  	public function  regular_data(){
    	//是否隐藏策略 1隐藏 2不隐藏
        $hide=Db::name('hide')->where(['id' => 1])->value('value');
        //策略可进行翻倍的倍数
        $times=Db::name('hide')->where(['id' => 2])->value('value');
        //T+1买卖的手续费比例
        $poundage=Db::name('hide')->where(['id' => 4])->value('value');
        //T+D买卖的手续费比例
        $bigpoundage=Db::name('hide')->where(['id' => 8])->value('value');
        //提现手续费
        $tixianpoundage=Db::name('hide')->where(['id' => 10])->value('value');
        //递延费比例
        $defer=Db::name('hide')->where(['id' => 5])->value('value');

        $credit_bs = Db::name('hide')->where('id',15)->value('value');//信用金的倍数
        $credit_min = Db::name('hide')->where('id',16)->value('value');//最低信用金
        $credit_max = Db::name('hide')->where('id',17)->value('value');//最高信用金
        $credit_kuishun = Db::name('hide')->where('id',18)->value('value');//允许亏损的信用金百分比
      
      	$if_allow_up=Db::name('hide')->where(['id' => 49])->value('value');//是否允许客户上调止盈 1允许 2不允许
      	$surplus_value=Db::name('hide')->where(['id' => 50])->value('value');//止盈点  

        $arr = [
          'hide'=>$hide,
          'times'=>$times,
          'poundage'=>$poundage,
          'bigpoundage'=>$bigpoundage,
          'defer'=>$defer,
          'tixianpoundage'=>$tixianpoundage,
          'credit_bs'=>$credit_bs,
          'credit_min'=>$credit_min,
          'credit_max'=>$credit_max,
          'credit_kuishun'=>$credit_kuishun,
          'if_allow_up'=>$if_allow_up,
          'surplus_value'=>$surplus_value,
        ];
    
    	return json($arr, 1, '查询成功');
    
    }

  
    //首页我们的优势PC
    public function index_imgpc(){
    	$list = Db::name('web_introduce')->field('introduce_img,introduce_title,introduce_wenan')->where('is_del',1)->select();
    	return json($list, 1, '查询成功');
    }

	//app下载页面的图片PC
  	public function app_downpc(){
        $list['app_img'] = Db::name('web_appdown')->where('id',1)->value('app_img');
      	$hide = Db::name('hide')->select();
      	//下载页链接（前端自己生产二维码）
      	$list['app_down']=$hide[27]['value'].'/index/down/down';
      	//二维码中间的logo
      	$list['app_down_logo']=$hide[30]['value'];
      
      
        return json($list, 1, '查询成功');
    }

    //问题列表
    public function problem_list(){
        $res = Db::name('guide')->where('is_del',1)->select();
        return json($res, 1, '查询成功');
    }

	//PC底部
    public function bottomPC(){
        $hide = Db::name('hide')->select();
      	$list['address']=$hide[32]['value'];
      	$list['tel']=$hide[33]['value'];
      	$list['record']=$hide[36]['value'];
      
      	$list['anzhuo_down']=$hide[34]['value'];
      	$list['ios_down']=$hide[35]['value'];
      
      	
      
        return json($list, 1, '查询成功');
    }
  
  

}
