<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use Stock;
use SecretUtilTools;
use Crypt3Des;
use SignUtil;

header('Content-Type:application/json; charset=utf-8');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,Authorization");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');
class Pay extends controller
{
    public $shanghuhao='';//商户代码
    public $daishoufu='';//代收付交易密钥
    public $zhifu='';//手机快捷支付/银行卡验证/身份认证/企业认证/协议支付交易密钥
    public $zhifuurl='https://mpay.fuioupay.com:16128/h5pay/payAction.pay';//支付地址
    public $jiebangurl='https://mpay.fuioupay.com/newpropay/unbind.pay';//PC协议卡解绑
    public $xieyichaxunurl='https://mpay.fuioupay.com/newpropay/bindQuery.pay';//PC协议卡查询
    public $msgurl='https://mpay.fuioupay.com/newpropay/bindMsg.pay';//PC协议卡绑定发送短信验证码的url
    public $addbankurl='https://mpay.fuioupay.com/newpropay/bindCommit.pay';//PC提交银行卡信息的url
    public $pcchongzhiurl='https://mpay.fuioupay.com/newpropay/order.pay';//PC充值的url

    public function apiurl() {return Db::name('hide')->where(['id'=>25])->value('value');}//项目接口地址
    public function h5url() {return Db::name('hide')->where(['id'=>26])->value('value');}//项目H5地址

    //H5充值
    public function chargeMoney() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //账户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        if($admin['is_true']!=1){
            return json('', 0, '请实名认证后，再进行充值');
        }
        //银行卡信息
        $bankcart=Db::name('bankcart')->where(['account'=>$info['account'],'is_del'=>1,'bank_require'=>1])->find();
        if(!$bankcart){
            return json('', 0, '请绑定银行卡后再进行充值');
        }
        //添加待付款的充值记录
        $para=[
            'account'=>$info['account'],
            'charge_num'=>date('YmdHis').rand(1000,9999),
            'trade_price'=>$info['money'],
            'xbalance'=>$admin['balance']+$info['money'],
            'trade_type'=>'充值',
            'trade_status'=>2,
            'sort'=>0,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        Db::name('trade')->insert($para);

        $data=[
            'order_id'=>$para['charge_num'],
            'account'=>$info['account'],
            'money'=>$info['money']*100,
            'bank_card'=>$bankcart['bank_num'],
            'true_name'=>$admin['true_name'],
            'card_id'=>$admin['card_id'],
        ];

        $this->fuyoupay($data);
    }

    //H5充值 提交富有
    function fuyoupay($data){
        $zhifuurl=$this->zhifuurl; //支付的url
        $key = $this->zhifu; //密钥
        $SecretUtilTools=new SecretUtilTools();

        // 定义变量并设置为空值
        $mchntcdErr = $typeErr = $versionErr = $logotpErr = $mchntorderidErr = $useridErr = $amtErr = $bankcardErr = $backurlErr =
        $reurlErr = $homeurlErr = $nameErr = $idtypeErr = $idnoErr = $signtpErr ="";
        $mchntcd = $type = $gender = $version = $logotp = $mchntorderid = $userid = $amt = $bankcard = $backurl =
        $reurl = $homeurl = $name = $idtype = $idno = $signtp = $rem1 = $rem2 = $rem3 = $fm = $sign = "" ;
        $arror = "";

        $_POST=[
            'MCHNTCD'=>$this->shanghuhao,
            'TYPE'=>'10',
            'VERSION'=>'2.0',
            'LOGOTP'=>'0',
            'MCHNTORDERID'=>$data['order_id'],
            'USERID'=>$data['account'],
            'AMT'=>$data['money'],
            'BANKCARD'=>$data['bank_card'],
            'BACKURL'=>$this->apiurl().'/index/pay/callback',
            'REURL'=>$this->h5url().'/index/center/personal_recharge.html',
            'HOMEURL'=>$this->h5url().'/index/center/personal_account.html',
            'NAME'=>$data['true_name'],
            'IDTYPE'=>'0',
            'IDNO'=>$data['card_id'],
            'SIGNTP'=>'md5',
            'REM1'=>'',
            'REM2'=>'',
            'REM3'=>'',
        ];

        if (empty($_POST["MCHNTCD"])) {
            $mchntcdErr = "商户代码是必填的";
        } else {
            $mchntcd = $SecretUtilTools->test_input($_POST["MCHNTCD"]);
        }

        if (empty($_POST["TYPE"])) {
            $typeErr = "交易类型是必填的";
        } else {
            $type = $SecretUtilTools->test_input($_POST["TYPE"]);
        }

        if (empty($_POST["VERSION"])) {
            $versionErr = "版本号是必选的";
        } else {
            $version = $SecretUtilTools->test_input($_POST["VERSION"]);
        }

        $logotp = $SecretUtilTools->test_input($_POST["LOGOTP"]);

        if (empty($_POST["MCHNTORDERID"])) {
            $mchntorderidErr = "商户订单号是必选的";
        } else {
            $mchntorderid = $SecretUtilTools->test_input($_POST["MCHNTORDERID"]);
        }

        if (empty($_POST["USERID"])) {
            $useridErr = "用户编号是必选的";
        } else {
            $userid = $SecretUtilTools->test_input($_POST["USERID"]);
        }

        if (empty($_POST["AMT"])) {
            $amtErr = "交易金额是必选的";
        } else {
            $amt = $SecretUtilTools->test_input($_POST["AMT"]);
        }

        if (empty($_POST["BANKCARD"])) {
            $bankcardErr = "银行卡号是必选的";
        } else {
            $bankcard = $SecretUtilTools->test_input($_POST["BANKCARD"]);
        }

        if (empty($_POST["BACKURL"])) {
            $backurlErr = "后台通知URL是必选的";
        } else {
            $backurl = $SecretUtilTools->test_input($_POST["BACKURL"]);
        }

        if (empty($_POST["REURL"])) {
            $reurlErr = "支付失败URL是必选的";
        } else {
            $reurl = $SecretUtilTools->test_input($_POST["REURL"]);
        }

        if (empty($_POST["HOMEURL"])) {
            $homeurlErr = "页面通知URL是必选的";
        } else {
            $homeurl = $SecretUtilTools->test_input($_POST["HOMEURL"]);
        }

        if (empty($_POST["NAME"])) {
            $nameErr = "姓名是必选的";
        } else {
            $name = $SecretUtilTools->test_input($_POST["NAME"]);
        }

        $idtype = $SecretUtilTools->test_input($_POST["IDTYPE"]);

        if (empty($_POST["IDNO"])) {
            $idnoErr = "身份证号码是必选的";
        } else {
            $idno = $SecretUtilTools->test_input($_POST["IDNO"]);
        }

        if (empty($_POST["SIGNTP"])) {
            $signtpErr = "签名方式是必选的";
        } else {
            $signtp = $SecretUtilTools->test_input($_POST["SIGNTP"]);
        }

        $rem1 = $SecretUtilTools->test_input($_POST["REM1"]);
        $rem2 = $SecretUtilTools->test_input($_POST["REM2"]);
        $rem3 = $SecretUtilTools->test_input($_POST["REM3"]);


        $sign = $type."|".$version."|".$mchntcd."|".$mchntorderid ."|".$userid."|".$amt."|".$bankcard."|".$backurl."|".
            $name."|".$idno."|".$idtype."|".$logotp."|".$homeurl."|".$reurl."|".$key;
        $sign = str_replace(' ', '', $sign);

        $fm = "<ORDER>"
            ."<VERSION>".$version."</VERSION>"
            ."<LOGOTP>".$logotp."</LOGOTP>"
            ."<MCHNTCD>".$mchntcd."</MCHNTCD> "
            ."<TYPE>".$type."</TYPE>"
            ."<MCHNTORDERID>".$mchntorderid."</MCHNTORDERID>"
            ."<USERID>".$userid."</USERID>"
            ."<AMT>".$amt."</AMT>"
            ."<BANKCARD>".$bankcard."</BANKCARD>"
            ."<NAME>".$name."</NAME>"
            ."<IDTYPE>".$idtype."</IDTYPE>"
            ."<IDNO>".$idno."</IDNO>"
            ."<BACKURL>".$backurl."</BACKURL>"
            ."<HOMEURL>".$homeurl."</HOMEURL>"
            ."<REURL>".$reurl."</REURL>"
            ."<REM1>".$rem1."</REM1>"
            ."<REM2>".$rem2."</REM2>"
            ."<REM3>".$rem3."</REM3>"
            ."<SIGNTP>".$signtp."</SIGNTP>"
            ."<SIGN>".md5($sign)."</SIGN>"
            ."</ORDER>";

        $mstr = SecretUtilTools::encryptForDES($fm , $key );
        $sHtml = "<form id='paysubmit' name='llpaysubmit' action='" . $zhifuurl . "' method='post'>";
        $sHtml .= "<input type='hidden' name='VERSION' value='" . $version . "'/>";
        $sHtml .= "<input type='hidden' name='ENCTP' value='1'/>";
        $sHtml .= "<input type='hidden' name='LOGOTP' value='" . $logotp . "'/>";
        $sHtml .= "<input type='hidden' name='MCHNTCD' value='" . $mchntcd . "'/>";
        $sHtml .= "<input type='hidden' name='FM' value='" . str_replace(' ', '+', str_replace(' ', '', $mstr)) . "'/>";
        $sHtml = $sHtml."<script>document.forms['paysubmit'].submit();</script>";
        echo $sHtml;
    }

    //APP充值
    public function chargeMoneyapi() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //账户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        if($admin['is_true']!=1){
            return json('', 0, '请实名认证后，再进行充值');
        }
        //银行卡信息
        $bankcart=Db::name('bankcart')->where(['account'=>$info['account'],'is_del'=>1,'bank_require'=>1])->find();
        if(!$bankcart){
            return json('', 0, '请绑定银行卡后再进行充值');
        }
        //添加待付款的充值记录
        $para=[
            'account'=>$info['account'],
            'charge_num'=>date('YmdHis').rand(1000,9999),
            'trade_price'=>$info['money'],
            'xbalance'=>$admin['balance']+$info['money'],
            'trade_type'=>'充值',
            'trade_status'=>2,
            'sort'=>0,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        Db::name('trade')->insert($para);

        $data=[
            'order_id'=>$para['charge_num'],
            'account'=>$info['account'],
            'money'=>$info['money']*100,
            'bank_card'=>$bankcart['bank_num'],
            'true_name'=>$admin['true_name'],
            'card_id'=>$admin['card_id'],
        ];

        $this->fuyoupayapi($data);
    }

    //APP充值 提交富有
    function fuyoupayapi($data){
        $zhifuurl=$this->zhifuurl; //支付的url
        $key = $this->zhifu; //密钥
        $SecretUtilTools=new SecretUtilTools();

        // 定义变量并设置为空值
        $mchntcdErr = $typeErr = $versionErr = $logotpErr = $mchntorderidErr = $useridErr = $amtErr = $bankcardErr = $backurlErr =
        $reurlErr = $homeurlErr = $nameErr = $idtypeErr = $idnoErr = $signtpErr ="";
        $mchntcd = $type = $gender = $version = $logotp = $mchntorderid = $userid = $amt = $bankcard = $backurl =
        $reurl = $homeurl = $name = $idtype = $idno = $signtp = $rem1 = $rem2 = $rem3 = $fm = $sign = "" ;
        $arror = "";

        $_POST=[
            'MCHNTCD'=>$this->shanghuhao,
            'TYPE'=>'10',
            'VERSION'=>'2.0',
            'LOGOTP'=>'0',
            'MCHNTORDERID'=>$data['order_id'],
            'USERID'=>$data['account'],
            'AMT'=>$data['money'],
            'BANKCARD'=>$data['bank_card'],
            'BACKURL'=>$this->apiurl().'/index/pay/callback',
            'REURL'=>$this->h5url().'/index/center/errorpay.html',
            'HOMEURL'=>$this->h5url().'/index/center/successpay.html',
            'NAME'=>$data['true_name'],
            'IDTYPE'=>'0',
            'IDNO'=>$data['card_id'],
            'SIGNTP'=>'md5',
            'REM1'=>'',
            'REM2'=>'',
            'REM3'=>'',
        ];

        if (empty($_POST["MCHNTCD"])) {
            $mchntcdErr = "商户代码是必填的";
        } else {
            $mchntcd = $SecretUtilTools->test_input($_POST["MCHNTCD"]);
        }

        if (empty($_POST["TYPE"])) {
            $typeErr = "交易类型是必填的";
        } else {
            $type = $SecretUtilTools->test_input($_POST["TYPE"]);
        }

        if (empty($_POST["VERSION"])) {
            $versionErr = "版本号是必选的";
        } else {
            $version = $SecretUtilTools->test_input($_POST["VERSION"]);
        }

        $logotp = $SecretUtilTools->test_input($_POST["LOGOTP"]);

        if (empty($_POST["MCHNTORDERID"])) {
            $mchntorderidErr = "商户订单号是必选的";
        } else {
            $mchntorderid = $SecretUtilTools->test_input($_POST["MCHNTORDERID"]);
        }

        if (empty($_POST["USERID"])) {
            $useridErr = "用户编号是必选的";
        } else {
            $userid = $SecretUtilTools->test_input($_POST["USERID"]);
        }

        if (empty($_POST["AMT"])) {
            $amtErr = "交易金额是必选的";
        } else {
            $amt = $SecretUtilTools->test_input($_POST["AMT"]);
        }

        if (empty($_POST["BANKCARD"])) {
            $bankcardErr = "银行卡号是必选的";
        } else {
            $bankcard = $SecretUtilTools->test_input($_POST["BANKCARD"]);
        }

        if (empty($_POST["BACKURL"])) {
            $backurlErr = "后台通知URL是必选的";
        } else {
            $backurl = $SecretUtilTools->test_input($_POST["BACKURL"]);
        }

        if (empty($_POST["REURL"])) {
            $reurlErr = "支付失败URL是必选的";
        } else {
            $reurl = $SecretUtilTools->test_input($_POST["REURL"]);
        }

        if (empty($_POST["HOMEURL"])) {
            $homeurlErr = "页面通知URL是必选的";
        } else {
            $homeurl = $SecretUtilTools->test_input($_POST["HOMEURL"]);
        }

        if (empty($_POST["NAME"])) {
            $nameErr = "姓名是必选的";
        } else {
            $name = $SecretUtilTools->test_input($_POST["NAME"]);
        }

        $idtype = $SecretUtilTools->test_input($_POST["IDTYPE"]);

        if (empty($_POST["IDNO"])) {
            $idnoErr = "身份证号码是必选的";
        } else {
            $idno = $SecretUtilTools->test_input($_POST["IDNO"]);
        }

        if (empty($_POST["SIGNTP"])) {
            $signtpErr = "签名方式是必选的";
        } else {
            $signtp = $SecretUtilTools->test_input($_POST["SIGNTP"]);
        }

        $rem1 = $SecretUtilTools->test_input($_POST["REM1"]);
        $rem2 = $SecretUtilTools->test_input($_POST["REM2"]);
        $rem3 = $SecretUtilTools->test_input($_POST["REM3"]);


        $sign = $type."|".$version."|".$mchntcd."|".$mchntorderid ."|".$userid."|".$amt."|".$bankcard."|".$backurl."|".
            $name."|".$idno."|".$idtype."|".$logotp."|".$homeurl."|".$reurl."|".$key;
        $sign = str_replace(' ', '', $sign);

        $fm = "<ORDER>"
            ."<VERSION>".$version."</VERSION>"
            ."<LOGOTP>".$logotp."</LOGOTP>"
            ."<MCHNTCD>".$mchntcd."</MCHNTCD> "
            ."<TYPE>".$type."</TYPE>"
            ."<MCHNTORDERID>".$mchntorderid."</MCHNTORDERID>"
            ."<USERID>".$userid."</USERID>"
            ."<AMT>".$amt."</AMT>"
            ."<BANKCARD>".$bankcard."</BANKCARD>"
            ."<NAME>".$name."</NAME>"
            ."<IDTYPE>".$idtype."</IDTYPE>"
            ."<IDNO>".$idno."</IDNO>"
            ."<BACKURL>".$backurl."</BACKURL>"
            ."<HOMEURL>".$homeurl."</HOMEURL>"
            ."<REURL>".$reurl."</REURL>"
            ."<REM1>".$rem1."</REM1>"
            ."<REM2>".$rem2."</REM2>"
            ."<REM3>".$rem3."</REM3>"
            ."<SIGNTP>".$signtp."</SIGNTP>"
            ."<SIGN>".md5($sign)."</SIGN>"
            ."</ORDER>";

        $mstr = SecretUtilTools::encryptForDES($fm , $key );
        $sHtml = "<form id='paysubmit' name='llpaysubmit' action='" . $zhifuurl . "' method='post'>";
        $sHtml .= "<input type='hidden' name='VERSION' value='" . $version . "'/>";
        $sHtml .= "<input type='hidden' name='ENCTP' value='1'/>";
        $sHtml .= "<input type='hidden' name='LOGOTP' value='" . $logotp . "'/>";
        $sHtml .= "<input type='hidden' name='MCHNTCD' value='" . $mchntcd . "'/>";
        $sHtml .= "<input type='hidden' name='FM' value='" . str_replace(' ', '+', str_replace(' ', '', $mstr)) . "'/>";
        $sHtml = $sHtml."<script>document.forms['paysubmit'].submit();</script>";
        echo $sHtml;
    }

    //APP充值成功的显示页面
    public function successpay(){
        echo '恭喜您充值成功';
    }

    //APP充值失败的显示页面
    public function errorpay(){
        echo '充值失败';
    }

    //H5 APP充值之后的回调
    public function callback(){
        $str=file_get_contents('php://input');

        $arr = explode('&', $str);
        foreach($arr as $k=>$v){
            $list=explode('=',$v);
            if($list[0]=='MCHNTORDERID'){
                $MCHNTORDERID=$list[1];//商户订单号
            }elseif($list[0]=='SIGN'){
                $SIGN=$list[1];
            }elseif($list[0]=='MCHNTCD'){
                $MCHNTCD=$list[1];//商户号
            }elseif($list[0]=='BANKCARD'){
                $BANKCARD=$list[1];
            }elseif($list[0]=='VERSION'){
                $VERSION=$list[1];
            }elseif($list[0]=='RESPONSECODE'){
                $RESPONSECODE=$list[1];//响应代码
            }elseif($list[0]=='ORDERID'){
                $ORDERID=$list[1];//富友订单号
            }elseif($list[0]=='RESPONSEMSG'){
                $RESPONSEMSG=urldecode($list[1]);//响应中文描述
            }elseif($list[0]=='AMT'){
                $AMT=$list[1];
            }elseif($list[0]=='TYPE'){
                $TYPE=$list[1];
            }
        }
        if($MCHNTORDERID&&$SIGN&&$MCHNTCD&&$BANKCARD&&$VERSION&&$RESPONSECODE&&$ORDERID&&$RESPONSEMSG&&$AMT&&$TYPE&&$RESPONSECODE==='0000'){

            $res=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->find();
            if($res['trade_status']!=1){
                //修改充值订单交易状态
                Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->update(['trade_status'=>1]);
                $account=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->value('account');
                $balance=Db::name('admin')->where(['account'=>$account])->value('balance');
                //确认用户的实名认证信息并添加金额
                Db::name('admin')->where(['account'=>$account])->update(['is_true'=>1,'balance'=>$balance+$AMT/100]);
            }

        }
    }

    //用户提现（商户付款）是否允许
    public function isExtractMoney() {
      	return json('', 1, '可以提现');
      	
        $data['account']=input('account');
        $data['trade_type']='提现';
        $data['create_time']=['egt',strtotime(date('Y-m-d'))];
        $res=Db::name("trade")->where($data)->find();
        if($res){
            return json('', 0, '今天已经提现过了');
        }
        return json('', 1, '可以提现');
    }

    //用户提现（商户付款）
    public function extractMoney() {
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        $admin=Db::name('admin')->where(['account' => $info['account'],'is_del'=>1])->find();
        if($admin['trade_pwd']!=md5($info['trade_pwd'])){
            return json('', 0, '交易密码不正确');
        }

        if(($info['money'])>$admin['balance']){
            return json('', 0, '提现金额超过余额');
        }

        //提现手续费
        $tixian=Db::name('hide')->where(['id'=>10])->value('value');
        if(($info['money'])<=$tixian){
            return json('', 0, '提现金额小于等于手续费'.$tixian);
        }

        Db::startTrans();
        //创建提现订单
        $para=[
            'account'=>$info['account'],
            'charge_num'=>date('YmdHis',time()).rand(1000,9999),
            'trade_price'=>$info['money']-$tixian,
            'service_charge'=>$tixian,
            'trade_type'=>'提现',
            'trade_status'=>5,
            'sort'=>0,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result1=Db::name('trade')->insert($para);

        //先进行扣钱，同意后在进行真实打钱，拒绝后钱再加回来
        $result2=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->update(['balance'=>$admin['balance']-$info['money']]);

        if(!$result1 || !$result2){
            Db::rollback();
            return json('', 0, '提现发起失败');
        }
        Db::commit();

        return json('', 1, '提现发起成功,系统将在24小时内为您审核');
    }

    //用户提现（商家付款）之后的回调
    public function extractCallback(){
        $str=file_get_contents('php://input');

      	file_put_content('123.txt','提现回调成功');
      
        $str=urldecode($str);
        $arr = explode('&', $str);
        foreach($arr as $k=>$v){
            $list=explode('=',$v);
            if($list[0]=='orderno'){
                $orderno=$list[1];//商户请求流水
            }elseif($list[0]=='merdt'){
                $merdt=$list[1];//请求日期
            }elseif($list[0]=='fuorderno'){
                $fuorderno=$list[1];//富友流水
            }elseif($list[0]=='accntno'){
                $accntno=$list[1];//账号
            }elseif($list[0]=='accntnm'){
                $accntnm=urldecode($list[1]);//账户名称
            }elseif($list[0]=='bankno'){
                $bankno=$list[1];//总行代码
            }elseif($list[0]=='amt'){
                $amt=$list[1];//金额
            }elseif($list[0]=='state'){
                $state=$list[1];//状态
            }elseif($list[0]=='result'){
                $result=urldecode($list[1]);//交易结果
            }elseif($list[0]=='reason'){
                $reason=urldecode($list[1]);//结果原因
            }elseif($list[0]=='mac'){
                $mac=$list[1];//校验值
            }
        }

        if($orderno&&$merdt&&$fuorderno&&$accntno&&$accntnm&&$bankno&&$amt&&$state==1&&$result&&$reason&&$mac){

            $res=Db::name('trade')->where(['charge_num'=>$orderno])->find();
            if($res['trade_status']!=1){
                //修改提现订单交易状态
                Db::name('trade')->where(['charge_num'=>$orderno])->update(['trade_status'=>1]);
            }

        }
    }

    //查看是否绑定银行卡
    public function bindingbank(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $banklist=Db::name('bankcart')
            ->field('id,account,bank_num,bank_name,no_agree,logo,background')
            ->where(['account'=>input('account'),'is_del'=>1,'bank_require'=>1])
            ->order('id desc')
            ->find();
        if($banklist&&$banklist['no_agree']){
            $banklist['no_agree']=1;
        }else{
            $banklist['no_agree']=0;
        }
        return json($banklist, 1, '查询成功');
    }

    //查询银行卡信息
    public function selectBankCard() {
        $info = Request::instance()->param();
        $list=Db::name('bankcart')
            ->field('id,account,bank_num,bank_name,logo,background,create_time')
            ->where(['bank_require'=>1,'is_del'=>1,'account'=>$info['account']])
            ->find();
        return json($list, 1, '查询成功');
    }

    //H5提交银行卡信息
    public function checkBankCard() {
        $info = Request::instance()->param();

        $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_num'].'&cardBinCheck=true');
        if(strpos($str,'errorCodes')){
            return json('', 0, '卡号输入有误，请重新输入');
        }
		$data=[];
        $banktype=explode('"',explode(':',$str)[2])[1];
        foreach(config('terminal') as $k => $v) {
            if($v['banknum']==$banktype){
                $data['logo']=$v['logo'];
                $data['background']=$v['background'];
                $data['bank_name']=$v['name'];
            }
        }
      	if(!isset($data['background'])){
            $data['logo']=config('bank_default')['logo'];
            $data['background']=config('bank_default')['background'];
            $data['bank_name']='银行卡';
        }

        $data['account']=$info['account'];
        $data['bank_num']=$info['bank_num'];
      	$data['bank_address']=$info['bank_address'];
        $data['card_type']=2;
        $data['bank_require']=1;
        $data['is_del']=1;
        $data['create_time']=time();

        Db::name('bankcart')->insert($data);
      
      	push($info['account'],'恭喜您，您的银行卡已经绑定成功！',1);
      
        return json($info['bank_num'], 1, '提交成功');
    }

    //PC协议卡解绑
    public function PCuntyingbank(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $zhifuurl=$this->jiebangurl;
        $key = $this->zhifu; //密钥

        $VERSION='1.0';
        $MCHNTCD=$this->shanghuhao;
        $USERID=$info['account'];
        $PROTOCOLNO=Db::name('bankcart')->where(['account'=>$info['account'],'bank_require'=>1,'is_del'=>1])->value('no_agree');


        $SIGN = $VERSION."|".$MCHNTCD."|".$USERID ."|".$PROTOCOLNO."|".$key;
        $SIGN = md5($SIGN);

        $fm='<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            ."<REQUEST>"
            ."<VERSION>".$VERSION."</VERSION>"
            ."<MCHNTCD>".$MCHNTCD."</MCHNTCD>"
            ."<USERID>".$USERID."</USERID>"
            ."<PROTOCOLNO>".$PROTOCOLNO."</PROTOCOLNO>"
            ."<SIGN>".$SIGN."</SIGN>"
            ."</REQUEST>";

        $res = urlencode(SecretUtilTools::encryptForDES($fm , $key ));

        $sHtml=file_get_contents($zhifuurl.'?MCHNTCD='.$MCHNTCD.'&APIFMS='.$res);

        $Crypt3Des=new Crypt3Des();

        $result=$Crypt3Des->decrypt_base64($sHtml,$key);

        $arr=json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        echo "<pre>";
        print_r($arr);
        echo "</pre>";die;
    }

    //PC协议卡查询
    public function PCselectbank(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();

        $zhifuurl=$this->xieyichaxunurl;
        $key = $this->zhifu; //密钥

        $VERSION='1.0';
        $MCHNTCD=$this->shanghuhao;
        $USERID=$info['account'];

        $SIGN = $VERSION."|".$MCHNTCD."|".$USERID."|".$key;
        $SIGN = md5($SIGN);

        $fm='<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            ."<REQUEST>"
            ."<VERSION>".$VERSION."</VERSION>"
            ."<MCHNTCD>".$MCHNTCD."</MCHNTCD>"
            ."<USERID>".$USERID."</USERID>"
            ."<SIGN>".$SIGN."</SIGN>"
            ."</REQUEST>";

        $res = urlencode(SecretUtilTools::encryptForDES($fm , $key ));

        $sHtml=file_get_contents($zhifuurl.'?MCHNTCD='.$MCHNTCD.'&APIFMS='.$res);

        $Crypt3Des=new Crypt3Des();

        $result=$Crypt3Des->decrypt_base64($sHtml,$key);

        $arr=json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        echo "<pre>";
        print_r($arr);
        echo "</pre>";die;
    }

    //PC协议卡绑定发送短信验证码
    public function PCsendmes(){
        if(token()==100){return json('', 100, '您的账号已在别的设备登陆');}

        $info = Request::instance()->param();
        //账户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        if($admin['is_true']!=1){
            return json('', 0, '请实名认证后，再进行充值');
        }

        $zhifuurl=$this->msgurl; //PC协议卡绑定发送短信验证码的url
        $key = $this->zhifu; //密钥

        $VERSION='1.0';
        $MCHNTCD=$this->shanghuhao;
        $TRADEDATE=date('Ymd');
        $MCHNTSSN=date('YmdHis').rand(1000,9999);
        $USERID=$info['account'];
        $ACCOUNT=$admin['true_name'];
        $CARDNO=$info['bank_card'];
        $IDTYPE='0';
        $IDCARD=$admin['card_id'];
        $MOBILENO=$info['account'];

        $SIGN = $VERSION."|".$MCHNTSSN."|".$MCHNTCD ."|".$USERID."|".$ACCOUNT."|".$CARDNO."|".$IDTYPE."|".
            $IDCARD."|".$MOBILENO."|".$key;
        $SIGN = md5($SIGN);

        $fm='<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            ."<REQUEST>"
            ."<VERSION>".$VERSION."</VERSION>"
            ."<MCHNTCD>".$MCHNTCD."</MCHNTCD>"
            ."<USERID>".$USERID."</USERID>"
            ."<TRADEDATE>".$TRADEDATE."</TRADEDATE>"
            ."<MCHNTSSN>".$MCHNTSSN."</MCHNTSSN>"
            ."<ACCOUNT>".$ACCOUNT."</ACCOUNT>"
            ."<CARDNO>".$CARDNO."</CARDNO>"
            ."<IDTYPE>".$IDTYPE."</IDTYPE>"
            ."<IDCARD>".$IDCARD."</IDCARD>"
            ."<MOBILENO>".$MOBILENO."</MOBILENO>"
            ."<SIGN>".$SIGN."</SIGN>"
            ."</REQUEST>";

        $res = urlencode(SecretUtilTools::encryptForDES($fm , $key ));

        $sHtml=file_get_contents($zhifuurl.'?MCHNTCD='.$MCHNTCD.'&APIFMS='.$res);

        $Crypt3Des=new Crypt3Des();

        $result=$Crypt3Des->decrypt_base64($sHtml,$key);

        $arr=json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if($arr['RESPONSECODE']=='0000'){

            $data=[
                'account'=>$USERID,
                'bank_num'=>$CARDNO,
                'bank_require'=>2,
                'is_del'=>1,
                'create_time'=>time(),
                'waternum'=>$MCHNTSSN,
            ];
            Db::name('bankcart')->insert($data);

            return json('', 1, '发送成功');
        }else{
            return json('', 0, $arr['RESPONSEMSG']);
        }
    }

    //PC提交银行卡信息
    public function PCcheckBankCard() {
        $info = Request::instance()->param();

        //账户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        if($admin['is_true']!=1){
            return json('', 0, '请实名认证后，再进行充值');
        }

        $bankcart=Db::name('bankcart')
            ->where(['account'=>$info['account'],'bank_require'=>2,'is_del'=>1])
            ->order('create_time desc')
            ->find();

        $zhifuurl=$this->addbankurl; //PC提交银行卡信息的url
        $key = $this->zhifu; //密钥

        $VERSION='1.0';
        $MCHNTCD=$this->shanghuhao;
        $TRADEDATE=substr($bankcart['waternum'],0,8);
        $MCHNTSSN=$bankcart['waternum'];
        $USERID=$info['account'];
        $ACCOUNT=$admin['true_name'];
        $CARDNO=$info['bank_card'];
        $IDTYPE='0';
        $IDCARD=$admin['card_id'];
        $MOBILENO=$info['account'];
        $MSGCODE=$info['message'];

        $SIGN = $VERSION."|".$MCHNTSSN."|".$MCHNTCD ."|".$USERID."|".$ACCOUNT."|".$CARDNO."|".$IDTYPE."|".
            $IDCARD."|".$MOBILENO."|".$MSGCODE."|".$key;
        $SIGN = md5($SIGN);

        $fm='<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            ."<REQUEST>"
            ."<VERSION>".$VERSION."</VERSION>"
            ."<MCHNTCD>".$MCHNTCD."</MCHNTCD>"
            ."<USERID>".$USERID."</USERID>"
            ."<TRADEDATE>".$TRADEDATE."</TRADEDATE>"
            ."<MCHNTSSN>".$MCHNTSSN."</MCHNTSSN>"
            ."<ACCOUNT>".$ACCOUNT."</ACCOUNT>"
            ."<CARDNO>".$CARDNO."</CARDNO>"
            ."<IDTYPE>".$IDTYPE."</IDTYPE>"
            ."<IDCARD>".$IDCARD."</IDCARD>"
            ."<MOBILENO>".$MOBILENO."</MOBILENO>"
            ."<MSGCODE>".$MSGCODE."</MSGCODE>"
            ."<SIGN>".$SIGN."</SIGN>"
            ."</REQUEST>";

        $res = urlencode(SecretUtilTools::encryptForDES($fm , $key ));

        $sHtml=file_get_contents($zhifuurl.'?MCHNTCD='.$MCHNTCD.'&APIFMS='.$res);

        $Crypt3Des=new Crypt3Des();

        $result=$Crypt3Des->decrypt_base64($sHtml,$key);

        $arr=json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        if($arr['RESPONSECODE']=='0000'){

            $str=file_get_contents('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo='.$info['bank_card'].'&cardBinCheck=true');
            if(strpos($str,'errorCodes')){
                return json('', 0, '卡号输入有误，请重新输入');
            }

            $banktype=explode('"',explode(':',$str)[2])[1];
            foreach(config('terminal') as $k => $v) {
                if($v['banknum']==$banktype){
                    $data['logo']=$v['logo'];
                    $data['background']=$v['background'];
                    $data['bank_name']=$v['name'];
                }
            }

            $data['no_agree']=$arr['PROTOCOLNO'];
            $data['bank_require']=1;

            Db::name('bankcart')->where(['id'=>$bankcart['id']])->update($data);
            return json('', 1, '提交成功');
        }else{
            return json('', 0, $arr['RESPONSEMSG']);
        }
    }

    //PC充值
    public function PCchargeMoney() {
        $info = Request::instance()->param();

        //账户信息
        $admin=Db::name('admin')->where(['account'=>$info['account'],'is_del'=>1])->find();
        if($admin['is_true']!=1){
            return json('', 0, '请实名认证后，再进行充值');
        }
        //银行卡信息
        $bankcart=Db::name('bankcart')->where(['account'=>$info['account'],'is_del'=>1,'bank_require'=>1])->find();
        if(!$bankcart['no_agree']){
            return json('', 0, '请绑定银行卡后再进行充值');
        }
        //添加待付款的充值记录
        $para=[
            'account'=>$info['account'],
            'charge_num'=>date('YmdHis').rand(1000,9999),
            'trade_price'=>$info['money'],
            'xbalance'=>$admin['balance']+$info['money'],
            'trade_type'=>'充值',
            'trade_status'=>2,
            'sort'=>0,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        Db::name('trade')->insert($para);

        $data=[
            'order_id'=>$para['charge_num'],
            'account'=>$info['account'],
            'money'=>$info['money']*100,
            'bank_card'=>$bankcart['bank_num'],
            'true_name'=>$admin['true_name'],
            'card_id'=>$admin['card_id'],
            'no_agree'=>$bankcart['no_agree'],
        ];

        return $this->PCfuyoupay($data);
    }

    //PC充值 提交富有
    public function PCfuyoupay($data) {
        $zhifuurl=$this->pcchongzhiurl; //PC充值的url
        $key = $this->zhifu; //密钥

        $VERSION='1.0';
        $USERIP=gethostbyname($_SERVER['SERVER_NAME']);
        $MCHNTCD=$this->shanghuhao;
        $TYPE='03';
        $MCHNTORDERID=$data['order_id'];
        $USERID=$data['account'];
        $AMT=$data['money'];
        $PROTOCOLNO=$data['no_agree'];
        $NEEDSENDMSG=0;
        $BACKURL=$this->apiurl().'/index/pay/PCcallback';
        $SIGNTP='md5';


        $SIGN = $TYPE."|".$VERSION."|".$MCHNTCD ."|".$MCHNTORDERID."|".$USERID."|".$PROTOCOLNO."|".$AMT."|".$BACKURL."|".$USERIP."|".$key;
        $SIGN = md5($SIGN);

        $fm='<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
            ."<REQUEST>"
            ."<VERSION>".$VERSION."</VERSION>"
            ."<USERIP>".$USERIP."</USERIP>"
            ."<MCHNTCD>".$MCHNTCD."</MCHNTCD>"
            ."<USERID>".$USERID."</USERID>"
            ."<TYPE>".$TYPE."</TYPE>"
            ."<MCHNTORDERID>".$MCHNTORDERID."</MCHNTORDERID>"
            ."<USERID>".$USERID."</USERID>"
            ."<AMT>".$AMT."</AMT>"
            ."<PROTOCOLNO>".$PROTOCOLNO."</PROTOCOLNO>"
            ."<NEEDSENDMSG>".$NEEDSENDMSG."</NEEDSENDMSG>"
            ."<BACKURL>".$BACKURL."</BACKURL>"
            ."<REM1></REM1>"
            ."<REM2></REM2>"
            ."<REM3></REM3>"
            ."<SIGNTP>".$SIGNTP."</SIGNTP>"
            ."<SIGN>".$SIGN."</SIGN>"
            ."</REQUEST>";

        $res = urlencode(SecretUtilTools::encryptForDES($fm , $key ));

        $sHtml=file_get_contents($zhifuurl.'?MCHNTCD='.$MCHNTCD.'&APIFMS='.$res);

        $Crypt3Des=new Crypt3Des();

        $result=$Crypt3Des->decrypt_base64($sHtml,$key);

        $arr=json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        if($arr['RESPONSECODE']=='0000'){

            $res=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->find();
            if($res['trade_status']!=1){
                //修改充值订单交易状态
                Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->update(['trade_status'=>1]);
                $account=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->value('account');
                $balance=Db::name('admin')->where(['account'=>$account])->value('balance');
                //确认用户的实名认证信息并添加金额
                Db::name('admin')->where(['account'=>$account])->update(['balance'=>$balance+$AMT/100]);
            }

            return json('', 1, '充值成功');
        }else{
            return json('', 0, $arr['RESPONSEMSG']);
        }
    }

//    //PC充值之后的回调
//    public function PCcallback(){
//        $str=file_get_contents('php://input');
//        $str=json_encode($str,JSON_UNESCAPED_UNICODE);
//        file_put_contents('ceshiceshi.log',$str."1\n", FILE_APPEND);
//
//        $str2=json_encode($_POST,JSON_UNESCAPED_UNICODE);
//        file_put_contents('ceshiceshi.log',$str2."2\n", FILE_APPEND);
//
//        $info = Request::instance()->param(true);
//        $str3=json_encode($info,JSON_UNESCAPED_UNICODE);
//        file_put_contents('ceshiceshi.log',$str3."3\n", FILE_APPEND);
//
//        file_put_contents('ceshiceshi.log',"4\n", FILE_APPEND);
//
//        $arr = explode('&', $str);
//        foreach($arr as $k=>$v){
//            $list=explode('=',$v);
//            if($list[0]=='TYPE'){
//                $TYPE=$list[1];
//            }elseif($list[0]=='VERSION'){
//                $VERSION=$list[1];
//            }elseif($list[0]=='MCHNTCD'){
//                $MCHNTCD=$list[1];
//            }elseif($list[0]=='USERID'){
//                $USERID=$list[1];
//            }elseif($list[0]=='RESPONSECODE'){
//                $RESPONSECODE=$list[1];
//            }elseif($list[0]=='RESPONSEMSG'){
//                $RESPONSEMSG=$list[1];
//            }elseif($list[0]=='MCHNTORDERID'){
//                $MCHNTORDERID=$list[1];
//            }elseif($list[0]=='ORDERID'){
//                $ORDERID=urldecode($list[1]);
//            }elseif($list[0]=='AMT'){
//                $AMT=$list[1];
//            }elseif($list[0]=='BANKCARD'){
//                $BANKCARD=$list[1];
//            }elseif($list[0]=='PROTOCOLNO'){
//                $PROTOCOLNO=$list[1];
//            }elseif($list[0]=='SIGN'){
//                $SIGN=$list[1];
//            }
//        }
//        if($TYPE&&$VERSION&&$MCHNTCD&&$USERID&&$RESPONSECODE&&$RESPONSEMSG&&$MCHNTORDERID&&$ORDERID&&$AMT&&$BANKCARD&&$PROTOCOLNO&&$SIGN&&$RESPONSECODE==='0000'){
//
//            $res=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->find();
//            if($res['trade_status']!=1){
//                //修改充值订单交易状态
//                Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->update(['trade_status'=>1]);
//                $account=Db::name('trade')->where(['charge_num'=>$MCHNTORDERID])->value('account');
//                $balance=Db::name('admin')->where(['account'=>$account])->value('balance');
//                //确认用户的实名认证信息并添加金额
//                Db::name('admin')->where(['account'=>$account])->update(['balance'=>$balance+$AMT/100]);
//            }
//
//        }
//    }

  
  
  	//上传凭证接口
    public function upload_voucher(){
        $info=Request::instance()->param(true);     
        $voucher = request()->file('voucher');//凭证
       

      
        if($voucher){
            $data['voucher']='';//logo
            foreach($voucher as $file){
                // $name1 = $file->getInfo()['name'];
                // $num = strpos($name1,'.');
                // $name1 = rand(10000000,99999999).substr($name1, $num);
                //中文文件名用此行代码转码
                // $name2=iconv('utf-8','gbk',$name1);
                // 移动到框架应用根目录/public/uploads/ 目录下
                $move = $file->validate(['ext'=>'jpg,jpeg,png'])->move(ROOT_PATH . 'public' . DS . 'uploads');

                $image = \think\Image::open(ROOT_PATH . 'public' . DS . 'uploads/'. $move->getSaveName());
 
                // 按照原图的比例生成一个最大为600*600的缩略图替换原图
                $image->thumb(1500, 1500)->save(ROOT_PATH . 'public' . DS . 'uploads/'.$move->getSaveName());
                if($move){
                    $myheader = str_replace('\\','/',DS . 'public' . DS . 'uploads' . DS .$move->getSaveName());
                    //存库
                  	//获取h5端地址
                  	$h5_url = Db::name('hide')->where('id',25)->value('value');
                    $myheader = $h5_url.$myheader;
                    
                    return json($myheader, 1, '上传成功');
                }else{
                    // 上传失败获取错误信息
                    // echo $file->getError();
                    return json('', 1100, $file->getError());
                }
            }
        }else{
            return json('', 0, "未获取到文件");
        }

    }
  	
  	//发送短信
    public function sendMessage() {
        $info = Request::instance()->param();
        $mobile = $info['mobile'];
      
      
        $rand = rand(1000, 9999);
        session('yzm',$rand);
        session('mobile',$mobile);
        $json_data=sendMessage($rand,$mobile);
        $array = json_decode($json_data, true);
        if ($array['code'] == 0) {
            return json($list['rand'] = strval(md5($rand)), 1, '发送成功');
        } else {
            return json('', 0, '发送失败');
        }
    }
  
  	
  	//支付二维码
    public function ewm_info(){
        $list['alipay_ewm'] = Db::name('hide')->where('id',42)->value('value');
      	$list['wx_ewm'] = Db::name('hide')->where('id',43)->value('value');
      	$list['due_bank'] = Db::name('hide')->where('id',44)->value('value');//收款银行
      	$list['bank_num'] = Db::name('hide')->where('id',45)->value('value');//银行账号
      	$list['bank_username'] = Db::name('hide')->where('id',46)->value('value');//账户名称
        return json($list, 1, '查询成功');
    }
  
  
  	/**
     * 确认充值接口
     * @param  int    account             账号
     * @param  int    trade_price         转账金额
     * @param  int    detailed            凭证图url
     * @param  int    pay_account         付款账号
     * @param  int    pay_username        付款人
     * @param  int    trade_price         转账金额
     * @param  int    trade_type          1支付宝   2微信   3银行卡
     * @param  int    yzm                 验证码
     */
    public function comfire_recharge(){
        $info = Request::instance()->param();   
        
       

       
        
      
      	$data['pay_account'] = $info['pay_account'];//付款账号

        $data['pay_username'] = $info['pay_username'];//付款人

        $data['detailed'] = $info['detailed'];//凭证图url

        $data['account'] = $info['account'];
      
      	$data['charge_num'] = date('YmdHis').rand(1000,9999);
      
        if($info['trade_type']==1){
            $data['trade_type'] = '支付宝充值';
          	$data['trade_price'] = $info['trade_price'];
          	$data['service_charge'] = 0;
        }elseif($info['trade_type']==2){
            $data['trade_type'] = '微信充值';
          	$data['trade_price'] = $info['trade_price'];
          	$data['service_charge'] = 0;
        }else{
            $data['trade_type'] = '银行卡充值';
          	$data['trade_price'] = $info['trade_price'];
          	$data['service_charge'] = 0;
          	
        }   
        

        $data['trade_status'] = 2;//未付款
        $data['create_time'] = time();
    
      
        $res = Db::name('trade')->insert($data);
        return json('', 1, '充值成功，等待客服打款');

    }
  
  
  
  
  	/**
     * 支付宝扫码或微信扫码付款   1支付宝   2微信
     * @param  int    trade_type         充值类型 1支付宝   2微信
     * @param  int    account             平台的账号
     * @param  int    pay_account         付款账号
     * @param  int    trade_price         转账金额
     * @param  int    name         		  姓名
     * @param  int    tel                 手机号
     * @param  int    transfer_type       转账方式
     */
    public function ewm_turninto(){
        $info = Request::instance()->param();   

        $data['trade_price'] = $info['trade_price'];

        $data['charge_num'] = date('YmdHis').rand(1000,9999);
        
        $data['account'] = $info['account'];

        $data['trade_status'] = 2;//未付款
      	
      	$data['transfer_type'] = $info['transfer_type'];//转账方式
      	$data['tel'] = $info['tel'];//手机号
      	$data['name'] = $info['name'];//姓名
      	$data['pay_account'] = $info['pay_account'];//付款账号
      	
        if($info['trade_type']==1){
          $data['trade_type'] = '支付宝充值';
        }else{
          $data['trade_type'] = '微信充值';

        }
            
        
        $data['create_time'] = time();
      
      
	
        
        $res = Db::name('trade')->insert($data);
        return json('', 1, '充值成功，等待客服打款');
        
    }

}