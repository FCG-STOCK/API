<?php
namespace app\index\controller;
use think\Db;
use think\Request;
use think\Controller;
use think\Validate;
use think\Loader;
use think\Cache;
use think\Session;
use Stock;

header('Content-Type:application/json; charset=utf-8');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,Authorization");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');

class Plan extends controller {

    //策略发起平仓，卖出股票（改变状态）
    public function sellStrategy2($id,$type=1) {
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1455)) {
                return json('', 0, '非交易时间禁止平仓');
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return json('', 0, '非交易时间禁止平仓');
            }
        }

        //策略信息
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$id])->find();

        if($strategy['status']>2){
            return 0;
        }
        if(date('Ymd',time())<=date('Ymd',$strategy['buy_time'])){
            return 0;
        }
		if(thischeckstockstop($strategy['stock_code'])!=200){
            return 0;
        }
        //实盘委托
        if($strategy['is_real_disk']==1){
            $shipanstock=new Stock();

            //实盘账户信息
            $shipaninfo=$this->getshipaninfo($strategy['shipan_account'],substr($strategy['stock_code'],0,2));
            //实盘进行委托
            $success=$shipanstock->sendorder($shipaninfo['ip'],$shipaninfo['version'],$shipaninfo['yybID'],$shipaninfo['zhanghao'],
                $shipaninfo['zhanghao'],$shipaninfo['mima'],$shipaninfo['port'],'',1,$shipaninfo['PriceType'],$shipaninfo['gudongdaima'],
                substr($strategy['stock_code'],2),0,$strategy['stock_number']);
            $success=json_decode($success,true);
            //有委托编号这个参数名和值，说明委托成功了
            if(array_key_exists('委托编号',$success['data1'])&&$success['data1']['委托编号']){
                //卖出合同编号
                $data['sell_contract_num']=$success['data1']['委托编号'];

                logg(5,$strategy['account']."实盘中的策咯".$strategy['strategy_num']."在实盘系统自动卖出，委托成功");
            }else{
                logg(5,$strategy['account']."实盘中的策咯".$strategy['strategy_num']."在实盘系统自动卖出，但是委托失败了");
                return json('', 0, '委托失败');
            }
        }

        $data['status']=3;
        $data['sell_type']=$type;
        $data['sell_time']=time();
        //修改策略记录
        $res=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$id])->update($data);
        if($res){
            return 1;
        }else{
            return 0;
        }
    }
    //策略真正卖出
    public function truesellstrategy($id,$price='',$time=''){
        //T+1买卖的手续费比例
        $poundage=Db::name('hide')->where(['id' => 4])->value('value');
        //T+D买卖的手续费比例
        $bigpoundage=Db::name('hide')->where(['id' => 8])->value('value');
        //用户卖出股票时所能拿到的成数
        $chengshu=Db::name('hide')->where(['id' => 3])->value('value');

        //策略信息
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$id])->find();

        //股票当前价格(如果有实盘的，按照实盘传来的价格来计算)
        if(!$price){
            $nowprice=getOneStock2($strategy['stock_code'])['nowPri'];
        }else{
            $nowprice=$price;
        }

        //修改策略
        $data['status']=4;
        $data['sell_price']=$nowprice;
        if($time){
            $data['sell_time']=$time;
        }else{
            $data['sell_time']=time();
        }
        if($strategy['strategy_type']==1){
            $data['sell_poundage']=$strategy['double_value']*$poundage;
            $data['all_poundage']=$strategy['all_poundage']+$strategy['double_value']*$poundage;
        }else{
            $data['sell_poundage']=$strategy['double_value']*$bigpoundage;
            $data['all_poundage']=$strategy['all_poundage']+$strategy['double_value']*$bigpoundage;
        }

        $get_money=$nowprice*$strategy['stock_number']-$strategy['market_value'];
        if($get_money>0){
            $data['true_getmoney']=$get_money*$chengshu+$strategy['credit']+$strategy['credit_add']-$data['sell_poundage'];
        }else{
            $data['true_getmoney']=$get_money+$strategy['credit']+$strategy['credit_add']-$data['sell_poundage'];
        }
        if(Db::name('trade')->where(['charge_num'=>$strategy['strategy_num'],'trade_type'=>'策略卖出'])->find()){
            return '已有多个交易记录';
        }
        //最新策略信息
        $strategy=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$id])->find();
        if($strategy['status']!=3){
            return '此策略已经撤单了';
        }

        Db::startTrans();
        //修改策略记录
        $result1=Db::name('strategy')->where(['is_cancel_order'=>1])->where(['id'=>$id])->update($data);
        //修改用户策略余额和冻结金额
        $admin=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->find();
        $result2=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->update(['tactics_balance'=>$admin['tactics_balance']+$data['true_getmoney']]);
        $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->update(['frozen_money'=>$admin['frozen_money']-$strategy['credit']-$strategy['credit_add']]);
        //添加交易记录
        $trade=[
            'charge_num'=>$strategy['strategy_num'],
            'trade_price'=>$data['true_getmoney'],
            'account'=>$strategy['account'],
            'trade_type'=>'策略卖出',
            'trade_status'=>1,
            'is_del'=>1,
            'create_time'=>time(),
        ];
        $result4=Db::name('trade')->insert($trade);
        if(!$result1 || !$result2 || !$result3 || !$result4){
            Db::rollback();
            return '平仓失败';
        }
        Db::commit();
        return '平仓成功';
    }

    //每n秒进行一次操作（监听策略进行平仓操作）
    public function checkStrategyAlltime(){
        //是否限制股票交易时间

        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 931 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1505)) {
                return 0;
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<931){
                return 0;
            }
        }

        if(date('Hi', time()) >= 1450 && date('Hi', time()) <= 1500){
            //真实策略T+1
            $strategy = Db::name('strategy')
                ->alias('str')
                ->field('str.*,ad.regid')
                ->join('link_admin ad', 'str.account=ad.account')
                ->where(['str.is_cancel_order'=>1])
                ->where(['strategy_type'=>1,'str.status' => ['in', [1, 2]],'defer'=>2,'str.buy_time' => ['elt', strtotime(date('Ymd'))],'ad.is_del'=>1])
                ->select();
            if($strategy){
                //两点五十平仓
                foreach ($strategy as $k => $v) {
                    $this->sellStrategy2($v['id'],2);
                    $content = '您的策略 '.$v['stock_name'].' 已经到期，系统为您进行平仓！';
                    //进行推送
                    $admin=Db::name('settings')
                        ->alias('set')
                        ->field('set.*,ad.regid')
                        ->join('link_admin ad','ad.account=set.account')
                        ->where(['set.account'=>$v['account'],'set.is_del'=>1,'ad.is_del'=>1])
                        ->find();
                    if($admin){
                        if ($admin['sell_strategy_push'] == 1) {
                            push($v['account'], $content, 3);
                        }
                    }else{
                        if(Db::name('settings')->where(['account'=>$v['account'],'is_del'=>1])->count()<1){
                            Db::name('settings')->insert(['account'=>$v['account']]);
                        }
                        push($v['account'], $content, 3,1);
                    }
                }
            }
            //真实策略T+D
            $strategytd = Db::name('strategy')
                ->alias('str')
                ->field('str.*,ad.regid')
                ->join('link_admin ad', 'str.account=ad.account')
                ->where(['str.is_cancel_order'=>1])
                ->where(['strategy_type'=>2,'str.status' => ['in', [1, 2]],'defer'=>2,'ad.is_del'=>1])
                ->select();
            foreach($strategytd as $k=>$v){
                $day=(strtotime(date('Ymd').'000000')-strtotime(date('Ymd',$v['buy_time']).'000000'))/24/3600;
                $times=0;
                for($i=0;$i<=$day;$i++){
                    if(isHoliday($i)==0){
                        $times+=1;
                    }
                }
                if($times==5){
                    $this->sellStrategy2($v['id'],2);
                    $content = '您的策略 '.$v['stock_name'].' 已经到期，系统为您进行平仓！';
                    //进行推送
                    $admin=Db::name('settings')
                        ->alias('set')
                        ->field('set.*,ad.regid')
                        ->join('link_admin ad','ad.account=set.account')
                        ->where(['set.account'=>$v['account'],'set.is_del'=>1,'ad.is_del'=>1])
                        ->find();
                    if($admin){
                        if ($admin['sell_strategy_push'] == 1) {
                            push($v['account'], $content, 3);
                        }
                    }else{
                        if(Db::name('settings')->where(['account'=>$v['account'],'is_del'=>1])->count()<1){
                            Db::name('settings')->insert(['account'=>$v['account']]);
                        }
                        push($v['account'], $content, 3,1);
                    }
                }
            }
        }

        $shipanstock= new Stock();

        //允许亏损的信用金百分比
        $losecredit=Db::name('hide')->where(['id'=>18])->value('value');

        $step=5;
        for($i=0;$i<60;$i=($i+$step)){
            if($i>=60-$step){
                break;
            }else{
                //真实策略
                $strategy_second = Db::name('strategy')
                    ->alias('str')
                    ->field('str.*,ad.regid')
                    ->join('link_admin ad', 'str.account=ad.account')
                    ->where(['str.is_cancel_order'=>1])
                    ->where(['str.status' => 2,'str.buy_time' => ['elt', strtotime(date('Ymd'))],'ad.is_del'=>1])
                    ->select();
                if($strategy_second){
                    $str='';
                    foreach($strategy_second as $k=>$v){
                        $str.=$v['stock_code'].',';
                    }
                    $result=$shipanstock->Hq_list(substr($str,0,-1));
                    $arr=explode(';',$result);
                    unset($arr[count($arr)-1]);
                    foreach($arr as $k=>$v){
                        $strategy_second[$k]['nowprice']=explode(',',$v)[3];
                    }

                    foreach($strategy_second as $k=>$v){
                        if(intval($v['nowprice'])>1){
                            //止盈进行平仓
                            if($v['nowprice']>=$v['surplus_value']){
                                $this->sellStrategy2($v['id'],3);

                                logg(7,"策略".$v['strategy_num']. "在达到止盈价格".$v['surplus_value']."，进行了委托，委托价格为：".$v['nowprice']);
                              	//止盈发短信
                              	$message_data_zhiying['account'] = $v['account'];
                                $message_data_zhiying['strategy_num'] = $v['strategy_num'];
                                $message_data_zhiying['type'] = '止盈平仓提醒';
                                $message_data_zhiying['content'] = "您的策略".$v['stock_name'].$v['strategy_num']. "达到止盈价,进行了委托，委托价格为：".$v['nowprice'];
                                $message_data_zhiying['create_time'] = time();
                                Db::name('message')->insert($message_data_zhiying);
                              	zhiying_message_tip($message_data_zhiying['content'],$v['account']);

                                //进行推送
                                $admin_second=Db::name('settings')
                                    ->alias('set')
                                    ->field('set.*,ad.regid')
                                    ->join('link_admin ad','ad.account=set.account')
                                    ->where(['set.account'=>$v['account'],'set.is_del'=>1,'ad.is_del'=>1])
                                    ->find();
                                $content='您的策略'.$v['stock_name'].'已触发止盈，已经为您进行平仓了！';
                                if($admin_second){
                                    if ($admin_second['sell_strategy_push'] == 1) {
                                        push($admin_second['account'], $content, 3);
                                    }
                                }else{
                                    if(Db::name('settings')->where(['account'=>$v['account'],'is_del'=>1])->count()<1){
                                        Db::name('settings')->insert(['account'=>$v['account']]);
                                    }
                                    push($admin_second['account'], $content, 3,1);
                                }
                                //止损进行平仓
                            }elseif($v['nowprice']<=$v['loss_value']){
                                $this->sellStrategy2($v['id'],4);

                                logg(8,"策略".$v['strategy_num']. "在达到止损价格".$v['loss_value']."，进行了委托，委托价格为：".$v['nowprice']);
                              
                              	//止盈发短信
                              	$message_data_zhisun['account'] = $v['account'];
                                $message_data_zhisun['strategy_num'] = $v['strategy_num'];
                                $message_data_zhisun['type'] = '止盈平仓提醒';
                                $message_data_zhisun['content'] = "您的策略".$v['stock_name'].$v['strategy_num']. "达到止盈价,进行了委托，委托价格为：".$v['nowprice'];
                                $message_data_zhisun['create_time'] = time();
                                Db::name('message')->insert($message_data_zhisun);
                              	zhisun_message_tip($message_data_zhisun['content'],$v['account']);

                                //进行推送
                                $admin_second=Db::name('settings')
                                    ->alias('set')
                                    ->field('set.*,ad.regid')
                                    ->join('link_admin ad','ad.account=set.account')
                                    ->where(['set.account'=>$v['account'],'set.is_del'=>1,'ad.is_del'=>1])
                                    ->find();
                                $content='您的策略'.$v['stock_name'].'已触发止损，已经为您进行平仓了！';
                                if($admin_second){
                                    if ($admin_second['sell_strategy_push'] == 1) {
                                        push($admin_second['account'], $content, 3);
                                    }
                                }else{
                                    if(Db::name('settings')->where(['account'=>$v['account'],'is_del'=>1])->count()<1){
                                        Db::name('settings')->insert(['account'=>$v['account']]);
                                    }
                                    push($admin_second['account'], $content, 3,1);
                                }
                            }

                                //亏钱
                                if(($v['nowprice']*$v['stock_number']-$v['buy_price']*$v['stock_number'])<0){
                                  
                                  	//单个策略亏损达到信用金50%（后台可设）要做短信提示补充。
                                  	$losecredit_message_bfb = Db::name('hide')->where('id',30)->value('value');//50%
                                  	if(($v['buy_price']*$v['stock_number']-$v['nowprice']*$v['stock_number'])>=$losecredit_message_bfb*($v['credit']+$v['credit_add'])){
                                    
                                      	$if_send = Db::name('message')->where(['account'=>$v['account'],'strategy_num'=>$v['strategy_num']])->find();
                                      	if(!$if_send){
                                        	logg(13,"策略".$v['strategy_num']. "信用金亏损达到".($losecredit_message_bfb*100)."%，买入价格：".$v['buy_price']."，委托价格为：".$v['nowprice']);
                                            $message_data['account'] = $v['account'];
                                            $message_data['strategy_num'] = $v['strategy_num'];
                                            $message_data['type'] = '策略亏损提醒';
                                            $message_data['content'] = "您的策略".$v['stock_name'].$v['strategy_num']. "信用金亏损达到".($losecredit_message_bfb*100)."%，买入价格：".$v['buy_price']."，请及时追加信用金";
                                            $message_data['create_time'] = time();
                                            Db::name('message')->insert($message_data);
                                          
                                          	losecredit_message_tip($message_data['content'],$v['account']);
                                          
                                        
                                        }
                                    	
                                    
                                    }
                                  
                                  
                                  
                                    //信用金不能亏到的百分比   （现价*股数-买入价*股数）<=信用金*百分比
                                    if(($v['buy_price']*$v['stock_number']-$v['nowprice']*$v['stock_number'])>=$losecredit*($v['credit']+$v['credit_add'])){

                                         $this->sellStrategy2($v['id'],4);
                                         if($v['is_real_disk']==1){
                                             file_put_contents('shipan.txt','委托时间:'.date('Y-m-d H:i:s') ."    实盘中的策咯".$v['strategy_num'].
                                                 "信用金亏损达到".($losecredit*100)."%，进行了委托，委托价格为：".$v['nowprice']."\n", FILE_APPEND);
                                         }

                                        logg(9,"策略".$v['strategy_num']. "信用金亏损达到".($losecredit*100)."%，买入价格：".$v['buy_price']."，委托价格为：".$v['nowprice']);
                                        //进行推送
                                        $admin_second=Db::name('settings')
                                            ->alias('set')
                                            ->field('set.*,ad.regid')
                                            ->join('link_admin ad','ad.account=set.account')
                                            ->where(['set.account'=>$v['account'],'set.is_del'=>1,'ad.is_del'=>1])
                                            ->find();
                                        $content='您的策略'.$v['stock_name']."信用金亏损达到".($losecredit*100)."%，已经为您进行平仓了！";
                                        if($admin_second){
                                            if ($admin_second['sell_strategy_push'] == 1) {
                                                $this->push($admin_second['account'], $content, 3);
                                            }
                                        }else{
                                            if(Db::name('settings')->where(['account'=>$v['account'],'is_del'=>1])->count()<1){
                                                Db::name('settings')->insert(['account'=>$v['account']]);
                                            }
                                            $this->push($admin_second['account'], $content, 3,1);
                                        }
                                    }
                                }
                              
                        }
                    }
                }

                if(date('s')-$i+$step<6){
                    sleep(5);
                }
                $i=date('s');
                continue;
            }
        }

    }

    //每天查看红包有效期
    public function checkpacket(){
        $list=Db::name('packet')->where(['is_use'=>2])->select();
        foreach ($list as $k => $v) {
            if(time()>=$v['daoqi_time']){
                Db::name('packet')->where(['packet_id'=>$v['packet_id']])->update(['is_use'=>3]);
            }
        }
    }

    //每天计算递延费
    public function checkDeferMoney(){
        //扣除递延费的时间点

        $defertime=Db::name('hide')->where(['id' => 14])->value('value');
        if(date('H:i')!=$defertime){return 0;}

        //最近的一笔扣除递延费是今天的，说明今天扣过了，不能扣
        $lasttrade=Db::name('trade')
            ->where(['trade_type'=>'扣除递延费','trade_status'=>1,'is_del'=>1])
            ->order('create_time desc')
            ->find();
        if(date('m-d',$lasttrade['create_time'])==date('m-d')){
            return 0;
        }
        //下午晚上扣款则查找今天以前买入的股票，后半夜则不需要
        if($defertime>='15:01'){
            $where=['str.buy_time'=>['lt',strtotime(date('Y-m-d').' 00:00:00')]];
        }else{
            $where=[];
        }
        //递延费比例  19.9/万/天
        $defer=Db::name('hide')->where(['id' => 5])->value('value');

        //真实策略
        $strategy=Db::name('strategy')
            ->alias('str')
            ->field('str.*,ad.tactics_balance')
            ->join('link_admin ad','ad.account=str.account')
            ->where(['str.status'=>['in',[1,2]],'str.defer'=>1])
            ->where(['str.is_cancel_order'=>1])
            ->where($where)
            ->select();
        foreach($strategy as $k=>$v){
            //工作日进行扣钱，递延修改；休息日则休息日+1
            if(isHoliday()!=0){
                //休息日天数
                Db::name('strategy')->where(['id'=>$v['id']])->update(['rest_day'=>$v['rest_day']+1]);

                logg(10,$v['account'].'的'.$v['strategy_num'].'策略单号没有进行递延，休息天数由'.$v['rest_day']. '增加到'.($v['rest_day']+1).'，增加了1天');
            }else{
                //T+1免费1天的递延费，T+5免费4天的递延费  (此项目T+1早收一天手续费)
                $v['strategy_type']==1 ? $num=0 : $num=4;

                //下午晚上扣款的，交易天数=当前时间-买入时间的天数-休息天数，交易天数需要减1，后半夜则不需要
                if($defertime>='15:01'){
                    $num=$num-1;
                }
                //T+5的第一天免费
                if(((strtotime(date('Ymd'))-strtotime(date('Ymd',$v['buy_time'])))/3600/24-$v['rest_day'])>$num){
                    //递延费
                    $defer_value=$v['double_value']*$defer;
                    //账户的现在余额
                    $tactics_balance=Db::name('admin')->where(['account'=>$v['account'],'is_del'=>1])->value('tactics_balance');
                    //余额中进行扣除手续费
                    Db::name('admin')->where(['account'=>$v['account'],'is_del'=>1])->update(['tactics_balance'=>$tactics_balance-$defer_value]);
                    //策略递延天数，递延费
                    Db::name('strategy')
                        ->where(['is_cancel_order'=>1])
                        ->where(['id'=>$v['id']])
                        ->update(['defer_value'=>$v['defer_value']+$defer_value,'defer_day'=>$v['defer_day']+1]);
                    //添加扣除递延费的交易记录
                    $trade=[
                        'charge_num'=>$v['strategy_num'],
                        'trade_price'=>$defer_value,
                        'account'=>$v['account'],
                        'trade_type'=>'扣除递延费',
                        'trade_status'=>1,
                        'is_del'=>1,
                        'create_time'=>time(),
                    ];
                    Db::name('trade')->insert($trade);

                    logg(10,$v['account'].'的'.$v['strategy_num'].'策略单号进行了递延，策略的递延费由'. $v['defer_value']. '增加到'.
                        ($v['defer_value']+$defer_value).'，增加了'.$defer_value.'，-----递延天数由'.$v['defer_day']. '增加到'.
                        ($v['defer_day']+1).'，增加了1天,-----策略余额由'.$tactics_balance. '减少到'.
                        ($tactics_balance-$defer_value).'，减少了'.$defer_value);
                }else{

                    logg(10,$v['account'].'的'.$v['strategy_num'].'策略单号进行了递延，但策略创建的第二天，不进行扣钱');
                }
            }
        }
    }
    public function checkshipan(){
        //是否限制股票交易时间
        $hide=Db::name('hide')->where(['id'=>29])->value('value');
        if($hide==1){
            if (isHoliday()!=0 || (date('Hi', time()) < 930 || ((date('Hi', time()) > 1130) && (date('Hi', time()) < 1300)) || date('Hi', time()) > 1505)) {
                return 0;
            }
        }else{
            if(date('Hi', time())>830 && date('Hi', time())<930){
                return 0;
            }
        }

        $step=5;
        for($t=0;$t<60;$t=($t+$step)){
            if($t>=60-$step){
                break;
            }else{
                //没走实盘的会在买后15s变成持股中
                $meizoustrategy=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['status'=>1,'is_real_disk'=>2,'buy_time'=>['elt',time()-5]])
                    ->select();
                foreach($meizoustrategy as $k=>$v){
                    //股票当前价格
                    $nowprice=getOneStock2($v['stock_code'])['nowPri'];
                    $lastprice=$nowprice;
                    $market_value=$lastprice*$v['stock_number'];
                    Db::name('strategy')->where(['id'=>$v['id']])->update(['status'=>2,'buy_price'=>$lastprice,'market_value'=>$market_value]);
                }
                //实盘中的持仓申报中修改为持仓中
                $strategy=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['status'=>1,'is_real_disk'=>1])
                    ->select();

                //是否请求过实盘接口
                $qingqiuguo=0;
                if($strategy){
                    $qingqiuguo=1;
                    //实盘的当日委托当日成交
                    $success=$this->qingqiushipan();
                    $chengjiao=$success['data1'];
                    $weituo=$success['data2'];

                    //有时候接口会返回空的当日委托数据，这时候要查找当日成交，假如当日成交的这笔策略的股数之和等于买卖的股数，相当于买卖成功了
                    if($weituo){
                        foreach ($strategy as $k => $v) {
                            foreach($weituo as $kk=>$vv){
                                if($vv['委托编号']==$v['buy_contract_num']&&$vv['委托类别']=='买入'){
                                    if($vv['状态说明']=='6-全部成交'){
                                        $last_buy_price=$vv['成交价格'];
                                        if($last_buy_price&&$last_buy_price!='0.0'&&$last_buy_price>0.1){
//                                            //买入时不管厘，直接进一位，再取小数点后两位
//                                            if(is_float($last_buy_price)){
//                                                if(strlen(explode('.',$last_buy_price)[1])==2){
//                                                    $last_buy_price=$last_buy_price;
//                                                }else{
//                                                    $last_buy_price=substr($last_buy_price,0,-1)+0.01;
//                                                }
//                                            }else{
//                                                if(substr($last_buy_price,strlen($last_buy_price)-1)==0){
//                                                    $last_buy_price=substr($last_buy_price,0,-1);
//                                                }else{
//                                                    $last_buy_price=substr($last_buy_price,0,-1)+0.01;
//                                                }
//                                            }
                                            Db::name('strategy')
                                                ->where(['is_cancel_order'=>1])
                                                ->where(['id'=>$v['id']])
                                                ->update(['buy_price'=>$last_buy_price,'market_value'=>$last_buy_price*$v['stock_number'],'status'=>2]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //没走实盘的会在卖后15s变成已平仓
                $truesellstrategy=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['status'=>3,'is_real_disk'=>2,'sell_time'=>['elt',time()-5]])
                    ->select();
                foreach($truesellstrategy as $k=>$v){
                    //股票当前价格
                    $nowprice=getOneStock2($v['stock_code'])['nowPri']*0.999;
                    //扣除印花税
                    $lastprice=explode('.',$nowprice)[0].'.'.substr(explode('.',$nowprice)[1],0,3);

                    //卖出时不要厘，再取小数点后两位
                    if(strlen(explode('.',$lastprice*1)[1])==3){
                        $lastprice=substr($lastprice,0,-1);
                    }
                    $this->truesellstrategy($v['id'],$lastprice);
                }
                //如果有走实盘的平仓申报中的状态,平仓申报中修改为已平仓
                $strategy_sell=Db::name('strategy')
                    ->where(['is_cancel_order'=>1])
                    ->where(['status'=>3,'is_real_disk'=>1])
                    ->select();
                if($strategy_sell){
                    if($qingqiuguo!=1){
                        $qingqiuguo=1;
                        //实盘的当日委托当日成交
                        $success=$this->qingqiushipan();
                        $chengjiao=$success['data1'];
                        $weituo=$success['data2'];
                    }
                    if($weituo){
                        foreach ($strategy_sell as $k => $v) {
                            foreach($weituo as $kk=>$vv){
                                if($vv['委托编号']==$v['sell_contract_num']&&$vv['委托类别']=='卖出'){
                                    $last_sell_price=$vv['成交价格'];
                                    $weituotime=strtotime(date('Y-m-d ').$vv['委托时间']);
                                    if($vv['状态说明']=='6-全部成交'){
//                                        //扣除印花税
//                                        $last_sell_price=$last_sell_price*0.999;
//                                        //卖出时不要厘，再取小数点后两位
//                                        $last_sell_price=explode('.',$last_sell_price)[0].'.'.substr(explode('.',$last_sell_price)[1],0,3);
//                                        if(strlen(explode('.',$last_sell_price*1)[1])==3){
//                                            $last_sell_price=substr($last_sell_price,0,-1);
//                                        }
                                        $this->truesellstrategy($v['id'],$last_sell_price,$weituotime);
                                    }
                                }
                            }
                        }
                    }
                }

                sleep(3);

                //所有没走实盘的在持仓申请中的已申请撤单的策略
                $chestrategy=[];
                $chestrategy=Db::name('strategy')
                    ->where(['status'=>1,'is_real_disk'=>2,'is_cancel_order'=>2])
                    ->select();
                if($chestrategy){
                    foreach($chestrategy as $k=>$v){
                        $this->updatebuystrategy($v['id']);
                    }
                }

                //所有走实盘的在持仓申请中的已申请撤单的策略
                $chestrategy=[];
                $chestrategy=Db::name('strategy')
                    ->where(['status'=>1,'is_real_disk'=>1,'is_cancel_order'=>2])
                    ->select();
                if($chestrategy){
                    if($qingqiuguo!=1){
                        $qingqiuguo=1;
                        //实盘的当日委托当日成交
                        $success=$this->qingqiushipan();
                        $chengjiao=$success['data1'];
                        $weituo=$success['data2'];
                    }
                    foreach ($chestrategy as $k => $v) {
                        foreach($weituo as $kk=>$vv){
                            if($vv['委托编号']==$v['buy_contract_num']&&$vv['委托类别']='撤买'){
                                $this->updatebuystrategy($v['id']);
                            }
                        }
                    }
                }

                //所有没走实盘的在平仓申请中的已申请撤单的策略
                $chestrategy=[];
                $chestrategy=Db::name('strategy')
                    ->where(['status'=>3,'is_real_disk'=>2,'is_cancel_order'=>2])
                    ->select();
                if($chestrategy){
                    foreach($chestrategy as $k=>$v){
                        if($strategy=Db::name('strategy')->where(['id'=>$v['id']])->value('status')==3){
                            Db::name('strategy')->where(['id'=>$v['id']])->update(['is_cancel_order'=>1,'status'=>2]);

                            logg(12,'委托时间:'.date('Y-m-d H:i:s',$v['sell_time']).'撤单时间：'.date('Y-m-d H:i:s')
                                ."   策咯".$v['strategy_num']."在卖出后，申请了撤单，已修改");
                        }
                    }
                }

                //所有走实盘的在平仓申请中的已申请撤单的策略
                $chestrategy=[];
                $chestrategy=Db::name('strategy')
                    ->where(['status'=>3,'is_real_disk'=>1,'is_cancel_order'=>2])
                    ->select();
                if($chestrategy){
                    if($qingqiuguo!=1){
                        $qingqiuguo=1;
                        //实盘的当日委托当日成交
                        $success=$this->qingqiushipan();
                        $chengjiao=$success['data1'];
                        $weituo=$success['data2'];
                    }
                    foreach ($chestrategy as $k => $v) {
                        foreach($weituo as $kk=>$vv){
                            if($vv['委托编号']==$v['sell_contract_num']&&$vv['委托类别']='撤卖'){
                                if($strategy=Db::name('strategy')->where(['id'=>$v['id']])->value('status')==3){
                                    Db::name('strategy')->where(['id'=>$v['id']])->update(['is_cancel_order'=>1,'status'=>2]);

                                    logg(12,'委托时间:'.date('Y-m-d H:i:s',$v['sell_time']).'撤单时间：'.date('Y-m-d H:i:s')
                                        ."   策咯".$v['strategy_num']."在卖出后，申请了撤单，已修改");
                                }
                            }
                        }
                    }
                }

                sleep(2);

                $t=date('s');
                continue;
            }
        }
    }

    //把买入的策略进行撤单
    public function updatebuystrategy($id){
        $strategy=Db::name('strategy')
            ->where(['id'=>$id])
            ->find();
        if($strategy['status']!=1){
            logg(11,"策咯". $strategy['strategy_num']."买入后申请了撤单，但是这个策略已经持股中了\n");
            return 0;
        }
        //把持仓申报的策略进行撤单
        Db::startTrans();
        //订单删除
        $result1=Db::name('trade')->where(['charge_num'=>$strategy['strategy_num'],'is_del'=>1])->update(['is_del'=>2]);
        //策略变成已撤单
        $result2=Db::name('strategy')
            ->where(['is_cancel_order'=>2])
            ->where(['id'=>$strategy['id']])
            ->update(['is_cancel_order'=>3]);
        //用户信息
        $admin=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])->find();
        //用户策略余额加上信用金再加上买的手续费,冻结金额再减上信用金
        $data=[
            'tactics_balance'=>$admin['tactics_balance']+$strategy['credit']+$strategy['buy_poundage'],
            'frozen_money'=>$admin['frozen_money']-$strategy['credit']
        ];
        $result3=Db::name('admin')->where(['account' => $strategy['account'],'is_del'=>1])
            ->update($data);
        if(!$result1 || !$result2 || !$admin || !$result3){
            Db::rollback();

            logg(11,'委托时间:'.date('Y-m-d H:i:s',$strategy['buy_time']).'撤单时间：'.date('Y-m-d H:i:s')
                ."   策咯".$strategy['strategy_num']."在买入后，申请了撤单，修改失败了");
        }
        Db::commit();

        logg(11,'委托时间:'.date('Y-m-d H:i:s',$strategy['buy_time']).'撤单时间：'.date('Y-m-d H:i:s')
            ."   策咯".$strategy['strategy_num']."在买入后，申请了撤单，已修改");
    }

    //每天早上观察余额不足的人
    public function checkAdminBalance(){
        $list=Db::name('admin')->where(['tactics_balance'=>['lt',0],'is_del'=>1])->select();
        $content='您的策略余额已不足，请及时进行充值';
        foreach($list as $k=>$v){
            push($v['account'],$content,1);

          	//递延费不足发短信。
            $if_send = Db::name('message')->where(['account'=>$v['account'],'type'=>'递延费不足提醒'])->find();
            if(!$if_send){                
                $message_data['account'] = $v['account'];
                $message_data['content'] = "您的策略余额已不足扣除递延费，请及时进行充值";
                $message_data['type'] = '递延费不足提醒';
                $message_data['create_time'] = time();
                Db::name('message')->insert($message_data);              
                diyan_tip($message_data['content'],$v['account']);                          
            }
          
        }
        //查询持股中，开启自动递延，并且开启推送的人进行推送
        $deferlist=Db::name('strategy')
            ->alias('str')
            ->field('str.*,ad.regid')
            ->join('link_admin ad','ad.account=str.account')
            ->join('link_settings set','set.account=ad.account')
            ->where(['str.is_cancel_order'=>1])
            ->where(['str.status'=>['in',[1,2]],'str.defer'=>1,'ad.is_del'=>1,'set.is_del'=>1,'set.defer_push'=>1])
            ->select();

        foreach($deferlist as $k=>$v){
            $content='您的策略'.$v['stock_name'].'已进行递延，请注意关注';
            if($v['defer_push']==1){
                push($v['account'],$content,1);
            }else{
                push($v['account'],$content,1,1);
            }
        }
    }

    //获取实盘查询的必备参数
    public function getshipaninfo($id,$sh_sz=''){
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipan=[];
        foreach($allshipan_account as $k=>$v){
            if($v['id']==$id){
                $shipan=$v;
            }
        }
        if($sh_sz){
            if($sh_sz=='sh'){
                $shipan['gudongdaima']=$shipan['shholder'];
                $shipan['ExchangeID']=1;
                $shipan['PriceType']=6;
            }else{
                $shipan['gudongdaima']=$shipan['szholder'];
                $shipan['ExchangeID']=0;
                $shipan['PriceType']=1;
            }
        }
        return $shipan;
    }
    //请求实盘接口的当日委托当日成交
    public function qingqiushipan(){
        $allshipan_account=[];
        $chengjiao=[];
        $weituo=[];
        if(cache('shipan_account_arr')){
            $allshipan_account=cache('shipan_account_arr');
        }else{
            $allshipan_account=Db::name('shipan_account')->where(['is_del'=>1])->select();
            cache('shipan_account_arr', $allshipan_account, 3600);
        }
        $shipanstock=new Stock();
        foreach($allshipan_account as $k=>$v){
            $success='';
            $success=$shipanstock->querydatas($v['ip'],$v['version'],$v['yybID'],$v['zhanghao'],$v['zhanghao'],$v['mima'],$v['port'],'','2,3',2);
            $success=json_decode($success,true);

            if($success['Category1']['data1']['证券代码']){
                foreach($success['Category1'] as $kk=>$vv){
                    $chengjiao[]=$vv;
                }
            }else{
                $chengjiao=[];
            }
            if($success['Category0']['data1']['证券代码']){
                foreach($success['Category0'] as $kk=>$vv){
                    $weituo[]=$vv;
                }
            }else{
                $weituo=[];
            }
        }

        $arr['data1']=$chengjiao;
        $arr['data2']=$weituo;
        return $arr;
    }
  
  
  	//9点一次   9点15一次
	//除权除息
	public function chuquan(){
		if(date('Hi',time())!=900 || date('Hi',time())!=915){
			return 0;
		}
		$stock = new Stock;
		$strategy = Db::name('strategy')->where(['status'=>2,'is_cancel_order'=>1])->select();

		$stock_chuquan = [];//得到的除权除息数据数组
		if($strategy){
			foreach ($strategy as $k => $v) {
				$chuquan_data = $stock->chuquan(substr($v['stock_code'],2));
				$chuquan_arr = json_decode($chuquan_data,true);
				if($chuquan_arr){
					foreach ($chuquan_arr as $kk => $vv) {
						if($vv['日期']==date('Ymd')&&$vv['保留']==1){
							$stock_chuquan[] = $vv;
						}
					}
				}
			}

		}
      

		//存入数据库
		foreach ($stock_chuquan as $k => $v) {
			if(substr($v['证券代码'], 0,2)=='00' || substr($v['证券代码'], 0,2)=='30'){
				$stock_code = 'sz'.$v['证券代码'];//股票代码
				$json = getOneStock($stock_code);
				$stock_name = $json['result'][0]['data']['name'];//股票名字
			}else{
				$stock_code = 'sh'.$v['证券代码'];//股票代码
				$json = getOneStock($stock_code);
				$stock_name = $json['result'][0]['data']['name'];//股票名字
			}

			$res = Db::name('stock_chuquan')->where('stock_code',$stock_code)->whereTime('create_time', 'today')->find();
			if(!$res){
				Db::name('stock_chuquan')->insert(['stock_code'=>$stock_code,'stock_name'=>$stock_name,'give_cash'=>round($v['送现金'],2),'give_stock_number'=>round($v['送股数'],2),'create_time'=>date('Y-m-d H:i:s')]);
			}

		}
	}
  
  
  	//9点20 执行 改变处于除权派息的股票 策略
  	public function create_chuquan(){
        //今天除权派息的股票
        $already_stock=Db::name('stock_chuquan')->whereTime('create_time', 'today')->select();
        if($already_stock){
            $loss_credit=Db::name('hide')->where(['id'=>18])->value('value');
            foreach($already_stock as $k=>$v){
                //所有今天除权派息的持股中的股票
                $strategy = Db::name('strategy')->where(['status'=>2,'is_cancel_order'=>1,'stock_code'=>$v['stock_code']])->select();
                foreach($strategy as $kk=>$vv){
                    $is_chuquan=Db::name('trade')
                        ->where(['charge_num'=>$vv['strategy_num'],'trade_type'=>'除权派息','trade_status'=>1,'is_del'=>1])
                        ->whereTime('create_time', 'today')
                        ->select();
                    if(!$is_chuquan){
                        $stock_number=floor($vv['stock_number']*($v['give_stock_number']+10)/10/100)*100;
                        $buy_price=round($vv['buy_price']/(($v['give_stock_number']+10)/10),2);
                        $red=round($vv['stock_number']*$vv['buy_price']-$stock_number*$buy_price+$vv['stock_number']*$v['give_cash']*0.08,2);
                        $array=[
                            'stock_number'=>$stock_number,
                            'buy_price'=>$buy_price,
                            'market_value'=>$stock_number*$buy_price,
                            'surplus_value'=>round($buy_price*1.2,2),
                            'loss_value'=>round(($stock_number*$buy_price-($vv['credit']+$vv['credit_add'])*$loss_credit)/$stock_number,2),
                        ];
                        $trade=[
                            'account'=>$vv['account'],
                            'charge_num'=>$vv['strategy_num'],
                            'trade_price'=>$red,
                            'trade_type'=>'除权派息',
                            'trade_status'=>1,
                            'is_del'=>1,
                            'create_time'=>time(),
                            'detailed'=>$vv['stock_name'].'进行了除权派息，价格由'.$vv['buy_price'].'变成'.$buy_price.'，数量由'.$vv['stock_number'].'变成'.$stock_number,
                        ];

                        Db::startTrans();
                        $v['give_stock_number'] ? $res1=Db::name('strategy')->where(['id'=>$vv['id']])->update($array) : $res1=1;
                        $red ? $res2=Db::name('admin')->where(['account'=>$vv['account'],'is_del'=>1])->setInc('tactics_balance',$red) : $res2=1;
                        $res3=Db::name('trade')->insert($trade);

                        if(!$res1 || !$res2 || !$res3){
                            Db::rollback();
                            return '除权派息失败';
                        }
                        Db::commit();
                    }

                }
            }
        }
    }
  
  

}
