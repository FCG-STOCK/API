﻿<?php

class SecretUtilTools
{
    //加密算法       
    public static  function encryptForDES($input,$key)   
    {         
       $size = mcrypt_get_block_size('des','ecb');  
       $input = self::pkcs5_pad($input, $size);  
       $td = mcrypt_module_open('des', '', 'ecb', '');  
       $iv = @mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);  
       @mcrypt_generic_init($td, $key, $iv);  
       $data = mcrypt_generic($td, $input);  
       mcrypt_generic_deinit($td);  
       mcrypt_module_close($td);  
       $data = base64_encode($data);  
       return $data;  
    }   
    
             
    public static  function pkcs5_pad ($text, $blocksize)   
    {         
       $pad = $blocksize - (strlen($text) % $blocksize);  
       return $text . str_repeat(chr($pad), $pad);  
    } 
        
    public static  function pkcs5_unpad($text)   
    {         
       $pad = ord($text{strlen($text)-1});  
       if ($pad > strlen($text))  
       {  
           return false;  
       }  
       if (strspn($text, chr($pad), strlen($text) - $pad) != $pad)  
       {  
          return false;  
       }  
       return substr($text, 0, -1 * $pad);  
    }


    /**
     * 发送post请求
     * @param string $url 请求地址
     * @param array $post_data post键值对数据
     * @return string
     */
    function send_post($url, $post_data) {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


}
